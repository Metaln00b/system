"use strict";Cu.import("resource://gre/modules/Services.jsm");const kPrefPresentationDiscoverable="dom.presentation.discoverable";(function setupRemoteControlSettings(){ var remoteControlScope={};function importRemoteControlService(){if(!("RemoteControlService"in remoteControlScope)){Cu.import("resource://gre/modules/RemoteControlService.jsm",remoteControlScope);}}
function getTime(){return new Date().getTime();}
function debug(s){let time=new Date(getTime()).toString();} 
var statusMonitor={init:function sm_init(){debug("init"); Services.prefs.addObserver(kPrefPresentationDiscoverable,this,false); Services.obs.addObserver(this,"network:offline-status-changed",false); window.addEventListener('moztimechange',this.onTimeChange); if(Ci.nsINetworkManager){Services.obs.addObserver(this,"network-active-changed",false);} 
if(!this.startService()){this.stopService();}},observe:function sm_observe(subject,topic,data){debug("observe: "+topic+", "+data);switch(topic){case"network-active-changed":{if(!subject){ this.stopService();break;}

if(!Services.io.offline){this.startService();}
break;}
case"network:offline-status-changed":{if(data=="offline"){ this.stopService();}else{
 this.startService();}
break;}
case"nsPref:changed":{if(data!=kPrefPresentationDiscoverable){break;}
if(!this.startService()){this.stopService();}
break;}
default:break;}},onTimeChange:function sm_onTimeChange(){debug("onTimeChange"); this.discoverable&&remoteControlScope.RemoteControlService&&remoteControlScope.RemoteControlService.onTimeChange(getTime());},startService:function sm_startService(){debug("startService"); if(this.discoverable){importRemoteControlService();remoteControlScope.RemoteControlService.start();return true;}
return false;},stopService:function sm_stopService(){debug("stopService");remoteControlScope.RemoteControlService&&remoteControlScope.RemoteControlService.stop();},get discoverable(){return Services.prefs.getBoolPref(kPrefPresentationDiscoverable);},};statusMonitor.init();})();