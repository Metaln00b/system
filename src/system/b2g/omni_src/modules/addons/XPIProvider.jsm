"use strict";const Cc=Components.classes;const Ci=Components.interfaces;const Cr=Components.results;const Cu=Components.utils;this.EXPORTED_SYMBOLS=["XPIProvider"];const CONSTANTS={};Cu.import("resource://gre/modules/addons/AddonConstants.jsm",CONSTANTS);const{ADDON_SIGNING,REQUIRE_SIGNING}=CONSTANTS
Cu.import("resource://gre/modules/Services.jsm");Cu.import("resource://gre/modules/XPCOMUtils.jsm");Cu.import("resource://gre/modules/AddonManager.jsm");Cu.import("resource://gre/modules/Preferences.jsm");XPCOMUtils.defineLazyModuleGetter(this,"AddonRepository","resource://gre/modules/addons/AddonRepository.jsm");XPCOMUtils.defineLazyModuleGetter(this,"ChromeManifestParser","resource://gre/modules/ChromeManifestParser.jsm");XPCOMUtils.defineLazyModuleGetter(this,"LightweightThemeManager","resource://gre/modules/LightweightThemeManager.jsm");XPCOMUtils.defineLazyModuleGetter(this,"ExtensionData","resource://gre/modules/Extension.jsm");XPCOMUtils.defineLazyModuleGetter(this,"Locale","resource://gre/modules/Locale.jsm");XPCOMUtils.defineLazyModuleGetter(this,"FileUtils","resource://gre/modules/FileUtils.jsm");XPCOMUtils.defineLazyModuleGetter(this,"ZipUtils","resource://gre/modules/ZipUtils.jsm");XPCOMUtils.defineLazyModuleGetter(this,"NetUtil","resource://gre/modules/NetUtil.jsm");XPCOMUtils.defineLazyModuleGetter(this,"PermissionsUtils","resource://gre/modules/PermissionsUtils.jsm");XPCOMUtils.defineLazyModuleGetter(this,"Promise","resource://gre/modules/Promise.jsm");XPCOMUtils.defineLazyModuleGetter(this,"Task","resource://gre/modules/Task.jsm");XPCOMUtils.defineLazyModuleGetter(this,"OS","resource://gre/modules/osfile.jsm");XPCOMUtils.defineLazyModuleGetter(this,"BrowserToolboxProcess","resource://devtools/client/framework/ToolboxProcess.jsm");XPCOMUtils.defineLazyModuleGetter(this,"ConsoleAPI","resource://gre/modules/Console.jsm");XPCOMUtils.defineLazyModuleGetter(this,"ProductAddonChecker","resource://gre/modules/addons/ProductAddonChecker.jsm");XPCOMUtils.defineLazyModuleGetter(this,"UpdateUtils","resource://gre/modules/UpdateUtils.jsm");XPCOMUtils.defineLazyModuleGetter(this,"AppConstants","resource://gre/modules/AppConstants.jsm");XPCOMUtils.defineLazyServiceGetter(this,"Blocklist","@mozilla.org/extensions/blocklist;1",Ci.nsIBlocklistService);XPCOMUtils.defineLazyServiceGetter(this,"ChromeRegistry","@mozilla.org/chrome/chrome-registry;1","nsIChromeRegistry");XPCOMUtils.defineLazyServiceGetter(this,"ResProtocolHandler","@mozilla.org/network/protocol;1?name=resource","nsIResProtocolHandler");XPCOMUtils.defineLazyServiceGetter(this,"AddonPolicyService","@mozilla.org/addons/policy-service;1","nsIAddonPolicyService");XPCOMUtils.defineLazyGetter(this,"CertUtils",function(){let certUtils={};Components.utils.import("resource://gre/modules/CertUtils.jsm",certUtils);return certUtils;});const nsIFile=Components.Constructor("@mozilla.org/file/local;1","nsIFile","initWithPath");const PREF_DB_SCHEMA="extensions.databaseSchema";const PREF_INSTALL_CACHE="extensions.installCache";const PREF_XPI_STATE="extensions.xpiState";const PREF_BOOTSTRAP_ADDONS="extensions.bootstrappedAddons";const PREF_PENDING_OPERATIONS="extensions.pendingOperations";const PREF_EM_DSS_ENABLED="extensions.dss.enabled";const PREF_DSS_SWITCHPENDING="extensions.dss.switchPending";const PREF_DSS_SKIN_TO_SELECT="extensions.lastSelectedSkin";const PREF_GENERAL_SKINS_SELECTEDSKIN="general.skins.selectedSkin";const PREF_EM_UPDATE_URL="extensions.update.url";const PREF_EM_UPDATE_BACKGROUND_URL="extensions.update.background.url";const PREF_EM_ENABLED_ADDONS="extensions.enabledAddons";const PREF_EM_EXTENSION_FORMAT="extensions.";const PREF_EM_ENABLED_SCOPES="extensions.enabledScopes";const PREF_EM_SHOW_MISMATCH_UI="extensions.showMismatchUI";const PREF_XPI_ENABLED="xpinstall.enabled";const PREF_XPI_WHITELIST_REQUIRED="xpinstall.whitelist.required";const PREF_XPI_DIRECT_WHITELISTED="xpinstall.whitelist.directRequest";const PREF_XPI_FILE_WHITELISTED="xpinstall.whitelist.fileRequest";const PREF_XPI_SIGNATURES_REQUIRED="xpinstall.signatures.required";const PREF_XPI_SIGNATURES_DEV_ROOT="xpinstall.signatures.dev-root";const PREF_XPI_PERMISSIONS_BRANCH="xpinstall.";const PREF_XPI_UNPACK="extensions.alwaysUnpack";const PREF_INSTALL_REQUIREBUILTINCERTS="extensions.install.requireBuiltInCerts";const PREF_INSTALL_REQUIRESECUREORIGIN="extensions.install.requireSecureOrigin";const PREF_INSTALL_DISTRO_ADDONS="extensions.installDistroAddons";const PREF_BRANCH_INSTALLED_ADDON="extensions.installedDistroAddon.";const PREF_INTERPOSITION_ENABLED="extensions.interposition.enabled";const PREF_SYSTEM_ADDON_SET="extensions.systemAddonSet";const PREF_SYSTEM_ADDON_UPDATE_URL="extensions.systemAddon.update.url";const PREF_E10S_BLOCK_ENABLE="extensions.e10sBlocksEnabling";const PREF_EM_MIN_COMPAT_APP_VERSION="extensions.minCompatibleAppVersion";const PREF_EM_MIN_COMPAT_PLATFORM_VERSION="extensions.minCompatiblePlatformVersion";const PREF_CHECKCOMAT_THEMEOVERRIDE="extensions.checkCompatibility.temporaryThemeOverride_minAppVersion";const PREF_EM_HOTFIX_ID="extensions.hotfix.id";const PREF_EM_CERT_CHECKATTRIBUTES="extensions.hotfix.cert.checkAttributes";const PREF_EM_HOTFIX_CERTS="extensions.hotfix.certs.";const URI_EXTENSION_UPDATE_DIALOG="chrome://mozapps/content/extensions/update.xul";const URI_EXTENSION_STRINGS="chrome://mozapps/locale/extensions/extensions.properties";const STRING_TYPE_NAME="type.%ID%.name";const DIR_EXTENSIONS="extensions";const DIR_SYSTEM_ADDONS="features";const DIR_STAGE="staged";const DIR_TRASH="trash";const FILE_DATABASE="extensions.json";const FILE_OLD_CACHE="extensions.cache";const FILE_RDF_MANIFEST="install.rdf";const FILE_WEB_MANIFEST="manifest.json";const FILE_XPI_ADDONS_LIST="extensions.ini";const KEY_PROFILEDIR="ProfD";const KEY_ADDON_APP_DIR="XREAddonAppDir";const KEY_TEMPDIR="TmpD";const KEY_APP_DISTRIBUTION="XREAppDist";const KEY_APP_FEATURES="XREAppFeat";const KEY_APP_PROFILE="app-profile";const KEY_APP_SYSTEM_ADDONS="app-system-addons";const KEY_APP_SYSTEM_DEFAULTS="app-system-defaults";const KEY_APP_GLOBAL="app-global";const KEY_APP_SYSTEM_LOCAL="app-system-local";const KEY_APP_SYSTEM_SHARE="app-system-share";const KEY_APP_SYSTEM_USER="app-system-user";const KEY_APP_TEMPORARY="app-temporary";const NOTIFICATION_FLUSH_PERMISSIONS="flush-pending-permissions";const XPI_PERMISSION="install";const RDFURI_INSTALL_MANIFEST_ROOT="urn:mozilla:install-manifest";const PREFIX_NS_EM="http://www.mozilla.org/2004/em-rdf#";const TOOLKIT_ID="toolkit@mozilla.org";const XPI_SIGNATURE_CHECK_PERIOD=24*60*60;XPCOMUtils.defineConstant(this,"DB_SCHEMA",17);const NOTIFICATION_TOOLBOXPROCESS_LOADED="ToolboxProcessLoaded";const PROP_METADATA=["id","version","type","internalName","updateURL","updateKey","optionsURL","optionsType","aboutURL","iconURL","icon64URL"];const PROP_LOCALE_SINGLE=["name","description","creator","homepageURL"];const PROP_LOCALE_MULTI=["developers","translators","contributors"];const PROP_TARGETAPP=["id","minVersion","maxVersion"];const PENDING_INSTALL_METADATA=["syncGUID","targetApplications","userDisabled","softDisabled","existingAddonID","sourceURI","releaseNotesURI","installDate","updateDate","applyBackgroundUpdates","compatibilityOverrides"];
const STATIC_BLOCKLIST_PATTERNS=[{creator:"Mozilla Corp.",level:Blocklist.STATE_BLOCKED,blockID:"i162"},{creator:"Mozilla.org",level:Blocklist.STATE_BLOCKED,blockID:"i162"}];const BOOTSTRAP_REASONS={APP_STARTUP:1,APP_SHUTDOWN:2,ADDON_ENABLE:3,ADDON_DISABLE:4,ADDON_INSTALL:5,ADDON_UNINSTALL:6,ADDON_UPGRADE:7,ADDON_DOWNGRADE:8};const TYPES={extension:2,theme:4,locale:8,multipackage:32,dictionary:64,experiment:128,};
const TYPE_ALIASES={"webextension":"extension",};const CHROME_TYPES=new Set(["extension","locale","experiment",]);const RESTARTLESS_TYPES=new Set(["webextension","dictionary","experiment","locale",]);const SIGNED_TYPES=new Set(["webextension","extension","experiment",]);function mustSign(aType){if(!SIGNED_TYPES.has(aType))
return false;return REQUIRE_SIGNING||Preferences.get(PREF_XPI_SIGNATURES_REQUIRED,false);}

const XPI_STARTING="XPIStarting";const XPI_BEFORE_UI_STARTUP="BeforeFinalUIStartup";const XPI_AFTER_UI_STARTUP="AfterFinalUIStartup";const COMPATIBLE_BY_DEFAULT_TYPES={extension:true,dictionary:true};const MSG_JAR_FLUSH="AddonJarFlush";const MSG_MESSAGE_MANAGER_CACHES_FLUSH="AddonMessageManagerCachesFlush";var gGlobalScope=this;var gIDTest=/^(\{[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}\}|[a-z0-9-\._]*\@[a-z0-9-\._]+)$/i;Cu.import("resource://gre/modules/Log.jsm");const LOGGER_ID="addons.xpi";
var logger=Log.repository.getLogger(LOGGER_ID);const LAZY_OBJECTS=["XPIDatabase","XPIDatabaseReconcile"];var gLazyObjectsLoaded=false;function loadLazyObjects(){let uri="resource://gre/modules/addons/XPIProviderUtils.js";let scope=Cu.Sandbox(Services.scriptSecurityManager.getSystemPrincipal(),{sandboxName:uri,wantGlobalProperties:["TextDecoder"],});let shared={ADDON_SIGNING,SIGNED_TYPES,BOOTSTRAP_REASONS,DB_SCHEMA,AddonInternal,XPIProvider,XPIStates,syncLoadManifestFromFile,isUsableAddon,recordAddonTelemetry,applyBlocklistChanges,flushChromeCaches,canRunInSafeMode,}
for(let key of Object.keys(shared))
scope[key]=shared[key];Services.scriptloader.loadSubScript(uri,scope);for(let name of LAZY_OBJECTS){delete gGlobalScope[name];gGlobalScope[name]=scope[name];}
gLazyObjectsLoaded=true;return scope;}
LAZY_OBJECTS.forEach(name=>{Object.defineProperty(gGlobalScope,name,{get:function(){let objs=loadLazyObjects();return objs[name];},configurable:true});});
function waitForAllPromises(promises){return new Promise((resolve,reject)=>{let shouldReject=false;let rejectValue=null;let newPromises=promises.map(p=>p.catch(value=>{shouldReject=true;rejectValue=value;}));Promise.all(newPromises).then((results)=>shouldReject?reject(rejectValue):resolve(results));});}
function findMatchingStaticBlocklistItem(aAddon){for(let item of STATIC_BLOCKLIST_PATTERNS){if("creator"in item&&typeof item.creator=="string"){if((aAddon.defaultLocale&&aAddon.defaultLocale.creator==item.creator)||(aAddon.selectedLocale&&aAddon.selectedLocale.creator==item.creator)){return item;}}}
return null;}
function addonMap(addons){return new Map(addons.map(a=>[a.id,a]));}
function setFilePermissions(aFile,aPermissions){try{aFile.permissions=aPermissions;}
catch(e){logger.warn("Failed to set permissions "+aPermissions.toString(8)+" on "+
aFile.path,e);}}
function SafeInstallOperation(){this._installedFiles=[];this._createdDirs=[];}
SafeInstallOperation.prototype={_installedFiles:null,_createdDirs:null,_installFile:function(aFile,aTargetDirectory,aCopy){let oldFile=aCopy?null:aFile.clone();let newFile=aFile.clone();try{if(aCopy){newFile.copyTo(aTargetDirectory,null);newFile=aTargetDirectory.clone();newFile.append(aFile.leafName);
newFile.lastModifiedTime=Date.now();}
else{newFile.moveTo(aTargetDirectory,null);}}
catch(e){logger.error("Failed to "+(aCopy?"copy":"move")+" file "+aFile.path+" to "+aTargetDirectory.path,e);throw e;}
this._installedFiles.push({oldFile:oldFile,newFile:newFile});},_installDirectory:function(aDirectory,aTargetDirectory,aCopy){let newDir=aTargetDirectory.clone();newDir.append(aDirectory.leafName);try{newDir.create(Ci.nsIFile.DIRECTORY_TYPE,FileUtils.PERMS_DIRECTORY);}
catch(e){logger.error("Failed to create directory "+newDir.path,e);throw e;}
this._createdDirs.push(newDir);


let entries=getDirectoryEntries(aDirectory,true);for(let entry of entries){try{this._installDirEntry(entry,newDir,aCopy);}
catch(e){logger.error("Failed to "+(aCopy?"copy":"move")+" entry "+
entry.path,e);throw e;}} 
if(aCopy)
return;
 try{setFilePermissions(aDirectory,FileUtils.PERMS_DIRECTORY);aDirectory.remove(false);}
catch(e){logger.error("Failed to remove directory "+aDirectory.path,e);throw e;}
 
this._installedFiles.push({oldFile:aDirectory,newFile:newDir});},_installDirEntry:function(aDirEntry,aTargetDirectory,aCopy){let isDir=null;try{isDir=aDirEntry.isDirectory()&&!aDirEntry.isSymlink();}
catch(e){

if(e.result==Cr.NS_ERROR_FILE_TARGET_DOES_NOT_EXIST)
return;logger.error("Failure "+(aCopy?"copying":"moving")+" "+aDirEntry.path+" to "+aTargetDirectory.path);throw e;}
try{if(isDir)
this._installDirectory(aDirEntry,aTargetDirectory,aCopy);else
this._installFile(aDirEntry,aTargetDirectory,aCopy);}
catch(e){logger.error("Failure "+(aCopy?"copying":"moving")+" "+aDirEntry.path+" to "+aTargetDirectory.path);throw e;}},moveUnder:function(aFile,aTargetDirectory){try{this._installDirEntry(aFile,aTargetDirectory,false);}
catch(e){this.rollback();throw e;}},moveTo:function(aOldLocation,aNewLocation){try{let oldFile=aOldLocation.clone(),newFile=aNewLocation.clone();oldFile.moveTo(newFile.parent,newFile.leafName);this._installedFiles.push({oldFile:oldFile,newFile:newFile,isMoveTo:true});}
catch(e){this.rollback();throw e;}},copy:function(aFile,aTargetDirectory){try{this._installDirEntry(aFile,aTargetDirectory,true);}
catch(e){this.rollback();throw e;}},rollback:function(){while(this._installedFiles.length>0){let move=this._installedFiles.pop();if(move.isMoveTo){move.newFile.moveTo(move.oldDir.parent,move.oldDir.leafName);}
else if(move.newFile.isDirectory()&&!move.newFile.isSymlink()){let oldDir=move.oldFile.parent.clone();oldDir.append(move.oldFile.leafName);oldDir.create(Ci.nsIFile.DIRECTORY_TYPE,FileUtils.PERMS_DIRECTORY);}
else if(!move.oldFile){ move.newFile.remove(true);}
else{move.newFile.moveTo(move.oldFile.parent,null);}}
while(this._createdDirs.length>0)
recursiveRemove(this._createdDirs.pop());}};function applyBlocklistChanges(aOldAddon,aNewAddon,aOldAppVersion,aOldPlatformVersion){ aNewAddon.userDisabled=aOldAddon.userDisabled;aNewAddon.softDisabled=aOldAddon.softDisabled;let oldBlocklistState=Blocklist.getAddonBlocklistState(aOldAddon.wrapper,aOldAppVersion,aOldPlatformVersion);let newBlocklistState=Blocklist.getAddonBlocklistState(aNewAddon.wrapper);
 if(newBlocklistState==oldBlocklistState)
return;if(newBlocklistState==Blocklist.STATE_SOFTBLOCKED){if(aNewAddon.type!="theme"){
 aNewAddon.softDisabled=!aNewAddon.userDisabled;}
else{ aNewAddon.userDisabled=true;}}
else{ aNewAddon.softDisabled=false;}}
function canRunInSafeMode(aAddon){


if(aAddon._installLocation.name==KEY_APP_TEMPORARY)
return true;return aAddon._installLocation.name==KEY_APP_SYSTEM_DEFAULTS||aAddon._installLocation.name==KEY_APP_SYSTEM_ADDONS;}
function isUsableAddon(aAddon){ if(aAddon.type=="theme"&&aAddon.internalName==XPIProvider.defaultSkin)
return true;if(aAddon._installLocation.name==KEY_APP_SYSTEM_ADDONS&&aAddon.signedState!=AddonManager.SIGNEDSTATE_SYSTEM){return false;}


if(((aAddon._installLocation.scope!=AddonManager.SCOPE_SYSTEM||Services.appinfo.OS=="Darwin")&&aAddon._installLocation.name!=KEY_APP_SYSTEM_DEFAULTS&&aAddon._installLocation.name!=KEY_APP_TEMPORARY)&&mustSign(aAddon.type)){if(aAddon.signedState<=AddonManager.SIGNEDSTATE_MISSING)
return false;}
if(aAddon.blocklistState==Blocklist.STATE_BLOCKED)
return false;



if(aAddon.type=="experiment")
return true;if(AddonManager.checkUpdateSecurity&&!aAddon.providesUpdatesSecurely)
return false;if(!aAddon.isPlatformCompatible)
return false;if(AddonManager.checkCompatibility){if(!aAddon.isCompatible)
return false;}
else{let app=aAddon.matchingTargetApplication;if(!app)
return false;

if(aAddon.type=="theme"&&app.id==Services.appinfo.ID){try{let minCompatVersion=Services.prefs.getCharPref(PREF_CHECKCOMAT_THEMEOVERRIDE);if(minCompatVersion&&Services.vc.compare(minCompatVersion,app.maxVersion)>0){return false;}}catch(e){}}}
return true;}
XPCOMUtils.defineLazyServiceGetter(this,"gRDF","@mozilla.org/rdf/rdf-service;1",Ci.nsIRDFService);function EM_R(aProperty){return gRDF.GetResource(PREFIX_NS_EM+aProperty);}
function createAddonDetails(id,aAddon){return{id:id||aAddon.id,type:aAddon.type,version:aAddon.version,multiprocessCompatible:aAddon.multiprocessCompatible,runInSafeMode:aAddon.runInSafeMode,};}
function getExternalType(aType){if(aType in TYPE_ALIASES)
return TYPE_ALIASES[aType];return aType;}
function getManifestFileForDir(aDir){let file=aDir.clone();file.append(FILE_RDF_MANIFEST);if(file.exists()&&file.isFile())
return file;file.leafName=FILE_WEB_MANIFEST;if(file.exists()&&file.isFile())
return file;return null;}
function getManifestEntryForZipReader(aZipReader){if(aZipReader.hasEntry(FILE_RDF_MANIFEST))
return FILE_RDF_MANIFEST;if(aZipReader.hasEntry(FILE_WEB_MANIFEST))
return FILE_WEB_MANIFEST;return null;}
function getAllAliasesForTypes(aTypes){if(!aTypes)
return null; let typeset=new Set(aTypes);for(let alias of Object.keys(TYPE_ALIASES)){ typeset.delete(alias); if(typeset.has(TYPE_ALIASES[alias]))
typeset.add(alias);}
return[...typeset];}
function getRDFValue(aLiteral){if(aLiteral instanceof Ci.nsIRDFLiteral)
return aLiteral.Value;if(aLiteral instanceof Ci.nsIRDFResource)
return aLiteral.Value;if(aLiteral instanceof Ci.nsIRDFInt)
return aLiteral.Value;return null;}
function getRDFProperty(aDs,aResource,aProperty){return getRDFValue(aDs.GetTarget(aResource,EM_R(aProperty),true));}
var loadManifestFromWebManifest=Task.async(function*(aUri){
let uri=NetUtil.newURI("./",null,aUri);let extension=new ExtensionData(uri);let manifest=yield extension.readManifest();
let locales=yield extension.initAllLocales();if(extension.errors.length)
throw new Error("Extension is invalid");let bss=(manifest.browser_specific_settings&&manifest.browser_specific_settings.gecko)||(manifest.applications&&manifest.applications.gecko)||{};if(manifest.browser_specific_settings&&manifest.applications){logger.warn("Ignoring applications property in manifest");}
let addon=new AddonInternal();addon.id=bss.id;addon.version=manifest.version;addon.type="webextension";addon.unpack=false;addon.strictCompatibility=true;addon.bootstrap=true;addon.hasBinaryComponents=false;addon.multiprocessCompatible=true;addon.internalName=null;addon.updateURL=bss.update_url;addon.updateKey=null;addon.optionsURL=null;addon.optionsType=null;addon.aboutURL=null;if(manifest.options_ui){addon.optionsURL=extension.getURL(manifest.options_ui.page);if(manifest.options_ui.open_in_tab)
addon.optionsType=AddonManager.OPTIONS_TYPE_TAB;else
addon.optionsType=AddonManager.OPTIONS_TYPE_INLINE_BROWSER;} 
addon.iconURL=null;addon.icon64URL=null;addon.icons=manifest.icons||{};addon.applyBackgroundUpdates=AddonManager.AUTOUPDATE_DEFAULT;function getLocale(aLocale){
let rawManifest=extension.rawManifest;let result={name:extension.localize(rawManifest.name,aLocale),description:extension.localize(rawManifest.description,aLocale),creator:extension.localize(rawManifest.creator,aLocale),homepageURL:extension.localize(rawManifest.homepage_url,aLocale),developers:null,translators:null,contributors:null,locales:[aLocale],};return result;}
addon.defaultLocale=getLocale(extension.defaultLocale);addon.locales=Array.from(locales.keys(),getLocale);delete addon.defaultLocale.locales;addon.targetApplications=[{id:TOOLKIT_ID,minVersion:(bss.strict_min_version||AddonManagerPrivate.webExtensionsMinPlatformVersion),maxVersion:bss.strict_max_version||"*",}];addon.targetPlatforms=[];addon.userDisabled=false;addon.softDisabled=addon.blocklistState==Blocklist.STATE_SOFTBLOCKED;return addon;});function loadManifestFromRDF(aUri,aStream){function getPropertyArray(aDs,aSource,aProperty){let values=[];let targets=aDs.GetTargets(aSource,EM_R(aProperty),true);while(targets.hasMoreElements())
values.push(getRDFValue(targets.getNext()));return values;}
function readLocale(aDs,aSource,isDefault,aSeenLocales){let locale={};if(!isDefault){locale.locales=[];let targets=ds.GetTargets(aSource,EM_R("locale"),true);while(targets.hasMoreElements()){let localeName=getRDFValue(targets.getNext());if(!localeName){logger.warn("Ignoring empty locale in localized properties");continue;}
if(aSeenLocales.indexOf(localeName)!=-1){logger.warn("Ignoring duplicate locale in localized properties");continue;}
aSeenLocales.push(localeName);locale.locales.push(localeName);}
if(locale.locales.length==0){logger.warn("Ignoring localized properties with no listed locales");return null;}}
for(let prop of PROP_LOCALE_SINGLE){locale[prop]=getRDFProperty(aDs,aSource,prop);}
for(let prop of PROP_LOCALE_MULTI){ let props=getPropertyArray(aDs,aSource,prop.substring(0,prop.length-1));if(props.length>0)
locale[prop]=props;}
return locale;}
let rdfParser=Cc["@mozilla.org/rdf/xml-parser;1"].createInstance(Ci.nsIRDFXMLParser)
let ds=Cc["@mozilla.org/rdf/datasource;1?name=in-memory-datasource"].createInstance(Ci.nsIRDFDataSource);let listener=rdfParser.parseAsync(ds,aUri);let channel=Cc["@mozilla.org/network/input-stream-channel;1"].createInstance(Ci.nsIInputStreamChannel);channel.setURI(aUri);channel.contentStream=aStream;channel.QueryInterface(Ci.nsIChannel);channel.contentType="text/xml";listener.onStartRequest(channel,null);try{let pos=0;let count=aStream.available();while(count>0){listener.onDataAvailable(channel,null,aStream,pos,count);pos+=count;count=aStream.available();}
listener.onStopRequest(channel,null,Components.results.NS_OK);}
catch(e){listener.onStopRequest(channel,null,e.result);throw e;}
let root=gRDF.GetResource(RDFURI_INSTALL_MANIFEST_ROOT);let addon=new AddonInternal();for(let prop of PROP_METADATA){addon[prop]=getRDFProperty(ds,root,prop);}
addon.unpack=getRDFProperty(ds,root,"unpack")=="true";if(!addon.type){addon.type=addon.internalName?"theme":"extension";}
else{let type=addon.type;addon.type=null;for(let name in TYPES){if(TYPES[name]==type){addon.type=name;break;}}}
if(!(addon.type in TYPES))
throw new Error("Install manifest specifies unknown type: "+addon.type);if(addon.type!="multipackage"){if(!addon.id)
throw new Error("No ID in install manifest");if(!gIDTest.test(addon.id))
throw new Error("Illegal add-on ID "+addon.id);if(!addon.version)
throw new Error("No version in install manifest");}
addon.strictCompatibility=!(addon.type in COMPATIBLE_BY_DEFAULT_TYPES)||getRDFProperty(ds,root,"strictCompatibility")=="true";if(addon.type=="extension"){addon.bootstrap=getRDFProperty(ds,root,"bootstrap")=="true";addon.multiprocessCompatible=getRDFProperty(ds,root,"multiprocessCompatible")=="true";if(addon.optionsType&&addon.optionsType!=AddonManager.OPTIONS_TYPE_DIALOG&&addon.optionsType!=AddonManager.OPTIONS_TYPE_INLINE&&addon.optionsType!=AddonManager.OPTIONS_TYPE_TAB&&addon.optionsType!=AddonManager.OPTIONS_TYPE_INLINE_INFO){throw new Error("Install manifest specifies unknown type: "+addon.optionsType);}}
else{if(RESTARTLESS_TYPES.has(addon.type)){addon.bootstrap=true;}
 
addon.optionsURL=null;addon.optionsType=null;addon.aboutURL=null;if(addon.type=="theme"){if(!addon.internalName)
throw new Error("Themes must include an internalName property");addon.skinnable=getRDFProperty(ds,root,"skinnable")=="true";}}
addon.defaultLocale=readLocale(ds,root,true);let seenLocales=[];addon.locales=[];let targets=ds.GetTargets(root,EM_R("localized"),true);while(targets.hasMoreElements()){let target=targets.getNext().QueryInterface(Ci.nsIRDFResource);let locale=readLocale(ds,target,false,seenLocales);if(locale)
addon.locales.push(locale);}
let seenApplications=[];addon.targetApplications=[];targets=ds.GetTargets(root,EM_R("targetApplication"),true);while(targets.hasMoreElements()){let target=targets.getNext().QueryInterface(Ci.nsIRDFResource);let targetAppInfo={};for(let prop of PROP_TARGETAPP){targetAppInfo[prop]=getRDFProperty(ds,target,prop);}
if(!targetAppInfo.id||!targetAppInfo.minVersion||!targetAppInfo.maxVersion){logger.warn("Ignoring invalid targetApplication entry in install manifest");continue;}
if(seenApplications.indexOf(targetAppInfo.id)!=-1){logger.warn("Ignoring duplicate targetApplication entry for "+targetAppInfo.id+" in install manifest");continue;}
seenApplications.push(targetAppInfo.id);addon.targetApplications.push(targetAppInfo);}

let targetPlatforms=getPropertyArray(ds,root,"targetPlatform");addon.targetPlatforms=[];for(let targetPlatform of targetPlatforms){let platform={os:null,abi:null};let pos=targetPlatform.indexOf("_");if(pos!=-1){platform.os=targetPlatform.substring(0,pos);platform.abi=targetPlatform.substring(pos+1);}
else{platform.os=targetPlatform;}
addon.targetPlatforms.push(platform);}


if(addon.type=="theme"){addon.userDisabled=!!LightweightThemeManager.currentTheme||addon.internalName!=XPIProvider.selectedSkin;}
else if(addon.type=="experiment"){
addon.userDisabled=true;}
else{addon.userDisabled=false;}
addon.softDisabled=addon.blocklistState==Blocklist.STATE_SOFTBLOCKED;addon.applyBackgroundUpdates=AddonManager.AUTOUPDATE_DEFAULT;
if(addon.type=="experiment"){addon.applyBackgroundUpdates=AddonManager.AUTOUPDATE_DISABLE;addon.updateURL=null;addon.updateKey=null;} 
addon.icons={};return addon;}
function defineSyncGUID(aAddon){let storage=Services.storage; Object.defineProperty(aAddon,"syncGUID",{get:()=>{let rng=Cc["@mozilla.org/security/random-generator;1"].createInstance(Ci.nsIRandomGenerator);let bytes=rng.generateRandomBytes(9);let byte_string=bytes.map(byte=>String.fromCharCode(byte)).join(""); let guid=btoa(byte_string).replace(/\+/g,'-').replace(/\//g,'_');delete aAddon.syncGUID;aAddon.syncGUID=guid;return guid;},set:(val)=>{delete aAddon.syncGUID;aAddon.syncGUID=val;},configurable:true,enumerable:true,});}
var loadManifestFromDir=Task.async(function*(aDir,aInstallLocation){function getFileSize(aFile){if(aFile.isSymlink())
return 0;if(!aFile.isDirectory())
return aFile.fileSize;let size=0;let entries=aFile.directoryEntries.QueryInterface(Ci.nsIDirectoryEnumerator);let entry;while((entry=entries.nextFile))
size+=getFileSize(entry);entries.close();return size;}
function loadFromRDF(aUri){let fis=Cc["@mozilla.org/network/file-input-stream;1"].createInstance(Ci.nsIFileInputStream);fis.init(aUri.file,-1,-1,false);let bis=Cc["@mozilla.org/network/buffered-input-stream;1"].createInstance(Ci.nsIBufferedInputStream);bis.init(fis,4096);try{var addon=loadManifestFromRDF(aUri,bis);}finally{bis.close();fis.close();}
let iconFile=aDir.clone();iconFile.append("icon.png");if(iconFile.exists()){addon.icons[32]="icon.png";addon.icons[48]="icon.png";}
let icon64File=aDir.clone();icon64File.append("icon64.png");if(icon64File.exists()){addon.icons[64]="icon64.png";}
let file=aDir.clone();file.append("chrome.manifest");let chromeManifest=ChromeManifestParser.parseSync(Services.io.newFileURI(file));addon.hasBinaryComponents=ChromeManifestParser.hasType(chromeManifest,"binary-component");return addon;}
let file=getManifestFileForDir(aDir);if(!file){throw new Error("Directory "+aDir.path+" does not contain a valid "+"install manifest");}
let uri=Services.io.newFileURI(file).QueryInterface(Ci.nsIFileURL);let addon;if(file.leafName==FILE_WEB_MANIFEST){addon=yield loadManifestFromWebManifest(uri);if(!addon.id){if(aInstallLocation==TemporaryInstallLocation){let id=Cc["@mozilla.org/uuid-generator;1"].getService(Ci.nsIUUIDGenerator).generateUUID().toString();logger.info(`Generated temporary id ${id} for ${aDir.path}`);addon.id=id;}else{addon.id=aDir.leafName;}}}else{addon=loadFromRDF(uri);}
addon._sourceBundle=aDir.clone();addon._installLocation=aInstallLocation;addon.size=getFileSize(aDir);addon.signedState=yield verifyDirSignedState(aDir,addon).then(({signedState})=>signedState);addon.appDisabled=!isUsableAddon(addon);defineSyncGUID(addon);return addon;});var loadManifestFromZipReader=Task.async(function*(aZipReader,aInstallLocation){function loadFromRDF(aUri){let zis=aZipReader.getInputStream(entry);let bis=Cc["@mozilla.org/network/buffered-input-stream;1"].createInstance(Ci.nsIBufferedInputStream);bis.init(zis,4096);try{var addon=loadManifestFromRDF(aUri,bis);}finally{bis.close();zis.close();}
if(aZipReader.hasEntry("icon.png")){addon.icons[32]="icon.png";addon.icons[48]="icon.png";}
if(aZipReader.hasEntry("icon64.png")){addon.icons[64]="icon64.png";}
if(addon.unpack){let uri=buildJarURI(aZipReader.file,"chrome.manifest");let chromeManifest=ChromeManifestParser.parseSync(uri);addon.hasBinaryComponents=ChromeManifestParser.hasType(chromeManifest,"binary-component");}else{addon.hasBinaryComponents=false;}
return addon;}
let entry=getManifestEntryForZipReader(aZipReader);if(!entry){throw new Error("File "+aZipReader.file.path+" does not contain a valid "+"install manifest");}
let uri=buildJarURI(aZipReader.file,entry);let isWebExtension=(entry==FILE_WEB_MANIFEST);let addon=isWebExtension?yield loadManifestFromWebManifest(uri):loadFromRDF(uri);addon._sourceBundle=aZipReader.file;addon._installLocation=aInstallLocation;addon.size=0;let entries=aZipReader.findEntries(null);while(entries.hasMore())
addon.size+=aZipReader.getEntry(entries.getNext()).realSize;let{signedState,cert}=yield verifyZipSignedState(aZipReader.file,addon);addon.signedState=signedState;if(isWebExtension&&!addon.id&&cert){addon.id=cert.commonName;}
addon.appDisabled=!isUsableAddon(addon);defineSyncGUID(addon);return addon;});var loadManifestFromZipFile=Task.async(function*(aXPIFile,aInstallLocation){let zipReader=Cc["@mozilla.org/libjar/zip-reader;1"].createInstance(Ci.nsIZipReader);try{zipReader.open(aXPIFile);

let manifest=yield loadManifestFromZipReader(zipReader,aInstallLocation);return manifest;}
finally{zipReader.close();}});function loadManifestFromFile(aFile,aInstallLocation){if(aFile.isFile())
return loadManifestFromZipFile(aFile,aInstallLocation);else
return loadManifestFromDir(aFile,aInstallLocation);}
function syncLoadManifestFromFile(aFile,aInstallLocation){let success=undefined;let result=null;loadManifestFromFile(aFile,aInstallLocation).then(val=>{success=true;result=val;},val=>{success=false;result=val});let thread=Services.tm.currentThread;while(success===undefined)
thread.processNextEvent(true);if(!success)
throw result;return result;}
function getURIForResourceInFile(aFile,aPath){if(aFile.isDirectory()){let resource=aFile.clone();if(aPath)
aPath.split("/").forEach(part=>resource.append(part));return NetUtil.newURI(resource);}
return buildJarURI(aFile,aPath);}
function buildJarURI(aJarfile,aPath){let uri=Services.io.newFileURI(aJarfile);uri="jar:"+uri.spec+"!/"+aPath;return NetUtil.newURI(uri);}
function flushJarCache(aJarFile){Services.obs.notifyObservers(aJarFile,"flush-cache-entry",null);Services.mm.broadcastAsyncMessage(MSG_JAR_FLUSH,aJarFile.path);}
function flushChromeCaches(){Services.obs.notifyObservers(null,"startupcache-invalidate",null); Services.obs.notifyObservers(null,"message-manager-flush-caches",null); Services.mm.broadcastAsyncMessage(MSG_MESSAGE_MANAGER_CACHES_FLUSH,null);}
function getTemporaryFile(){let file=FileUtils.getDir(KEY_TEMPDIR,[]);let random=Math.random().toString(36).replace(/0./,'').substr(-3);file.append("tmp-"+random+".xpi");file.createUnique(Ci.nsIFile.NORMAL_FILE_TYPE,FileUtils.PERMS_FILE);return file;}
function verifyZipSigning(aZip,aCertificate){var count=0;var entries=aZip.findEntries(null);while(entries.hasMore()){var entry=entries.getNext();if(entry.substr(0,9)=="META-INF/")
continue;if(entry.substr(-1)=="/")
continue;count++;var entryCertificate=aZip.getSigningCert(entry);if(!entryCertificate||!aCertificate.equals(entryCertificate)){return false;}}
return aZip.manifestEntriesCount==count;}
function getSignedStatus(aRv,aCert,aAddonID){let expectedCommonName=aAddonID;if(aAddonID&&aAddonID.length>64){let converter=Cc["@mozilla.org/intl/scriptableunicodeconverter"].createInstance(Ci.nsIScriptableUnicodeConverter);converter.charset="UTF-8";let data=converter.convertToByteArray(aAddonID,{});let crypto=Cc["@mozilla.org/security/hash;1"].createInstance(Ci.nsICryptoHash);crypto.init(Ci.nsICryptoHash.SHA256);crypto.update(data,data.length);expectedCommonName=getHashStringForCrypto(crypto);}
switch(aRv){case Cr.NS_OK:if(expectedCommonName&&expectedCommonName!=aCert.commonName)
return AddonManager.SIGNEDSTATE_BROKEN;let hotfixID=Preferences.get(PREF_EM_HOTFIX_ID,undefined);if(hotfixID&&hotfixID==aAddonID&&Preferences.get(PREF_EM_CERT_CHECKATTRIBUTES,false)){ try{CertUtils.validateCert(aCert,CertUtils.readCertPrefs(PREF_EM_HOTFIX_CERTS));}
catch(e){logger.warn("The hotfix add-on was not signed by the expected "+"certificate and so will not be installed.",e);return AddonManager.SIGNEDSTATE_BROKEN;}}
if(aCert.organizationalUnit=="Mozilla Components")
return AddonManager.SIGNEDSTATE_SYSTEM;return/preliminary/i.test(aCert.organizationalUnit)?AddonManager.SIGNEDSTATE_PRELIMINARY:AddonManager.SIGNEDSTATE_SIGNED;break;case Cr.NS_ERROR_SIGNED_JAR_NOT_SIGNED:return AddonManager.SIGNEDSTATE_MISSING;break;case Cr.NS_ERROR_SIGNED_JAR_MANIFEST_INVALID:case Cr.NS_ERROR_SIGNED_JAR_ENTRY_INVALID:case Cr.NS_ERROR_SIGNED_JAR_ENTRY_MISSING:case Cr.NS_ERROR_SIGNED_JAR_ENTRY_TOO_LARGE:case Cr.NS_ERROR_SIGNED_JAR_UNSIGNED_ENTRY:case Cr.NS_ERROR_SIGNED_JAR_MODIFIED_ENTRY:return AddonManager.SIGNEDSTATE_BROKEN;break;default:
return AddonManager.SIGNEDSTATE_UNKNOWN;break;}}
function shouldVerifySignedState(aAddon){ if(aAddon._installLocation.name==KEY_APP_SYSTEM_ADDONS)
return true; if(aAddon._installLocation.name==KEY_APP_SYSTEM_DEFAULTS)
return false; let hotfixID=Preferences.get(PREF_EM_HOTFIX_ID,undefined);if(hotfixID&&aAddon.id==hotfixID)
return true;
return ADDON_SIGNING&&SIGNED_TYPES.has(aAddon.type);}
let gCertDB=Cc["@mozilla.org/security/x509certdb;1"].getService(Ci.nsIX509CertDB);function verifyZipSignedState(aFile,aAddon){if(!shouldVerifySignedState(aAddon))
return Promise.resolve({signedState:AddonManager.SIGNEDSTATE_NOT_REQUIRED,cert:null});let root=Ci.nsIX509CertDB.AddonsPublicRoot;if(!REQUIRE_SIGNING&&Preferences.get(PREF_XPI_SIGNATURES_DEV_ROOT,false))
root=Ci.nsIX509CertDB.AddonsStageRoot;return new Promise(resolve=>{let callback={openSignedAppFileFinished:function(aRv,aZipReader,aCert){if(aZipReader)
aZipReader.close();resolve({signedState:getSignedStatus(aRv,aCert,aAddon.id),cert:aCert});}};
callback.wrappedJSObject=callback;gCertDB.openSignedAppFileAsync(root,aFile,callback);});}
function verifyDirSignedState(aDir,aAddon){if(!shouldVerifySignedState(aAddon))
return Promise.resolve({signedState:AddonManager.SIGNEDSTATE_NOT_REQUIRED,cert:null,});let root=Ci.nsIX509CertDB.AddonsPublicRoot;if(!REQUIRE_SIGNING&&Preferences.get(PREF_XPI_SIGNATURES_DEV_ROOT,false))
root=Ci.nsIX509CertDB.AddonsStageRoot;return new Promise(resolve=>{let callback={verifySignedDirectoryFinished:function(aRv,aCert){resolve({signedState:getSignedStatus(aRv,aCert,aAddon.id),cert:null,});}};
callback.wrappedJSObject=callback;gCertDB.verifySignedDirectoryAsync(root,aDir,callback);});}
function verifyBundleSignedState(aBundle,aAddon){let promise=aBundle.isFile()?verifyZipSignedState(aBundle,aAddon):verifyDirSignedState(aBundle,aAddon);return promise.then(({signedState})=>signedState);}
function escapeAddonURI(aAddon,aUri,aUpdateType,aAppVersion)
{let uri=AddonManager.escapeAddonURI(aAddon,aUri,aAppVersion); if(aUpdateType)
uri=uri.replace(/%UPDATE_TYPE%/g,aUpdateType);

 let app=aAddon.matchingTargetApplication;if(app)
var maxVersion=app.maxVersion;else
maxVersion="";uri=uri.replace(/%ITEM_MAXAPPVERSION%/g,maxVersion);let compatMode="normal";if(!AddonManager.checkCompatibility)
compatMode="ignore";else if(AddonManager.strictCompatibility)
compatMode="strict";uri=uri.replace(/%COMPATIBILITY_MODE%/g,compatMode);return uri;}
function removeAsync(aFile){return Task.spawn(function*(){let info=null;try{info=yield OS.File.stat(aFile.path);if(info.isDir)
yield OS.File.removeDir(aFile.path);else
yield OS.File.remove(aFile.path);}
catch(e){if(!(e instanceof OS.File.Error)||!e.becauseNoSuchFile)
throw e; return;}});}
function recursiveRemove(aFile){let isDir=null;try{isDir=aFile.isDirectory();}
catch(e){

if(e.result==Cr.NS_ERROR_FILE_TARGET_DOES_NOT_EXIST)
return;if(e.result==Cr.NS_ERROR_FILE_NOT_FOUND)
return;throw e;}
setFilePermissions(aFile,isDir?FileUtils.PERMS_DIRECTORY:FileUtils.PERMS_FILE);try{aFile.remove(true);return;}
catch(e){if(!aFile.isDirectory()||aFile.isSymlink()){logger.error("Failed to remove file "+aFile.path,e);throw e;}}



let entries=getDirectoryEntries(aFile,true);entries.forEach(recursiveRemove);try{aFile.remove(true);}
catch(e){logger.error("Failed to remove empty directory "+aFile.path,e);throw e;}}
function recursiveLastModifiedTime(aFile){try{let modTime=aFile.lastModifiedTime;let fileName=aFile.leafName;if(aFile.isFile())
return[fileName,modTime,1];if(aFile.isDirectory()){let entries=aFile.directoryEntries.QueryInterface(Ci.nsIDirectoryEnumerator);let entry;let totalItems=1;while((entry=entries.nextFile)){let[subName,subTime,items]=recursiveLastModifiedTime(entry);totalItems+=items;if(subTime>modTime){modTime=subTime;fileName=subName;}}
entries.close();return[fileName,modTime,totalItems];}}
catch(e){logger.warn("Problem getting last modified time for "+aFile.path,e);}
return["",0,0];}
function getDirectoryEntries(aDir,aSortEntries){let dirEnum;try{dirEnum=aDir.directoryEntries.QueryInterface(Ci.nsIDirectoryEnumerator);let entries=[];while(dirEnum.hasMoreElements())
entries.push(dirEnum.nextFile);if(aSortEntries){entries.sort(function(a,b){return a.path>b.path?-1:1;});}
return entries}
catch(e){logger.warn("Can't iterate directory "+aDir.path,e);return[];}
finally{if(dirEnum){dirEnum.close();}}}
function makeSafe(aFunction){return function(...aArgs){try{return aFunction(...aArgs);}
catch(ex){logger.warn("XPIProvider callback failed",ex);}
return undefined;}}
function recordAddonTelemetry(aAddon){let locale=aAddon.defaultLocale;if(locale){if(locale.name)
XPIProvider.setTelemetry(aAddon.id,"name",locale.name);if(locale.creator)
XPIProvider.setTelemetry(aAddon.id,"creator",locale.creator);}}
function XPIState(saved){for(let[short,long]of XPIState.prototype.fields){if(short in saved){this[long]=saved[short];}}}
XPIState.prototype={fields:[['d','descriptor'],['e','enabled'],['v','version'],['st','scanTime'],['mt','manifestTime']],get mtime(){if(!this.enabled&&('manifestTime'in this)&&this.manifestTime>this.scanTime){return this.manifestTime;}
return this.scanTime;},toJSON(){let json={};for(let[short,long]of XPIState.prototype.fields){if(long in this){json[short]=this[long];}}
return json;},getModTime(aFile,aId){let changed=false;let scanStarted=Cu.now();if(!('scanTime'in this)||this.enabled){logger.debug('getModTime: Recursive scan of '+aId);let[modFile,modTime,items]=recursiveLastModifiedTime(aFile);XPIProvider._mostRecentlyModifiedFile[aId]=modFile;XPIProvider.setTelemetry(aId,"scan_items",items);if(modTime!=this.scanTime){this.scanTime=modTime;changed=true;}}


try{let maniFile=getManifestFileForDir(aFile);if(!(aId in XPIProvider._mostRecentlyModifiedFile)){XPIProvider._mostRecentlyModifiedFile[aId]=maniFile.leafName;}
let maniTime=maniFile.lastModifiedTime;if(maniTime!=this.manifestTime){this.manifestTime=maniTime;changed=true;}}catch(e){ delete this.manifestTime;try{let dtime=aFile.lastModifiedTime;if(dtime!=this.scanTime){changed=true;this.scanTime=dtime;}}catch(e){logger.warn("Can't get modified time of ${file}: ${e}",{file:aFile.path,e:e});changed=true;this.scanTime=0;}} 
XPIProvider.setTelemetry(aId,"scan_MS",Math.round(Cu.now()-scanStarted));return changed;},syncWithDB(aDBAddon,aUpdated=false){logger.debug("Updating XPIState for "+JSON.stringify(aDBAddon));
let mustGetMod=(aDBAddon.visible&&!aDBAddon.disabled&&!this.enabled);this.enabled=(aDBAddon.visible&&!aDBAddon.disabled);this.version=aDBAddon.version;if(aUpdated||mustGetMod){this.getModTime(new nsIFile(this.descriptor),aDBAddon.id);if(this.scanTime!=aDBAddon.updateDate){aDBAddon.updateDate=this.scanTime;XPIDatabase.saveChanges();}}},};
function SerializableMap(){let m=new Map();m.toJSON=function(){let out={}
for(let[key,val]of m){out[key]=val;}
return out;};return m;}
this.XPIStates={db:null,get size(){if(!this.db){return 0;}
let count=0;for(let location of this.db.values()){count+=location.size;}
return count;},loadExtensionState(){let state={};Preferences.reset(PREF_INSTALL_CACHE);let cache=Preferences.get(PREF_XPI_STATE,"{}");try{state=JSON.parse(cache);}catch(e){logger.warn("Error parsing extensions.xpiState ${state}: ${error}",{state:cache,error:e});}
logger.debug("Loaded add-on state from prefs: ${}",state);return state;},getInstallState(){let oldState=this.loadExtensionState();let changed=false;this.db=new SerializableMap();for(let location of XPIProvider.installLocations){let addons=location.getAddonLocations();let foundAddons=new SerializableMap();let locState={};if(location.name in oldState){locState=oldState[location.name];delete oldState[location.name];}
for(let[id,file]of addons){if(!(id in locState)){logger.debug("New add-on ${id} in ${location}",{id:id,location:location.name});let xpiState=new XPIState({d:file.persistentDescriptor});changed=xpiState.getModTime(file,id)||changed;foundAddons.set(id,xpiState);}else{let xpiState=new XPIState(locState[id]); delete locState[id];changed=xpiState.getModTime(file,id)||changed;if(file.persistentDescriptor!=xpiState.descriptor){xpiState.descriptor=file.persistentDescriptor;changed=true;}
if(changed){logger.debug("Changed add-on ${id} in ${location}",{id:id,location:location.name});}
else{logger.debug("Existing add-on ${id} in ${location}",{id:id,location:location.name});}
foundAddons.set(id,xpiState);}
XPIProvider.setTelemetry(id,"location",location.name);}
for(let id in locState){changed=true;break;}
if(foundAddons.size!=0){this.db.set(location.name,foundAddons);}}

for(let location in oldState){changed=true;break;}
logger.debug("getInstallState changed: ${rv}, state: ${state}",{rv:changed,state:this.db});return changed;},getLocation(aLocation){return this.db.get(aLocation);},getAddon(aLocation,aId){let location=this.db.get(aLocation);if(!location){return null;}
return location.get(aId);},findAddon(aId){
for(let[name,location]of this.db){if(location.has(aId)){return[name,location.get(aId)];}}
return[undefined,undefined];},addAddon(aAddon){let location=this.db.get(aAddon.location);if(!location){location=new SerializableMap();this.db.set(aAddon.location,location);}
logger.debug("XPIStates adding add-on ${id} in ${location}: ${descriptor}",aAddon);let xpiState=new XPIState({d:aAddon.descriptor});location.set(aAddon.id,xpiState);xpiState.syncWithDB(aAddon,true);XPIProvider.setTelemetry(aAddon.id,"location",aAddon.location);},save(){let cache=JSON.stringify(this.db);Services.prefs.setCharPref(PREF_XPI_STATE,cache);},removeAddon(aLocation,aId){logger.debug("Removing XPIState for "+aLocation+":"+aId);let location=this.db.get(aLocation);if(!location){return;}
location.delete(aId);if(location.size==0){this.db.delete(aLocation);}
this.save();},};this.XPIProvider={get name(){return"XPIProvider";}, installLocations:null, installLocationsByName:null, installs:null, defaultSkin:"classic/1.0", currentSkin:null,

 selectedSkin:null, minCompatibleAppVersion:null, minCompatiblePlatformVersion:null, bootstrappedAddons:{}, activeAddons:new Map(), extensionsActive:false,
 allAppGlobal:true, enabledAddons:null, runPhase:XPI_STARTING,
_mostRecentlyModifiedFile:{}, _telemetryDetails:{}, _addonFileMap:new Map(), _toolboxProcessLoaded:false,_closing:false,setTelemetry:function(aId,aName,aValue){if(!this._telemetryDetails[aId])
this._telemetryDetails[aId]={};this._telemetryDetails[aId][aName]=aValue;},_inProgress:[],doing:function(aCancellable){this._inProgress.push(aCancellable);},done:function(aCancellable){let i=this._inProgress.indexOf(aCancellable);if(i!=-1){this._inProgress.splice(i,1);return true;}
return false;},cancelAll:function(){ while(this._inProgress.length>0){let c=this._inProgress.shift();try{c.cancel();}
catch(e){logger.warn("Cancel failed",e);}}},_addURIMapping:function(aID,aFile){logger.info("Mapping "+aID+" to "+aFile.path);this._addonFileMap.set(aID,aFile.path);let service=Cc["@mozilla.org/addon-path-service;1"].getService(Ci.amIAddonPathService);service.insertPath(aFile.path,aID);},_resolveURIToFile:function(aURI){switch(aURI.scheme){case"jar":case"file":if(aURI instanceof Ci.nsIJARURI){return this._resolveURIToFile(aURI.JARFile);}
return aURI;case"chrome":aURI=ChromeRegistry.convertChromeURL(aURI);return this._resolveURIToFile(aURI);case"resource":aURI=Services.io.newURI(ResProtocolHandler.resolveURI(aURI),null,null);return this._resolveURIToFile(aURI);case"view-source":aURI=Services.io.newURI(aURI.path,null,null);return this._resolveURIToFile(aURI);case"about":if(aURI.spec=="about:blank"){ return null;}
let chan;try{chan=NetUtil.newChannel({uri:aURI,loadUsingSystemPrincipal:true});}
catch(ex){return null;} 
if(chan.URI.equals(aURI)){return null;}


return this._resolveURIToFile(chan.URI.clone());default:return null;}},startup:function(aAppChanged,aOldAppVersion,aOldPlatformVersion){function addDirectoryInstallLocation(aName,aKey,aPaths,aScope,aLocked){try{var dir=FileUtils.getDir(aKey,aPaths);}
catch(e){ logger.debug("Skipping unavailable install location "+aName);return;}
try{var location=aLocked?new DirectoryInstallLocation(aName,dir,aScope):new MutableDirectoryInstallLocation(aName,dir,aScope);}
catch(e){logger.warn("Failed to add directory install location "+aName,e);return;}
XPIProvider.installLocations.push(location);XPIProvider.installLocationsByName[location.name]=location;}
function addSystemAddonInstallLocation(aName,aKey,aPaths,aScope){try{var dir=FileUtils.getDir(aKey,aPaths);}
catch(e){ logger.debug("Skipping unavailable install location "+aName);return;}
try{var location=new SystemAddonInstallLocation(aName,dir,aScope,aAppChanged!==false);}
catch(e){logger.warn("Failed to add system add-on install location "+aName,e);return;}
XPIProvider.installLocations.push(location);XPIProvider.installLocationsByName[location.name]=location;}
function addRegistryInstallLocation(aName,aRootkey,aScope){try{var location=new WinRegInstallLocation(aName,aRootkey,aScope);}
catch(e){logger.warn("Failed to add registry install location "+aName,e);return;}
XPIProvider.installLocations.push(location);XPIProvider.installLocationsByName[location.name]=location;}
try{AddonManagerPrivate.recordTimestamp("XPI_startup_begin");logger.debug("startup");this.runPhase=XPI_STARTING;this.installs=[];this.installLocations=[];this.installLocationsByName={}; this._shutdownError=null; this._telemetryDetails={}; AddonManagerPrivate.setTelemetryDetails("XPI",this._telemetryDetails);let hasRegistry=("nsIWindowsRegKey"in Ci);let enabledScopes=Preferences.get(PREF_EM_ENABLED_SCOPES,AddonManager.SCOPE_ALL); XPIProvider.installLocations.push(TemporaryInstallLocation);XPIProvider.installLocationsByName[TemporaryInstallLocation.name]=TemporaryInstallLocation; addDirectoryInstallLocation(KEY_APP_PROFILE,KEY_PROFILEDIR,[DIR_EXTENSIONS],AddonManager.SCOPE_PROFILE,false);addSystemAddonInstallLocation(KEY_APP_SYSTEM_ADDONS,KEY_PROFILEDIR,[DIR_SYSTEM_ADDONS],AddonManager.SCOPE_PROFILE);addDirectoryInstallLocation(KEY_APP_SYSTEM_DEFAULTS,KEY_APP_FEATURES,[],AddonManager.SCOPE_PROFILE,true);if(enabledScopes&AddonManager.SCOPE_USER){addDirectoryInstallLocation(KEY_APP_SYSTEM_USER,"XREUSysExt",[Services.appinfo.ID],AddonManager.SCOPE_USER,true);if(hasRegistry){addRegistryInstallLocation("winreg-app-user",Ci.nsIWindowsRegKey.ROOT_KEY_CURRENT_USER,AddonManager.SCOPE_USER);}}
addDirectoryInstallLocation(KEY_APP_GLOBAL,KEY_ADDON_APP_DIR,[DIR_EXTENSIONS],AddonManager.SCOPE_APPLICATION,true);if(enabledScopes&AddonManager.SCOPE_SYSTEM){addDirectoryInstallLocation(KEY_APP_SYSTEM_SHARE,"XRESysSExtPD",[Services.appinfo.ID],AddonManager.SCOPE_SYSTEM,true);addDirectoryInstallLocation(KEY_APP_SYSTEM_LOCAL,"XRESysLExtPD",[Services.appinfo.ID],AddonManager.SCOPE_SYSTEM,true);if(hasRegistry){addRegistryInstallLocation("winreg-app-global",Ci.nsIWindowsRegKey.ROOT_KEY_LOCAL_MACHINE,AddonManager.SCOPE_SYSTEM);}}
let defaultPrefs=new Preferences({defaultBranch:true});this.defaultSkin=defaultPrefs.get(PREF_GENERAL_SKINS_SELECTEDSKIN,"classic/1.0");this.currentSkin=Preferences.get(PREF_GENERAL_SKINS_SELECTEDSKIN,this.defaultSkin);this.selectedSkin=this.currentSkin;this.applyThemeChange();this.minCompatibleAppVersion=Preferences.get(PREF_EM_MIN_COMPAT_APP_VERSION,null);this.minCompatiblePlatformVersion=Preferences.get(PREF_EM_MIN_COMPAT_PLATFORM_VERSION,null);this.enabledAddons="";Services.prefs.addObserver(PREF_EM_MIN_COMPAT_APP_VERSION,this,false);Services.prefs.addObserver(PREF_EM_MIN_COMPAT_PLATFORM_VERSION,this,false);if(!REQUIRE_SIGNING)
Services.prefs.addObserver(PREF_XPI_SIGNATURES_REQUIRED,this,false);Services.obs.addObserver(this,NOTIFICATION_FLUSH_PERMISSIONS,false);
if(ResProtocolHandler.hasSubstitution("devtools")){if(Cu.isModuleLoaded("resource://devtools/client/framework/ToolboxProcess.jsm")){
 this._toolboxProcessLoaded=true;BrowserToolboxProcess.on("connectionchange",this.onDebugConnectionChange.bind(this));}else{ Services.obs.addObserver(this,NOTIFICATION_TOOLBOXPROCESS_LOADED,false);}}
let flushCaches=this.checkForChanges(aAppChanged,aOldAppVersion,aOldPlatformVersion); this.applyThemeChange();AddonManagerPrivate.markProviderSafe(this);if(aAppChanged&&!this.allAppGlobal&&Preferences.get(PREF_EM_SHOW_MISMATCH_UI,true)){let addonsToUpdate=this.shouldForceUpdateCheck(aAppChanged);if(addonsToUpdate){this.showUpgradeUI(addonsToUpdate);flushCaches=true;}}
if(flushCaches){Services.obs.notifyObservers(null,"startupcache-invalidate",null);


 Services.obs.notifyObservers(null,"chrome-flush-skin-caches",null);Services.obs.notifyObservers(null,"chrome-flush-caches",null);}
this.enabledAddons=Preferences.get(PREF_EM_ENABLED_ADDONS,"");if("nsICrashReporter"in Ci&&Services.appinfo instanceof Ci.nsICrashReporter){try{Services.appinfo.annotateCrashReport("Theme",this.currentSkin);}catch(e){}
try{Services.appinfo.annotateCrashReport("EMCheckCompatibility",AddonManager.checkCompatibility);}catch(e){}
this.addAddonsToCrashReporter();}
try{AddonManagerPrivate.recordTimestamp("XPI_bootstrap_addons_begin");for(let id in this.bootstrappedAddons){try{let file=Cc["@mozilla.org/file/local;1"].createInstance(Ci.nsIFile);file.persistentDescriptor=this.bootstrappedAddons[id].descriptor;let reason=BOOTSTRAP_REASONS.APP_STARTUP;
 if(AddonManager.getStartupChanges(AddonManager.STARTUP_CHANGE_INSTALLED).indexOf(id)!==-1)
reason=BOOTSTRAP_REASONS.ADDON_INSTALL;this.callBootstrapMethod(createAddonDetails(id,this.bootstrappedAddons[id]),file,"startup",reason);}
catch(e){logger.error("Failed to load bootstrap addon "+id+" from "+
this.bootstrappedAddons[id].descriptor,e);}}
AddonManagerPrivate.recordTimestamp("XPI_bootstrap_addons_end");}
catch(e){logger.error("bootstrap startup failed",e);AddonManagerPrivate.recordException("XPI-BOOTSTRAP","startup failed",e);}
 
Services.obs.addObserver({observe:function(aSubject,aTopic,aData){XPIProvider._closing=true;for(let id in XPIProvider.bootstrappedAddons){

if(!XPIProvider.activeAddons.has(id))
continue;let file=Cc["@mozilla.org/file/local;1"].createInstance(Ci.nsIFile);file.persistentDescriptor=XPIProvider.bootstrappedAddons[id].descriptor;let addon=createAddonDetails(id,XPIProvider.bootstrappedAddons[id]);
if(XPIProvider.bootstrappedAddons[id].disable){XPIProvider.callBootstrapMethod(addon,file,"shutdown",BOOTSTRAP_REASONS.ADDON_DISABLE);delete XPIProvider.bootstrappedAddons[id];}
else{XPIProvider.callBootstrapMethod(addon,file,"shutdown",BOOTSTRAP_REASONS.APP_SHUTDOWN);}}
Services.obs.removeObserver(this,"quit-application-granted");}},"quit-application-granted",false); Services.obs.addObserver({observe:function(aSubject,aTopic,aData){AddonManagerPrivate.recordTimestamp("XPI_finalUIStartup");XPIProvider.runPhase=XPI_AFTER_UI_STARTUP;Services.obs.removeObserver(this,"final-ui-startup");}},"final-ui-startup",false);AddonManagerPrivate.recordTimestamp("XPI_startup_end");this.extensionsActive=true;this.runPhase=XPI_BEFORE_UI_STARTUP;let timerManager=Cc["@mozilla.org/updates/timer-manager;1"].getService(Ci.nsIUpdateTimerManager);timerManager.registerTimer("xpi-signature-verification",()=>{this.verifySignatures();},XPI_SIGNATURE_CHECK_PERIOD);}
catch(e){logger.error("startup failed",e);AddonManagerPrivate.recordException("XPI","startup failed",e);}},shutdown:function(){logger.debug("shutdown"); this.cancelAll();this.bootstrappedAddons={};this.activeAddons.clear();this.enabledAddons=null;this.allAppGlobal=true;
 if(Preferences.get(PREF_PENDING_OPERATIONS,false)){AddonManagerPrivate.recordSimpleMeasure("XPIDB_pending_ops",1);XPIDatabase.updateActiveAddons();Services.prefs.setBoolPref(PREF_PENDING_OPERATIONS,!XPIDatabase.writeAddonsList());}
this.installs=null;this.installLocations=null;this.installLocationsByName=null; this.extensionsActive=false;this._addonFileMap.clear();if(gLazyObjectsLoaded){let done=XPIDatabase.shutdown();done.then(ret=>{logger.debug("Notifying XPI shutdown observers");Services.obs.notifyObservers(null,"xpi-provider-shutdown",null);},err=>{logger.debug("Notifying XPI shutdown observers");this._shutdownError=err;Services.obs.notifyObservers(null,"xpi-provider-shutdown",err);});return done;}
else{logger.debug("Notifying XPI shutdown observers");Services.obs.notifyObservers(null,"xpi-provider-shutdown",null);}
return undefined;},applyThemeChange:function(){if(!Preferences.get(PREF_DSS_SWITCHPENDING,false))
return; try{this.selectedSkin=Preferences.get(PREF_DSS_SKIN_TO_SELECT);Services.prefs.setCharPref(PREF_GENERAL_SKINS_SELECTEDSKIN,this.selectedSkin);Services.prefs.clearUserPref(PREF_DSS_SKIN_TO_SELECT);logger.debug("Changed skin to "+this.selectedSkin);this.currentSkin=this.selectedSkin;}
catch(e){logger.error("Error applying theme change",e);}
Services.prefs.clearUserPref(PREF_DSS_SWITCHPENDING);},shouldForceUpdateCheck:function(aAppChanged){AddonManagerPrivate.recordSimpleMeasure("XPIDB_metadata_age",AddonRepository.metadataAge());let startupChanges=AddonManager.getStartupChanges(AddonManager.STARTUP_CHANGE_DISABLED);logger.debug("shouldForceUpdateCheck startupChanges: "+startupChanges.toSource());AddonManagerPrivate.recordSimpleMeasure("XPIDB_startup_disabled",startupChanges.length);let forceUpdate=[];if(startupChanges.length>0){let addons=XPIDatabase.getAddons();for(let addon of addons){if((startupChanges.indexOf(addon.id)!=-1)&&(addon.permissions()&AddonManager.PERM_CAN_UPGRADE)&&!addon.isCompatible){logger.debug("shouldForceUpdateCheck: can upgrade disabled add-on "+addon.id);forceUpdate.push(addon.id);}}}
if(AddonRepository.isMetadataStale()){logger.debug("shouldForceUpdateCheck: metadata is stale");return forceUpdate;}
if(forceUpdate.length>0){return forceUpdate;}
return false;},showUpgradeUI:function(aAddonIDs){logger.debug("XPI_showUpgradeUI: "+aAddonIDs.toSource()); Services.startup.interrupted=true;var variant=Cc["@mozilla.org/variant;1"].createInstance(Ci.nsIWritableVariant);variant.setFromVariant(aAddonIDs);var features="chrome,centerscreen,dialog,titlebar,modal";var ww=Cc["@mozilla.org/embedcomp/window-watcher;1"].getService(Ci.nsIWindowWatcher);ww.openWindow(null,URI_EXTENSION_UPDATE_DIALOG,"",features,variant); Services.prefs.setBoolPref(PREF_PENDING_OPERATIONS,!XPIDatabase.writeAddonsList());},updateSystemAddons:Task.async(function*(){let systemAddonLocation=XPIProvider.installLocationsByName[KEY_APP_SYSTEM_ADDONS];if(!systemAddonLocation)
return; if(Services.appinfo.inSafeMode)
return; let url=Preferences.get(PREF_SYSTEM_ADDON_UPDATE_URL,null);if(!url){yield systemAddonLocation.cleanDirectories();return;}
url=UpdateUtils.formatUpdateURL(url);logger.info(`Starting system add-on update check from ${url}.`);let addonList=yield ProductAddonChecker.getProductAddonList(url);if(!addonList){logger.info("No system add-ons list was returned.");yield systemAddonLocation.cleanDirectories();return;}
addonList=new Map(addonList.map(spec=>[spec.id,{spec,path:null,addon:null}]));let getAddonsInLocation=(location)=>{return new Promise(resolve=>{XPIDatabase.getAddonsInLocation(location,resolve);});};let setMatches=(wanted,existing)=>{if(wanted.size!=existing.size)
return false;for(let[id,addon]of existing){let wantedInfo=wanted.get(id);if(!wantedInfo)
return false;if(wantedInfo.spec.version!=addon.version)
return false;}
return true;};let updatedAddons=addonMap(yield getAddonsInLocation(KEY_APP_SYSTEM_ADDONS));if(setMatches(addonList,updatedAddons)){logger.info("Retaining existing updated system add-ons.");yield systemAddonLocation.cleanDirectories();return;}

let defaultAddons=addonMap(yield getAddonsInLocation(KEY_APP_SYSTEM_DEFAULTS));if(setMatches(addonList,defaultAddons)){logger.info("Resetting system add-ons.");systemAddonLocation.resetAddonSet();yield systemAddonLocation.cleanDirectories();return;}
 
let downloadAddon=Task.async(function*(item){try{let sourceAddon=updatedAddons.get(item.spec.id);if(sourceAddon&&sourceAddon.version==item.spec.version){


try{let path=OS.Path.join(OS.Constants.Path.tmpDir,"tmpaddon");let unique=yield OS.File.openUnique(path);unique.file.close();yield OS.File.copy(sourceAddon._sourceBundle.path,unique.path);
yield OS.File.setDates(unique.path);item.path=unique.path;}
catch(e){logger.warn(`Failed make temporary copy of ${sourceAddon._sourceBundle.path}.`,e);}}
if(!item.path){item.path=yield ProductAddonChecker.downloadAddon(item.spec);}
item.addon=yield loadManifestFromFile(nsIFile(item.path),systemAddonLocation);}
catch(e){logger.error(`Failed to download system add-on ${item.spec.id}`,e);}});yield Promise.all(Array.from(addonList.values()).map(downloadAddon));
 let validateAddon=(item)=>{if(item.spec.id!=item.addon.id){logger.warn(`Downloaded system add-on expected to be ${item.spec.id} but was ${item.addon.id}.`);return false;}
if(item.spec.version!=item.addon.version){logger.warn(`Expected system add-on ${item.spec.id} to be version ${item.spec.version} but was ${item.addon.version}.`);return false;}
if(!systemAddonLocation.isValidAddon(item.addon))
return false;return true;}
try{if(!Array.from(addonList.values()).every(item=>item.path&&item.addon&&validateAddon(item))){throw new Error("Rejecting updated system add-on set that either could not "+"be downloaded or contained unusable add-ons.");} 
logger.info("Installing new system add-on set");yield systemAddonLocation.installAddonSet(Array.from(addonList.values()).map(a=>a.addon));}
finally{ logger.info("Deleting temporary files");for(let item of addonList.values()){if(item.path){try{yield OS.File.remove(item.path);}
catch(e){logger.warn(`Failed to remove temporary file ${item.path}.`,e);}}}
yield systemAddonLocation.cleanDirectories();}}),verifySignatures:function(){XPIDatabase.getAddonList(a=>true,(addons)=>{Task.spawn(function*(){let changes={enabled:[],disabled:[]};for(let addon of addons){ if(!addon._sourceBundle.exists())
continue;let signedState=yield verifyBundleSignedState(addon._sourceBundle,addon);if(signedState!=addon.signedState){addon.signedState=signedState;AddonManagerPrivate.callAddonListeners("onPropertyChanged",addon.wrapper,["signedState"]);}
let disabled=XPIProvider.updateAddonDisabledState(addon);if(disabled!==undefined)
changes[disabled?"disabled":"enabled"].push(addon.id);}
XPIDatabase.saveChanges();Services.obs.notifyObservers(null,"xpi-signature-changed",JSON.stringify(changes));}).then(null,err=>{logger.error("XPI_verifySignature: "+err);})});},persistBootstrappedAddons:function(){let filtered={};for(let id in this.bootstrappedAddons){let entry=this.bootstrappedAddons[id];if(entry.type=="experiment"){continue;}
filtered[id]=entry;}
Services.prefs.setCharPref(PREF_BOOTSTRAP_ADDONS,JSON.stringify(filtered));},addAddonsToCrashReporter:function(){if(!("nsICrashReporter"in Ci)||!(Services.appinfo instanceof Ci.nsICrashReporter))
return;
 if(Services.appinfo.inSafeMode)
return;let data=this.enabledAddons;for(let id in this.bootstrappedAddons){data+=(data?",":"")+encodeURIComponent(id)+":"+
encodeURIComponent(this.bootstrappedAddons[id].version);}
try{Services.appinfo.annotateCrashReport("Add-ons",data);}
catch(e){}
let TelemetrySession=Cu.import("resource://gre/modules/TelemetrySession.jsm",{}).TelemetrySession;TelemetrySession.setAddOns(data);},processPendingFileChanges:function(aManifests){let changed=false;for(let location of this.installLocations){aManifests[location.name]={}; if(location.locked)
continue;let stagingDir=location.getStagingDir();try{if(!stagingDir||!stagingDir.exists()||!stagingDir.isDirectory())
continue;}
catch(e){logger.warn("Failed to find staging directory",e);continue;}
let seenFiles=[];


let stagingDirEntries=getDirectoryEntries(stagingDir,true);for(let stageDirEntry of stagingDirEntries){let id=stageDirEntry.leafName;let isDir;try{isDir=stageDirEntry.isDirectory();}
catch(e){if(e.result!=Cr.NS_ERROR_FILE_TARGET_DOES_NOT_EXIST)
throw e;

continue;}
if(!isDir){if(id.substring(id.length-4).toLowerCase()==".xpi"){id=id.substring(0,id.length-4);}
else{if(id.substring(id.length-5).toLowerCase()!=".json"){logger.warn("Ignoring file: "+stageDirEntry.path);seenFiles.push(stageDirEntry.leafName);}
continue;}}
if(!gIDTest.test(id)){logger.warn("Ignoring directory whose name is not a valid add-on ID: "+
stageDirEntry.path);seenFiles.push(stageDirEntry.leafName);continue;}
changed=true;if(isDir){let manifest=getManifestFileForDir(stageDirEntry);
if(!manifest){logger.debug("Processing uninstall of "+id+" in "+location.name);try{let addonFile=location.getLocationForID(id);let addonToUninstall=syncLoadManifestFromFile(addonFile,location);if(addonToUninstall.bootstrap){this.callBootstrapMethod(addonToUninstall,addonToUninstall._sourceBundle,"uninstall",BOOTSTRAP_REASONS.ADDON_UNINSTALL);}}
catch(e){logger.warn("Failed to call uninstall for "+id,e);}
try{location.uninstallAddon(id);seenFiles.push(stageDirEntry.leafName);}
catch(e){logger.error("Failed to uninstall add-on "+id+" in "+location.name,e);} 
continue;}}
aManifests[location.name][id]=null;let existingAddonID=id;let jsonfile=stagingDir.clone();jsonfile.append(id+".json"); let foreignInstall=!jsonfile.exists();let addon;try{addon=syncLoadManifestFromFile(stageDirEntry,location);}
catch(e){logger.error("Unable to read add-on manifest from "+stageDirEntry.path,e); seenFiles.push(stageDirEntry.leafName);seenFiles.push(jsonfile.leafName);continue;}
if(mustSign(addon.type)&&addon.signedState<=AddonManager.SIGNEDSTATE_MISSING){logger.warn("Refusing to install staged add-on "+id+" with signed state "+addon.signedState);seenFiles.push(stageDirEntry.leafName);seenFiles.push(jsonfile.leafName);continue;}
 
if(!foreignInstall){logger.debug("Found updated metadata for "+id+" in "+location.name);let fis=Cc["@mozilla.org/network/file-input-stream;1"].createInstance(Ci.nsIFileInputStream);let json=Cc["@mozilla.org/dom/json;1"].createInstance(Ci.nsIJSON);try{fis.init(jsonfile,-1,0,0);let metadata=json.decodeFromStream(fis,jsonfile.fileSize);addon.importMetadata(metadata);
 aManifests[location.name][id]=addon;}
catch(e){

 logger.error("Unable to read metadata from "+jsonfile.path,e);}
finally{fis.close();}}
seenFiles.push(jsonfile.leafName);existingAddonID=addon.existingAddonID||id;var oldBootstrap=null;logger.debug("Processing install of "+id+" in "+location.name);if(existingAddonID in this.bootstrappedAddons){try{var existingAddon=Cc["@mozilla.org/file/local;1"].createInstance(Ci.nsIFile);existingAddon.persistentDescriptor=this.bootstrappedAddons[existingAddonID].descriptor;if(existingAddon.exists()){oldBootstrap=this.bootstrappedAddons[existingAddonID];
 let newVersion=addon.version;let oldVersion=oldBootstrap.version;let uninstallReason=Services.vc.compare(oldVersion,newVersion)<0?BOOTSTRAP_REASONS.ADDON_UPGRADE:BOOTSTRAP_REASONS.ADDON_DOWNGRADE;this.callBootstrapMethod(createAddonDetails(existingAddonID,oldBootstrap),existingAddon,"uninstall",uninstallReason,{newVersion:newVersion});this.unloadBootstrapScope(existingAddonID);flushChromeCaches();}}
catch(e){}}
try{addon._sourceBundle=location.installAddon(id,stageDirEntry,existingAddonID);}
catch(e){logger.error("Failed to install staged add-on "+id+" in "+location.name,e); AddonInstall.createStagedInstall(location,stageDirEntry,addon); seenFiles.pop();delete aManifests[location.name][id];if(oldBootstrap){ this.callBootstrapMethod(createAddonDetails(existingAddonID,oldBootstrap),existingAddon,"install",BOOTSTRAP_REASONS.ADDON_INSTALL);}
continue;}}
try{location.cleanStagingDir(seenFiles);}
catch(e){logger.debug("Error cleaning staging dir "+stagingDir.path,e);}}
return changed;},installDistributionAddons:function(aManifests,aAppChanged){let distroDir;try{distroDir=FileUtils.getDir(KEY_APP_DISTRIBUTION,[DIR_EXTENSIONS]);}
catch(e){return false;}
if(!distroDir.exists())
return false;if(!distroDir.isDirectory())
return false;let changed=false;let profileLocation=this.installLocationsByName[KEY_APP_PROFILE];let entries=distroDir.directoryEntries.QueryInterface(Ci.nsIDirectoryEnumerator);let entry;while((entry=entries.nextFile)){let id=entry.leafName;if(entry.isFile()){if(id.substring(id.length-4).toLowerCase()==".xpi"){id=id.substring(0,id.length-4);}
else{logger.debug("Ignoring distribution add-on that isn't an XPI: "+entry.path);continue;}}
else if(!entry.isDirectory()){logger.debug("Ignoring distribution add-on that isn't a file or directory: "+
entry.path);continue;}
if(!gIDTest.test(id)){logger.debug("Ignoring distribution add-on whose name is not a valid add-on ID: "+
entry.path);continue;}
if(!aAppChanged&&Preferences.isSet(PREF_BRANCH_INSTALLED_ADDON+id)){continue;}
let addon;try{addon=syncLoadManifestFromFile(entry,profileLocation);}
catch(e){logger.warn("File entry "+entry.path+" contains an invalid add-on",e);continue;}
if(addon.id!=id){logger.warn("File entry "+entry.path+" contains an add-on with an "+"incorrect ID")
continue;}
let existingEntry=null;try{existingEntry=profileLocation.getLocationForID(id);}
catch(e){}
if(existingEntry){let existingAddon;try{existingAddon=syncLoadManifestFromFile(existingEntry,profileLocation);if(Services.vc.compare(addon.version,existingAddon.version)<=0)
continue;}
catch(e){ logger.warn("Profile contains an add-on with a bad or missing install "+"manifest at "+existingEntry.path+", overwriting",e);}}
else if(Preferences.get(PREF_BRANCH_INSTALLED_ADDON+id,false)){continue;} 
try{addon._sourceBundle=profileLocation.installAddon(id,entry,null,true);logger.debug("Installed distribution add-on "+id);Services.prefs.setBoolPref(PREF_BRANCH_INSTALLED_ADDON+id,true)

 
if(!(KEY_APP_PROFILE in aManifests))
aManifests[KEY_APP_PROFILE]={};aManifests[KEY_APP_PROFILE][id]=addon;changed=true;}
catch(e){logger.error("Failed to install distribution add-on "+entry.path,e);}}
entries.close();return changed;},importPermissions:function(){PermissionsUtils.importFromPrefs(PREF_XPI_PERMISSIONS_BRANCH,XPI_PERMISSION);},checkForChanges:function(aAppChanged,aOldAppVersion,aOldPlatformVersion){logger.debug("checkForChanges");
let updateReasons=[];if(aAppChanged){updateReasons.push("appChanged");}

 
try{this.bootstrappedAddons=JSON.parse(Preferences.get(PREF_BOOTSTRAP_ADDONS,"{}"));}catch(e){logger.warn("Error parsing enabled bootstrapped extensions cache",e);}

 
let manifests={};let updated=this.processPendingFileChanges(manifests);if(updated){updateReasons.push("pendingFileChanges");}


let hasPendingChanges=Preferences.get(PREF_PENDING_OPERATIONS);if(hasPendingChanges){updateReasons.push("hasPendingChanges");} 
if(Preferences.get(PREF_INSTALL_DISTRO_ADDONS,true))
{updated=this.installDistributionAddons(manifests,aAppChanged);if(updated){updateReasons.push("installDistributionAddons");}} 
let telemetryCaptureTime=Cu.now();let installChanged=XPIStates.getInstallState();let telemetry=Services.telemetry;telemetry.getHistogramById("CHECK_ADDONS_MODIFIED_MS").add(Math.round(Cu.now()-telemetryCaptureTime));if(installChanged){updateReasons.push("directoryState");}
let haveAnyAddons=(XPIStates.size>0); if(DB_SCHEMA!=Preferences.get(PREF_DB_SCHEMA,0)){
 if(!haveAnyAddons){logger.debug("Empty XPI database, setting schema version preference to "+DB_SCHEMA);Services.prefs.setIntPref(PREF_DB_SCHEMA,DB_SCHEMA);}
else{updateReasons.push("schemaChanged");}}


let dbFile=FileUtils.getFile(KEY_PROFILEDIR,[FILE_DATABASE],true);if(!dbFile.exists()&&haveAnyAddons){updateReasons.push("needNewDatabase");}
if(updateReasons.length==0){let bootstrapDescriptors=new Set(Object.keys(this.bootstrappedAddons).map(b=>this.bootstrappedAddons[b].descriptor));for(let location of XPIStates.db.values()){for(let state of location.values()){bootstrapDescriptors.delete(state.descriptor);}}
if(bootstrapDescriptors.size>0){logger.warn("Bootstrap state is invalid (missing add-ons: "
+Array.from(bootstrapDescriptors).join(", ")+")");updateReasons.push("missingBootstrapAddon");}} 
try{let extensionListChanged=false;
 if(updateReasons.length>0){AddonManagerPrivate.recordSimpleMeasure("XPIDB_startup_load_reasons",updateReasons);XPIDatabase.syncLoadDB(false);try{extensionListChanged=XPIDatabaseReconcile.processFileChanges(manifests,aAppChanged,aOldAppVersion,aOldPlatformVersion);}
catch(e){logger.error("Failed to process extension changes at startup",e);}}
if(aAppChanged){
 if(this.currentSkin!=this.defaultSkin){let oldSkin=XPIDatabase.getVisibleAddonForInternalName(this.currentSkin);if(!oldSkin||oldSkin.disabled)
this.enableDefaultTheme();}
 
let oldCache=FileUtils.getFile(KEY_PROFILEDIR,[FILE_OLD_CACHE],true);try{if(oldCache.exists())
oldCache.remove(true);}
catch(e){logger.warn("Unable to remove old extension cache "+oldCache.path,e);}}

if(extensionListChanged||hasPendingChanges){logger.debug("Updating database with changes to installed add-ons");XPIDatabase.updateActiveAddons();Services.prefs.setBoolPref(PREF_PENDING_OPERATIONS,!XPIDatabase.writeAddonsList());Services.prefs.setCharPref(PREF_BOOTSTRAP_ADDONS,JSON.stringify(this.bootstrappedAddons));return true;}
logger.debug("No changes found");}
catch(e){logger.error("Error during startup file checks",e);} 
let addonsList=FileUtils.getFile(KEY_PROFILEDIR,[FILE_XPI_ADDONS_LIST],true); if(addonsList.exists()!=haveAnyAddons){logger.debug("Add-ons list is invalid, rebuilding");XPIDatabase.writeAddonsList();}
return false;},supportsMimetype:function(aMimetype){return aMimetype=="application/x-xpinstall";},isInstallEnabled:function(){ return Preferences.get(PREF_XPI_ENABLED,true);},isDirectRequestWhitelisted:function(){return Preferences.get(PREF_XPI_DIRECT_WHITELISTED,true);},isFileRequestWhitelisted:function(){return Preferences.get(PREF_XPI_FILE_WHITELISTED,true);},isInstallAllowed:function(aInstallingPrincipal){if(!this.isInstallEnabled())
return false;let uri=aInstallingPrincipal.URI;if(!uri)
return this.isDirectRequestWhitelisted();if(this.isFileRequestWhitelisted()&&(uri.schemeIs("chrome")||uri.schemeIs("file")))
return true;this.importPermissions();let permission=Services.perms.testPermissionFromPrincipal(aInstallingPrincipal,XPI_PERMISSION);if(permission==Ci.nsIPermissionManager.DENY_ACTION)
return false;let requireWhitelist=Preferences.get(PREF_XPI_WHITELIST_REQUIRED,true);if(requireWhitelist&&(permission!=Ci.nsIPermissionManager.ALLOW_ACTION))
return false;let requireSecureOrigin=Preferences.get(PREF_INSTALL_REQUIRESECUREORIGIN,true);let safeSchemes=["https","chrome","file"];if(requireSecureOrigin&&safeSchemes.indexOf(uri.scheme)==-1)
return false;return true;},getInstallForURL:function(aUrl,aHash,aName,aIcons,aVersion,aBrowser,aCallback){AddonInstall.createDownload(function(aInstall){aCallback(aInstall.wrapper);},aUrl,aHash,aName,aIcons,aVersion,aBrowser);},getInstallForFile:function(aFile,aCallback){AddonInstall.createInstall(function(aInstall){if(aInstall)
aCallback(aInstall.wrapper);else
aCallback(null);},aFile);},installTemporaryAddon:Task.async(function*(aFile){let addon=yield loadManifestFromFile(aFile,TemporaryInstallLocation);if(!addon.bootstrap){throw new Error("Only restartless (bootstrap) add-ons"
+" can be temporarily installed:",addon.id);}
let oldAddon=yield new Promise(resolve=>XPIDatabase.getVisibleAddonForID(addon.id,resolve));if(oldAddon){if(!oldAddon.bootstrap){logger.warn("Non-restartless Add-on is already installed",addon.id);throw new Error("Non-restartless add-on with ID "
+oldAddon.id+" is already installed");}
else{logger.warn("Addon with ID "+oldAddon.id+" already installed,"
+" older version will be disabled");let existingAddonID=oldAddon.id;let existingAddon=oldAddon._sourceBundle;
 let newVersion=addon.version;let oldVersion=oldAddon.version;let uninstallReason=Services.vc.compare(oldVersion,newVersion)<0?BOOTSTRAP_REASONS.ADDON_UPGRADE:BOOTSTRAP_REASONS.ADDON_DOWNGRADE;if(oldAddon.active){XPIProvider.callBootstrapMethod(oldAddon,existingAddon,"shutdown",uninstallReason,{newVersion});}
this.callBootstrapMethod(oldAddon,existingAddon,"uninstall",uninstallReason,{newVersion});this.unloadBootstrapScope(existingAddonID);flushChromeCaches();}}
let file=addon._sourceBundle;XPIProvider._addURIMapping(addon.id,file);XPIProvider.callBootstrapMethod(addon,file,"install",BOOTSTRAP_REASONS.ADDON_INSTALL);addon.state=AddonManager.STATE_INSTALLED;logger.debug("Install of temporary addon in "+aFile.path+" completed.");addon.visible=true;addon.enabled=true;addon.active=true;addon=XPIDatabase.addAddonMetadata(addon,file.persistentDescriptor);XPIStates.addAddon(addon);XPIDatabase.saveChanges();AddonManagerPrivate.callAddonListeners("onInstalling",addon.wrapper,false);XPIProvider.callBootstrapMethod(addon,file,"startup",BOOTSTRAP_REASONS.ADDON_ENABLE);AddonManagerPrivate.callInstallListeners("onExternalInstall",null,addon.wrapper,oldAddon?oldAddon.wrapper:null,false);AddonManagerPrivate.callAddonListeners("onInstalled",addon.wrapper);return addon.wrapper;}),getAddonByInstanceID:function(aInstanceID){if(!aInstanceID||typeof aInstanceID!="symbol")
throw Components.Exception("aInstanceID must be a Symbol()",Cr.NS_ERROR_INVALID_ARG);for(let[id,val]of this.activeAddons){if(aInstanceID==val.instanceID){if(val.safeWrapper){return Promise.resolve(val.safeWrapper);}
return new Promise(resolve=>{this.getAddonByID(id,function(addon){val.safeWrapper=new PrivateWrapper(addon);resolve(val.safeWrapper);});});}}
return Promise.resolve(null);},removeActiveInstall:function(aInstall){let where=this.installs.indexOf(aInstall);if(where==-1){logger.warn("removeActiveInstall: could not find active install for "
+aInstall.sourceURI.spec);return;}
this.installs.splice(where,1);},getAddonByID:function(aId,aCallback){XPIDatabase.getVisibleAddonForID(aId,function(aAddon){aCallback(aAddon?aAddon.wrapper:null);});},getAddonsByTypes:function(aTypes,aCallback){let typesToGet=getAllAliasesForTypes(aTypes);XPIDatabase.getVisibleAddons(typesToGet,function(aAddons){aCallback(aAddons.map(a=>a.wrapper));});},getAddonBySyncGUID:function(aGUID,aCallback){XPIDatabase.getAddonBySyncGUID(aGUID,function(aAddon){aCallback(aAddon?aAddon.wrapper:null);});},getAddonsWithOperationsByTypes:function(aTypes,aCallback){let typesToGet=getAllAliasesForTypes(aTypes);XPIDatabase.getVisibleAddonsWithPendingOperations(typesToGet,function(aAddons){let results=aAddons.map(a=>a.wrapper);for(let install of XPIProvider.installs){if(install.state==AddonManager.STATE_INSTALLED&&!(install.addon.inDatabase))
results.push(install.addon.wrapper);}
aCallback(results);});},getInstallsByTypes:function(aTypes,aCallback){let results=this.installs.slice(0);if(aTypes){results=results.filter(install=>{return aTypes.includes(getExternalType(install.type));});}
aCallback(results.map(install=>install.wrapper));},mapURIToAddonID:function(aURI){if(aURI.scheme=="moz-extension"){return AddonPolicyService.extensionURIToAddonId(aURI);}
let resolved=this._resolveURIToFile(aURI);if(!resolved||!(resolved instanceof Ci.nsIFileURL))
return null;for(let[id,path]of this._addonFileMap){if(resolved.file.path.startsWith(path))
return id;}
return null;},addonChanged:function(aId,aType,aPendingRestart){ if(aType!="theme")
return;if(!aId){ this.enableDefaultTheme();return;}
 
let previousTheme=null;let newSkin=this.defaultSkin;let addons=XPIDatabase.getAddonsByType("theme");for(let theme of addons){if(!theme.visible)
return;if(theme.id==aId)
newSkin=theme.internalName;else if(theme.userDisabled==false&&!theme.pendingUninstall)
previousTheme=theme;}
if(aPendingRestart){Services.prefs.setBoolPref(PREF_DSS_SWITCHPENDING,true);Services.prefs.setCharPref(PREF_DSS_SKIN_TO_SELECT,newSkin);}
else if(newSkin==this.currentSkin){try{Services.prefs.clearUserPref(PREF_DSS_SWITCHPENDING);}
catch(e){}
try{Services.prefs.clearUserPref(PREF_DSS_SKIN_TO_SELECT);}
catch(e){}}
else{Services.prefs.setCharPref(PREF_GENERAL_SKINS_SELECTEDSKIN,newSkin);this.currentSkin=newSkin;}
this.selectedSkin=newSkin;
 Services.prefs.savePrefFile(null);
if(previousTheme)
this.updateAddonDisabledState(previousTheme,true);},updateAddonAppDisabledStates:function(){let addons=XPIDatabase.getAddons();for(let addon of addons){this.updateAddonDisabledState(addon);}},updateAddonRepositoryData:function(aCallback){XPIDatabase.getVisibleAddons(null,aAddons=>{let pending=aAddons.length;logger.debug("updateAddonRepositoryData found "+pending+" visible add-ons");if(pending==0){aCallback();return;}
function notifyComplete(){if(--pending==0)
aCallback();}
for(let addon of aAddons){AddonRepository.getCachedAddonByID(addon.id,aRepoAddon=>{if(aRepoAddon){logger.debug("updateAddonRepositoryData got info for "+addon.id);addon._repositoryAddon=aRepoAddon;addon.compatibilityOverrides=aRepoAddon.compatibilityOverrides;this.updateAddonDisabledState(addon);}
notifyComplete();});}});},enableDefaultTheme:function(){logger.debug("Activating default theme");let addon=XPIDatabase.getVisibleAddonForInternalName(this.defaultSkin);if(addon){if(addon.userDisabled){this.updateAddonDisabledState(addon,false);}
else if(!this.extensionsActive){

 Services.prefs.setCharPref(PREF_GENERAL_SKINS_SELECTEDSKIN,addon.internalName);this.currentSkin=this.selectedSkin=addon.internalName;Preferences.reset(PREF_DSS_SKIN_TO_SELECT);Preferences.reset(PREF_DSS_SWITCHPENDING);}
else{logger.warn("Attempting to activate an already active default theme");}}
else{logger.warn("Unable to activate the default theme");}},onDebugConnectionChange:function(aEvent,aWhat,aConnection){if(aWhat!="opened")
return;for(let[id,val]of this.activeAddons){aConnection.setAddonOptions(id,{global:val.debugGlobal||val.bootstrapScope});}},observe:function(aSubject,aTopic,aData){if(aTopic==NOTIFICATION_FLUSH_PERMISSIONS){if(!aData||aData==XPI_PERMISSION){this.importPermissions();}
return;}
else if(aTopic==NOTIFICATION_TOOLBOXPROCESS_LOADED){Services.obs.removeObserver(this,NOTIFICATION_TOOLBOXPROCESS_LOADED,false);this._toolboxProcessLoaded=true;BrowserToolboxProcess.on("connectionchange",this.onDebugConnectionChange.bind(this));}
if(aTopic=="nsPref:changed"){switch(aData){case PREF_EM_MIN_COMPAT_APP_VERSION:this.minCompatibleAppVersion=Preferences.get(PREF_EM_MIN_COMPAT_APP_VERSION,null);this.updateAddonAppDisabledStates();break;case PREF_EM_MIN_COMPAT_PLATFORM_VERSION:this.minCompatiblePlatformVersion=Preferences.get(PREF_EM_MIN_COMPAT_PLATFORM_VERSION,null);this.updateAddonAppDisabledStates();break;case PREF_XPI_SIGNATURES_REQUIRED:this.updateAddonAppDisabledStates();break;}}},isBlockingE10s:function(aAddon){if(aAddon.type!="extension"&&aAddon.type!="webextension"&&aAddon.type!="theme")
return false; let hotfixID=Preferences.get(PREF_EM_HOTFIX_ID,undefined);if(hotfixID&&hotfixID==aAddon.id)
return false; if(aAddon.type=="theme"&&aAddon.internalName==XPIProvider.defaultSkin)
return false; let locName=aAddon._installLocation?aAddon._installLocation.name:undefined;if(locName==KEY_APP_SYSTEM_DEFAULTS||locName==KEY_APP_SYSTEM_ADDONS)
return false;return true;},e10sBlocksEnabling:function(aAddon){ if(!Preferences.get(PREF_E10S_BLOCK_ENABLE,false))
return false; if(!Services.appinfo.browserTabsRemoteAutostart)
return false;return this.isBlockingE10s(aAddon);},enableRequiresRestart:function(aAddon){
if(!this.extensionsActive)
return false;
 if(Services.appinfo.inSafeMode)
return false; if(aAddon.active)
return false;if(aAddon.type=="theme"){
 if(Preferences.get(PREF_EM_DSS_ENABLED))
return false;
return aAddon.internalName!=this.currentSkin;}
if(this.e10sBlocksEnabling(aAddon))
return true;return!aAddon.bootstrap;},disableRequiresRestart:function(aAddon){
if(!this.extensionsActive)
return false;
 if(Services.appinfo.inSafeMode)
return false; if(!aAddon.active)
return false;if(aAddon.type=="theme"){
 if(Preferences.get(PREF_EM_DSS_ENABLED))
return false;

if(aAddon.internalName!=this.defaultSkin)
return true;




return this.selectedSkin!=this.currentSkin;}
return!aAddon.bootstrap;},installRequiresRestart:function(aAddon){
if(!this.extensionsActive)
return false;
 if(Services.appinfo.inSafeMode)
return false;

if(aAddon.inDatabase)
return false;
 if("_install"in aAddon&&aAddon._install){

 let existingAddon=aAddon._install.existingAddon;if(existingAddon&&this.uninstallRequiresRestart(existingAddon))
return true;}

if(aAddon.disabled)
return false;if(this.e10sBlocksEnabling(aAddon))
return true;

 return aAddon.type=="theme"||!aAddon.bootstrap;},uninstallRequiresRestart:function(aAddon){
if(!this.extensionsActive)
return false;
 if(Services.appinfo.inSafeMode)
return false;
 return this.disableRequiresRestart(aAddon);},loadBootstrapScope:function(aId,aFile,aVersion,aType,aMultiprocessCompatible,aRunInSafeMode){ this.bootstrappedAddons[aId]={version:aVersion,type:aType,descriptor:aFile.persistentDescriptor,multiprocessCompatible:aMultiprocessCompatible,runInSafeMode:aRunInSafeMode,};this.persistBootstrappedAddons();this.addAddonsToCrashReporter();this.activeAddons.set(aId,{debugGlobal:null,safeWrapper:null,bootstrapScope:null, instanceID:Symbol(aId),});let activeAddon=this.activeAddons.get(aId); if(aType=="locale"){return;}
logger.debug("Loading bootstrap scope from "+aFile.path);let principal=Cc["@mozilla.org/systemprincipal;1"].createInstance(Ci.nsIPrincipal);if(!aMultiprocessCompatible&&Preferences.get(PREF_INTERPOSITION_ENABLED,false)){let interposition=Cc["@mozilla.org/addons/multiprocess-shims;1"].getService(Ci.nsIAddonInterposition);Cu.setAddonInterposition(aId,interposition);}
if(!aFile.exists()){activeAddon.bootstrapScope=new Cu.Sandbox(principal,{sandboxName:aFile.path,wantGlobalProperties:["indexedDB"],addonId:aId,metadata:{addonID:aId}});logger.error("Attempted to load bootstrap scope from missing directory "+aFile.path);return;}
let uri=getURIForResourceInFile(aFile,"bootstrap.js").spec;if(aType=="dictionary")
uri="resource://gre/modules/addons/SpellCheckDictionaryBootstrap.js"
else if(aType=="webextension")
uri="resource://gre/modules/addons/WebExtensionBootstrap.js"
activeAddon.bootstrapScope=new Cu.Sandbox(principal,{sandboxName:uri,wantGlobalProperties:["indexedDB"],addonId:aId,metadata:{addonID:aId,URI:uri}});let loader=Cc["@mozilla.org/moz/jssubscript-loader;1"].createInstance(Ci.mozIJSSubScriptLoader);try{for(let name in BOOTSTRAP_REASONS)
activeAddon.bootstrapScope[name]=BOOTSTRAP_REASONS[name];const features=["Worker","ChromeWorker"];for(let feature of features)
activeAddon.bootstrapScope[feature]=gGlobalScope[feature]; activeAddon.bootstrapScope["console"]=new ConsoleAPI({consoleID:"addon/"+aId});

activeAddon.bootstrapScope.__SCRIPT_URI_SPEC__=uri;Components.utils.evalInSandbox("Components.classes['@mozilla.org/moz/jssubscript-loader;1'] \
                   .createInstance(Components.interfaces.mozIJSSubScriptLoader) \
                   .loadSubScript(__SCRIPT_URI_SPEC__);",activeAddon.bootstrapScope,"ECMAv5");}
catch(e){logger.warn("Error loading bootstrap.js for "+aId,e);}
 
if(this._toolboxProcessLoaded){BrowserToolboxProcess.setAddonOptions(aId,{global:activeAddon.bootstrapScope});}},unloadBootstrapScope:function(aId){
Cu.setAddonInterposition(aId,null);this.activeAddons.delete(aId);delete this.bootstrappedAddons[aId];this.persistBootstrappedAddons();this.addAddonsToCrashReporter();
 if(this._toolboxProcessLoaded){BrowserToolboxProcess.setAddonOptions(aId,{global:null});}},callBootstrapMethod:function(aAddon,aFile,aMethod,aReason,aExtraParams){if(!aAddon.id||!aAddon.version||!aAddon.type){logger.error(new Error("aAddon must include an id, version, and type"));return;} 
let runInSafeMode="runInSafeMode"in aAddon?aAddon.runInSafeMode:canRunInSafeMode(aAddon);if(Services.appinfo.inSafeMode&&!runInSafeMode)
return;let timeStart=new Date();if(CHROME_TYPES.has(aAddon.type)&&aMethod=="startup"){logger.debug("Registering manifest for "+aFile.path);Components.manager.addBootstrappedManifestLocation(aFile);}
try{ let activeAddon=this.activeAddons.get(aAddon.id);if(!activeAddon){this.loadBootstrapScope(aAddon.id,aFile,aAddon.version,aAddon.type,aAddon.multiprocessCompatible||false,runInSafeMode);activeAddon=this.activeAddons.get(aAddon.id);}
if(aAddon.bootstrap){if(aMethod=="startup"||aMethod=="shutdown"){if(!aExtraParams){aExtraParams={};}
aExtraParams["instanceID"]=this.activeAddons.get(aAddon.id).instanceID;}} 
if(aAddon.type=="locale")
return;let method=undefined;try{method=Components.utils.evalInSandbox(`${aMethod};`,activeAddon.bootstrapScope,"ECMAv5");}
catch(e){}
if(!method){logger.warn("Add-on "+aAddon.id+" is missing bootstrap method "+aMethod);return;}
let params={id:aAddon.id,version:aAddon.version,installPath:aFile.clone(),resourceURI:getURIForResourceInFile(aFile,"")};if(aExtraParams){for(let key in aExtraParams){params[key]=aExtraParams[key];}}
logger.debug("Calling bootstrap method "+aMethod+" on "+aAddon.id+" version "+
aAddon.version);try{method(params,aReason);}
catch(e){logger.warn("Exception running bootstrap method "+aMethod+" on "+aAddon.id,e);}}
finally{if(CHROME_TYPES.has(aAddon.type)&&aMethod=="shutdown"&&aReason!=BOOTSTRAP_REASONS.APP_SHUTDOWN){logger.debug("Removing manifest for "+aFile.path);Components.manager.removeBootstrappedManifestLocation(aFile);let manifest=getURIForResourceInFile(aFile,"chrome.manifest");for(let line of ChromeManifestParser.parseSync(manifest)){if(line.type=="resource"){ResProtocolHandler.setSubstitution(line.args[0],null);}}}
this.setTelemetry(aAddon.id,aMethod+"_MS",new Date()-timeStart);}},updateAddonDisabledState:function(aAddon,aUserDisabled,aSoftDisabled){if(!(aAddon.inDatabase))
throw new Error("Can only update addon states for installed addons.");if(aUserDisabled!==undefined&&aSoftDisabled!==undefined){throw new Error("Cannot change userDisabled and softDisabled at the "+"same time");}
if(aUserDisabled===undefined){aUserDisabled=aAddon.userDisabled;}
else if(!aUserDisabled){ aSoftDisabled=false;}
 
if(aSoftDisabled===undefined||aUserDisabled)
aSoftDisabled=aAddon.softDisabled;let appDisabled=!isUsableAddon(aAddon); if(aAddon.userDisabled==aUserDisabled&&aAddon.appDisabled==appDisabled&&aAddon.softDisabled==aSoftDisabled)
return undefined;let wasDisabled=aAddon.disabled;let isDisabled=aUserDisabled||aSoftDisabled||appDisabled;let appDisabledChanged=aAddon.appDisabled!=appDisabled;XPIDatabase.setAddonProperties(aAddon,{userDisabled:aUserDisabled,appDisabled:appDisabled,softDisabled:aSoftDisabled});let wrapper=aAddon.wrapper;if(appDisabledChanged){AddonManagerPrivate.callAddonListeners("onPropertyChanged",wrapper,["appDisabled"]);}
 
if(!aAddon.visible||(wasDisabled==isDisabled))
return undefined; Services.prefs.setBoolPref(PREF_PENDING_OPERATIONS,true);if(isDisabled!=aAddon.active){AddonManagerPrivate.callAddonListeners("onOperationCancelled",wrapper);}
else{if(isDisabled){var needsRestart=this.disableRequiresRestart(aAddon);AddonManagerPrivate.callAddonListeners("onDisabling",wrapper,needsRestart);}
else{needsRestart=this.enableRequiresRestart(aAddon);AddonManagerPrivate.callAddonListeners("onEnabling",wrapper,needsRestart);}
if(!needsRestart){XPIDatabase.updateAddonActive(aAddon,!isDisabled);if(isDisabled){if(aAddon.bootstrap){this.callBootstrapMethod(aAddon,aAddon._sourceBundle,"shutdown",BOOTSTRAP_REASONS.ADDON_DISABLE);this.unloadBootstrapScope(aAddon.id);}
AddonManagerPrivate.callAddonListeners("onDisabled",wrapper);}
else{if(aAddon.bootstrap){this.callBootstrapMethod(aAddon,aAddon._sourceBundle,"startup",BOOTSTRAP_REASONS.ADDON_ENABLE);}
AddonManagerPrivate.callAddonListeners("onEnabled",wrapper);}}
else if(aAddon.bootstrap){
 if(isDisabled){this.bootstrappedAddons[aAddon.id].disable=true;}
else{this.bootstrappedAddons[aAddon.id]={version:aAddon.version,type:aAddon.type,descriptor:aAddon._sourceBundle.persistentDescriptor,multiprocessCompatible:aAddon.multiprocessCompatible,runInSafeMode:canRunInSafeMode(aAddon),};this.persistBootstrappedAddons();}}}
let xpiState=XPIStates.getAddon(aAddon.location,aAddon.id);if(xpiState){xpiState.syncWithDB(aAddon);XPIStates.save();}else{ logger.warn("No XPIState for ${id} in ${location}",aAddon);} 
if(aAddon.type=="theme"&&!isDisabled)
AddonManagerPrivate.notifyAddonChanged(aAddon.id,aAddon.type,needsRestart);return isDisabled;},uninstallAddon:function(aAddon,aForcePending){if(!(aAddon.inDatabase))
throw new Error("Cannot uninstall addon "+aAddon.id+" because it is not installed");if(aAddon._installLocation.locked)
throw new Error("Cannot uninstall addon "+aAddon.id
+" from locked install location "+aAddon._installLocation.name); let requiresRestart=this.uninstallRequiresRestart(aAddon); let makePending=aForcePending||requiresRestart;if(makePending&&aAddon.pendingUninstall)
throw new Error("Add-on is already marked to be uninstalled");aAddon._hasResourceCache.clear();if(aAddon._updateCheck){logger.debug("Cancel in-progress update check for "+aAddon.id);aAddon._updateCheck.cancel();}
let wasPending=aAddon.pendingUninstall;if(makePending){


if(aAddon._installLocation.name!=KEY_APP_TEMPORARY){let stage=aAddon._installLocation.getStagingDir();stage.append(aAddon.id);if(!stage.exists())
stage.create(Ci.nsIFile.DIRECTORY_TYPE,FileUtils.PERMS_DIRECTORY);}
XPIDatabase.setAddonProperties(aAddon,{pendingUninstall:true});Services.prefs.setBoolPref(PREF_PENDING_OPERATIONS,true);let xpiState=XPIStates.getAddon(aAddon.location,aAddon.id);if(xpiState){xpiState.enabled=false;XPIStates.save();}else{logger.warn("Can't find XPI state while uninstalling ${id} from ${location}",aAddon);}}
if(!aAddon.visible)
return;let wrapper=aAddon.wrapper;if(!wasPending){


AddonManagerPrivate.callAddonListeners("onUninstalling",wrapper,makePending);} 
function revealAddon(aAddon){XPIDatabase.makeAddonVisible(aAddon);let wrappedAddon=aAddon.wrapper;AddonManagerPrivate.callAddonListeners("onInstalling",wrappedAddon,false);if(!aAddon.disabled&&!XPIProvider.enableRequiresRestart(aAddon)){XPIDatabase.updateAddonActive(aAddon,true);}
if(aAddon.bootstrap){XPIProvider.callBootstrapMethod(aAddon,aAddon._sourceBundle,"install",BOOTSTRAP_REASONS.ADDON_INSTALL);if(aAddon.active){XPIProvider.callBootstrapMethod(aAddon,aAddon._sourceBundle,"startup",BOOTSTRAP_REASONS.ADDON_INSTALL);}
else{XPIProvider.unloadBootstrapScope(aAddon.id);}}
 
AddonManagerPrivate.callAddonListeners("onInstalled",wrappedAddon);}
function findAddonAndReveal(aId){let[locationName,]=XPIStates.findAddon(aId);if(locationName){XPIDatabase.getAddonInLocation(aId,locationName,revealAddon);}}
if(!makePending){if(aAddon.bootstrap){if(aAddon.active){this.callBootstrapMethod(aAddon,aAddon._sourceBundle,"shutdown",BOOTSTRAP_REASONS.ADDON_UNINSTALL);}
this.callBootstrapMethod(aAddon,aAddon._sourceBundle,"uninstall",BOOTSTRAP_REASONS.ADDON_UNINSTALL);this.unloadBootstrapScope(aAddon.id);flushChromeCaches();}
aAddon._installLocation.uninstallAddon(aAddon.id);XPIDatabase.removeAddonMetadata(aAddon);XPIStates.removeAddon(aAddon.location,aAddon.id);AddonManagerPrivate.callAddonListeners("onUninstalled",wrapper);findAddonAndReveal(aAddon.id);}
else if(aAddon.bootstrap&&aAddon.active&&!this.disableRequiresRestart(aAddon)){this.callBootstrapMethod(aAddon,aAddon._sourceBundle,"shutdown",BOOTSTRAP_REASONS.ADDON_UNINSTALL);this.unloadBootstrapScope(aAddon.id);XPIDatabase.updateAddonActive(aAddon,false);} 
if(aAddon.type=="theme"&&aAddon.active)
AddonManagerPrivate.notifyAddonChanged(null,aAddon.type,requiresRestart);},cancelUninstallAddon:function(aAddon){if(!(aAddon.inDatabase))
throw new Error("Can only cancel uninstall for installed addons.");if(!aAddon.pendingUninstall)
throw new Error("Add-on is not marked to be uninstalled");if(aAddon._installLocation.name!=KEY_APP_TEMPORARY)
aAddon._installLocation.cleanStagingDir([aAddon.id]);XPIDatabase.setAddonProperties(aAddon,{pendingUninstall:false});if(!aAddon.visible)
return;Services.prefs.setBoolPref(PREF_PENDING_OPERATIONS,true);let wrapper=aAddon.wrapper;AddonManagerPrivate.callAddonListeners("onOperationCancelled",wrapper);if(aAddon.bootstrap&&!aAddon.disabled&&!this.enableRequiresRestart(aAddon)){this.callBootstrapMethod(aAddon,aAddon._sourceBundle,"startup",BOOTSTRAP_REASONS.ADDON_INSTALL);XPIDatabase.updateAddonActive(aAddon,true);}
if(aAddon.type=="theme"&&aAddon.active)
AddonManagerPrivate.notifyAddonChanged(aAddon.id,aAddon.type,false);}};function getHashStringForCrypto(aCrypto){ let toHexString=charCode=>("0"+charCode.toString(16)).slice(-2);let binary=aCrypto.finish(false);let hash=Array.from(binary,c=>toHexString(c.charCodeAt(0)))
return hash.join("").toLowerCase();}
function AddonInstall(aInstallLocation,aUrl,aHash,aReleaseNotesURI,aExistingAddon,aBrowser){this.wrapper=new AddonInstallWrapper(this);this.installLocation=aInstallLocation;this.sourceURI=aUrl;this.releaseNotesURI=aReleaseNotesURI;if(aHash){let hashSplit=aHash.toLowerCase().split(":");this.originalHash={algorithm:hashSplit[0],data:hashSplit[1]};}
this.hash=this.originalHash;this.browser=aBrowser;this.listeners=[];this.icons={};this.existingAddon=aExistingAddon;this.error=0;this.window=aBrowser?aBrowser.contentWindow:null;this.logger=logger;}
AddonInstall.prototype={installLocation:null,wrapper:null,stream:null,crypto:null,originalHash:null,hash:null,browser:null,badCertHandler:null,listeners:null,restartDownload:false,name:null,type:null,version:null,icons:null,releaseNotesURI:null,sourceURI:null,file:null,ownsTempFile:false,certificate:null,certName:null,linkedInstalls:null,existingAddon:null,addon:null,state:null,error:null,progress:null,maxProgress:null,initStagedInstall:function(aManifest){this.name=aManifest.name;this.type=aManifest.type;this.version=aManifest.version;this.icons=aManifest.icons;this.releaseNotesURI=aManifest.releaseNotesURI?NetUtil.newURI(aManifest.releaseNotesURI):null
this.sourceURI=aManifest.sourceURI?NetUtil.newURI(aManifest.sourceURI):null;this.file=null;this.addon=aManifest;this.state=AddonManager.STATE_INSTALLED;XPIProvider.installs.push(this);},initLocalInstall:function(aCallback){aCallback=makeSafe(aCallback);this.file=this.sourceURI.QueryInterface(Ci.nsIFileURL).file;if(!this.file.exists()){logger.warn("XPI file "+this.file.path+" does not exist");this.state=AddonManager.STATE_DOWNLOAD_FAILED;this.error=AddonManager.ERROR_NETWORK_FAILURE;aCallback(this);return;}
this.state=AddonManager.STATE_DOWNLOADED;this.progress=this.file.fileSize;this.maxProgress=this.file.fileSize;if(this.hash){let crypto=Cc["@mozilla.org/security/hash;1"].createInstance(Ci.nsICryptoHash);try{crypto.initWithString(this.hash.algorithm);}
catch(e){logger.warn("Unknown hash algorithm '"+this.hash.algorithm+"' for addon "+this.sourceURI.spec,e);this.state=AddonManager.STATE_DOWNLOAD_FAILED;this.error=AddonManager.ERROR_INCORRECT_HASH;aCallback(this);return;}
let fis=Cc["@mozilla.org/network/file-input-stream;1"].createInstance(Ci.nsIFileInputStream);fis.init(this.file,-1,-1,false);crypto.updateFromStream(fis,this.file.fileSize);let calculatedHash=getHashStringForCrypto(crypto);if(calculatedHash!=this.hash.data){logger.warn("File hash ("+calculatedHash+") did not match provided hash ("+
this.hash.data+")");this.state=AddonManager.STATE_DOWNLOAD_FAILED;this.error=AddonManager.ERROR_INCORRECT_HASH;aCallback(this);return;}}
this.loadManifest(this.file).then(()=>{XPIDatabase.getVisibleAddonForID(this.addon.id,aAddon=>{this.existingAddon=aAddon;if(aAddon)
applyBlocklistChanges(aAddon,this.addon);this.addon.updateDate=Date.now();this.addon.installDate=aAddon?aAddon.installDate:this.addon.updateDate;if(!this.addon.isCompatible){this.state=AddonManager.STATE_CHECKING;new UpdateChecker(this.addon,{onUpdateFinished:aAddon=>{this.state=AddonManager.STATE_DOWNLOADED;XPIProvider.installs.push(this);AddonManagerPrivate.callInstallListeners("onNewInstall",this.listeners,this.wrapper);aCallback(this);}},AddonManager.UPDATE_WHEN_ADDON_INSTALLED);}
else{XPIProvider.installs.push(this);AddonManagerPrivate.callInstallListeners("onNewInstall",this.listeners,this.wrapper);aCallback(this);}});},([error,message])=>{logger.warn("Invalid XPI",message);this.state=AddonManager.STATE_DOWNLOAD_FAILED;this.error=error;AddonManagerPrivate.callInstallListeners("onNewInstall",this.listeners,this.wrapper);aCallback(this);});},initAvailableDownload:function(aName,aType,aIcons,aVersion,aCallback){this.state=AddonManager.STATE_AVAILABLE;this.name=aName;this.type=aType;this.version=aVersion;this.icons=aIcons;this.progress=0;this.maxProgress=-1;XPIProvider.installs.push(this);AddonManagerPrivate.callInstallListeners("onNewInstall",this.listeners,this.wrapper);makeSafe(aCallback)(this);},install:function(){switch(this.state){case AddonManager.STATE_AVAILABLE:this.startDownload();break;case AddonManager.STATE_DOWNLOADED:this.startInstall();break;case AddonManager.STATE_DOWNLOAD_FAILED:case AddonManager.STATE_INSTALL_FAILED:case AddonManager.STATE_CANCELLED:this.removeTemporaryFile();this.state=AddonManager.STATE_AVAILABLE;this.error=0;this.progress=0;this.maxProgress=-1;this.hash=this.originalHash;XPIProvider.installs.push(this);this.startDownload();break;case AddonManager.STATE_DOWNLOADING:case AddonManager.STATE_CHECKING:case AddonManager.STATE_INSTALLING: return;default:throw new Error("Cannot start installing from this state");}},cancel:function(){switch(this.state){case AddonManager.STATE_DOWNLOADING:if(this.channel){logger.debug("Cancelling download of "+this.sourceURI.spec);this.channel.cancel(Cr.NS_BINDING_ABORTED);}
break;case AddonManager.STATE_AVAILABLE:case AddonManager.STATE_DOWNLOADED:logger.debug("Cancelling download of "+this.sourceURI.spec);this.state=AddonManager.STATE_CANCELLED;XPIProvider.removeActiveInstall(this);AddonManagerPrivate.callInstallListeners("onDownloadCancelled",this.listeners,this.wrapper);this.removeTemporaryFile();break;case AddonManager.STATE_INSTALLED:logger.debug("Cancelling install of "+this.addon.id);let xpi=this.installLocation.getStagingDir();xpi.append(this.addon.id+".xpi");flushJarCache(xpi);this.installLocation.cleanStagingDir([this.addon.id,this.addon.id+".xpi",this.addon.id+".json"]);this.state=AddonManager.STATE_CANCELLED;XPIProvider.removeActiveInstall(this);if(this.existingAddon){delete this.existingAddon.pendingUpgrade;this.existingAddon.pendingUpgrade=null;}
AddonManagerPrivate.callAddonListeners("onOperationCancelled",this.addon.wrapper);AddonManagerPrivate.callInstallListeners("onInstallCancelled",this.listeners,this.wrapper);break;default:throw new Error("Cannot cancel install of "+this.sourceURI.spec+" from this state ("+this.state+")");}},addListener:function(aListener){if(!this.listeners.some(function(i){return i==aListener;}))
this.listeners.push(aListener);},removeListener:function(aListener){this.listeners=this.listeners.filter(function(i){return i!=aListener;});},removeTemporaryFile:function(){ if(!this.ownsTempFile){this.logger.debug("removeTemporaryFile: "+this.sourceURI.spec+" does not own temp file");return;}
try{this.logger.debug("removeTemporaryFile: "+this.sourceURI.spec+" removing temp file "+
this.file.path);this.file.remove(true);this.ownsTempFile=false;}
catch(e){this.logger.warn("Failed to remove temporary file "+this.file.path+" for addon "+
this.sourceURI.spec,e);}},updateAddonURIs:function(){this.addon.sourceURI=this.sourceURI.spec;if(this.releaseNotesURI)
this.addon.releaseNotesURI=this.releaseNotesURI.spec;},_createLinkedInstalls:Task.async(function*(aFiles){if(aFiles.length==0)
return; if(!this.linkedInstalls)
this.linkedInstalls=[];for(let{entryName,file}of aFiles){logger.debug("Creating linked install from "+entryName);let install=yield new Promise(resolve=>AddonInstall.createInstall(resolve,file)); install.ownsTempFile=true;this.linkedInstalls.push(install);
 if(install.linkedInstalls){this.linkedInstalls.push(...install.linkedInstalls);install.linkedInstalls=null;}
install.sourceURI=this.sourceURI;install.releaseNotesURI=this.releaseNotesURI;if(install.state!=AddonManager.STATE_DOWNLOAD_FAILED)
install.updateAddonURIs();}}),_loadMultipackageManifests:Task.async(function*(aZipReader){let files=[];let entries=aZipReader.findEntries("(*.[Xx][Pp][Ii]|*.[Jj][Aa][Rr])");while(entries.hasMore()){let entryName=entries.getNext();let file=getTemporaryFile();try{aZipReader.extract(entryName,file);files.push({entryName,file});}
catch(e){logger.warn("Failed to extract "+entryName+" from multi-package "+"XPI",e);file.remove(false);}}
aZipReader.close();if(files.length==0){return Promise.reject([AddonManager.ERROR_CORRUPT_FILE,"Multi-package XPI does not contain any packages to install"]);}
let addon=null;
for(let{entryName,file}of files){this.removeTemporaryFile();try{yield this.loadManifest(file);logger.debug("Base multi-package XPI install came from "+entryName);this.file=file;this.ownsTempFile=true;yield this._createLinkedInstalls(files.filter(f=>f.file!=file));return undefined;}
catch(e){
}} 
for(let{file}of files){try{file.remove(true);}catch(e){this.logger.warn("Could not remove temp file "+file.path);}}
return Promise.reject([AddonManager.ERROR_CORRUPT_FILE,"Multi-package XPI does not contain any valid packages to install"]);}),loadManifest:Task.async(function*(file){let zipreader=Cc["@mozilla.org/libjar/zip-reader;1"].createInstance(Ci.nsIZipReader);try{zipreader.open(file);}
catch(e){zipreader.close();return Promise.reject([AddonManager.ERROR_CORRUPT_FILE,e]);}
try{ this.addon=yield loadManifestFromZipReader(zipreader,this.installLocation);}
catch(e){zipreader.close();return Promise.reject([AddonManager.ERROR_CORRUPT_FILE,e]);}
if(mustSign(this.addon.type)){if(this.addon.signedState<=AddonManager.SIGNEDSTATE_MISSING){
let state=this.addon.signedState;this.addon=null;zipreader.close();if(state==AddonManager.SIGNEDSTATE_MISSING)
return Promise.reject([AddonManager.ERROR_SIGNEDSTATE_REQUIRED,"signature is required but missing"])
return Promise.reject([AddonManager.ERROR_CORRUPT_FILE,"signature verification failed"])}}
else if(this.addon.signedState==AddonManager.SIGNEDSTATE_UNKNOWN||this.addon.signedState==AddonManager.SIGNEDSTATE_NOT_REQUIRED){ let x509=zipreader.getSigningCert(null);if(x509){logger.debug("Verifying XPI signature");if(verifyZipSigning(zipreader,x509)){this.certificate=x509;if(this.certificate.commonName.length>0){this.certName=this.certificate.commonName;}else{this.certName=this.certificate.organization;}}else{zipreader.close();return Promise.reject([AddonManager.ERROR_CORRUPT_FILE,"XPI is incorrectly signed"]);}}}
if(this.existingAddon&&this.existingAddon.type=="webextension"&&this.addon.type!="webextension"){zipreader.close();return Promise.reject([AddonManager.ERROR_UNEXPECTED_ADDON_TYPE,"WebExtensions may not be upated to other extension types"]);}
if(this.addon.type=="multipackage")
return this._loadMultipackageManifests(zipreader);zipreader.close();this.updateAddonURIs();this.addon._install=this;this.name=this.addon.selectedLocale.name;this.type=this.addon.type;this.version=this.addon.version;
 let repoAddon=yield new Promise(resolve=>AddonRepository.getCachedAddonByID(this.addon.id,resolve)); if(!repoAddon){yield new Promise(resolve=>AddonRepository.cacheAddons([this.addon.id],resolve));repoAddon=yield new Promise(resolve=>AddonRepository.getCachedAddonByID(this.addon.id,resolve));}
this.addon._repositoryAddon=repoAddon;this.name=this.name||this.addon._repositoryAddon.name;this.addon.compatibilityOverrides=repoAddon?repoAddon.compatibilityOverrides:null;this.addon.appDisabled=!isUsableAddon(this.addon);return undefined;}),observe:function(aSubject,aTopic,aData){ this.cancel();},startDownload:function(){this.state=AddonManager.STATE_DOWNLOADING;if(!AddonManagerPrivate.callInstallListeners("onDownloadStarted",this.listeners,this.wrapper)){logger.debug("onDownloadStarted listeners cancelled installation of addon "+this.sourceURI.spec);this.state=AddonManager.STATE_CANCELLED;XPIProvider.removeActiveInstall(this);AddonManagerPrivate.callInstallListeners("onDownloadCancelled",this.listeners,this.wrapper)
return;} 
if(this.state!=AddonManager.STATE_DOWNLOADING)
return;if(this.channel){
 logger.debug("Waiting for previous download to complete");this.restartDownload=true;return;}
this.openChannel();},openChannel:function(){this.restartDownload=false;try{this.file=getTemporaryFile();this.ownsTempFile=true;this.stream=Cc["@mozilla.org/network/file-output-stream;1"].createInstance(Ci.nsIFileOutputStream);this.stream.init(this.file,FileUtils.MODE_WRONLY|FileUtils.MODE_CREATE|FileUtils.MODE_TRUNCATE,FileUtils.PERMS_FILE,0);}
catch(e){logger.warn("Failed to start download for addon "+this.sourceURI.spec,e);this.state=AddonManager.STATE_DOWNLOAD_FAILED;this.error=AddonManager.ERROR_FILE_ACCESS;XPIProvider.removeActiveInstall(this);AddonManagerPrivate.callInstallListeners("onDownloadFailed",this.listeners,this.wrapper);return;}
let listener=Cc["@mozilla.org/network/stream-listener-tee;1"].createInstance(Ci.nsIStreamListenerTee);listener.init(this,this.stream);try{let requireBuiltIn=Preferences.get(PREF_INSTALL_REQUIREBUILTINCERTS,true);this.badCertHandler=new CertUtils.BadCertHandler(!requireBuiltIn);this.channel=NetUtil.newChannel({uri:this.sourceURI,loadUsingSystemPrincipal:true});this.channel.notificationCallbacks=this;if(this.channel instanceof Ci.nsIHttpChannel){this.channel.setRequestHeader("Moz-XPI-Update","1",true);if(this.channel instanceof Ci.nsIHttpChannelInternal)
this.channel.forceAllowThirdPartyCookie=true;}
this.channel.asyncOpen2(listener);Services.obs.addObserver(this,"network:offline-about-to-go-offline",false);}
catch(e){logger.warn("Failed to start download for addon "+this.sourceURI.spec,e);this.state=AddonManager.STATE_DOWNLOAD_FAILED;this.error=AddonManager.ERROR_NETWORK_FAILURE;XPIProvider.removeActiveInstall(this);AddonManagerPrivate.callInstallListeners("onDownloadFailed",this.listeners,this.wrapper);}},onDataAvailable:function(aRequest,aContext,aInputstream,aOffset,aCount){this.crypto.updateFromStream(aInputstream,aCount);this.progress+=aCount;if(!AddonManagerPrivate.callInstallListeners("onDownloadProgress",this.listeners,this.wrapper)){}},asyncOnChannelRedirect:function(aOldChannel,aNewChannel,aFlags,aCallback){if(!this.hash&&aOldChannel.originalURI.schemeIs("https")&&aOldChannel instanceof Ci.nsIHttpChannel){try{let hashStr=aOldChannel.getResponseHeader("X-Target-Digest");let hashSplit=hashStr.toLowerCase().split(":");this.hash={algorithm:hashSplit[0],data:hashSplit[1]};}
catch(e){}}

if(!this.hash)
this.badCertHandler.asyncOnChannelRedirect(aOldChannel,aNewChannel,aFlags,aCallback);else
aCallback.onRedirectVerifyCallback(Cr.NS_OK);this.channel=aNewChannel;},onStartRequest:function(aRequest,aContext){this.crypto=Cc["@mozilla.org/security/hash;1"].createInstance(Ci.nsICryptoHash);if(this.hash){try{this.crypto.initWithString(this.hash.algorithm);}
catch(e){logger.warn("Unknown hash algorithm '"+this.hash.algorithm+"' for addon "+this.sourceURI.spec,e);this.state=AddonManager.STATE_DOWNLOAD_FAILED;this.error=AddonManager.ERROR_INCORRECT_HASH;XPIProvider.removeActiveInstall(this);AddonManagerPrivate.callInstallListeners("onDownloadFailed",this.listeners,this.wrapper);aRequest.cancel(Cr.NS_BINDING_ABORTED);return;}}
else{
this.crypto.initWithString("sha1");}
this.progress=0;if(aRequest instanceof Ci.nsIChannel){try{this.maxProgress=aRequest.contentLength;}
catch(e){}
logger.debug("Download started for "+this.sourceURI.spec+" to file "+
this.file.path);}},onStopRequest:function(aRequest,aContext,aStatus){this.stream.close();this.channel=null;this.badCerthandler=null;Services.obs.removeObserver(this,"network:offline-about-to-go-offline"); if(aStatus==Cr.NS_BINDING_ABORTED){if(this.state==AddonManager.STATE_DOWNLOADING){logger.debug("Cancelled download of "+this.sourceURI.spec);this.state=AddonManager.STATE_CANCELLED;XPIProvider.removeActiveInstall(this);AddonManagerPrivate.callInstallListeners("onDownloadCancelled",this.listeners,this.wrapper);
 if(this.state!=AddonManager.STATE_CANCELLED)
return;}
this.removeTemporaryFile();if(this.restartDownload)
this.openChannel();return;}
logger.debug("Download of "+this.sourceURI.spec+" completed.");if(Components.isSuccessCode(aStatus)){if(!(aRequest instanceof Ci.nsIHttpChannel)||aRequest.requestSucceeded){if(!this.hash&&(aRequest instanceof Ci.nsIChannel)){try{CertUtils.checkCert(aRequest,!Preferences.get(PREF_INSTALL_REQUIREBUILTINCERTS,true));}
catch(e){this.downloadFailed(AddonManager.ERROR_NETWORK_FAILURE,e);return;}}
let calculatedHash=getHashStringForCrypto(this.crypto);this.crypto=null;if(this.hash&&calculatedHash!=this.hash.data){this.downloadFailed(AddonManager.ERROR_INCORRECT_HASH,"Downloaded file hash ("+calculatedHash+") did not match provided hash ("+this.hash.data+")");return;}
this.loadManifest(this.file).then(()=>{if(this.addon.isCompatible){this.downloadCompleted();}
else{this.state=AddonManager.STATE_CHECKING;new UpdateChecker(this.addon,{onUpdateFinished:aAddon=>this.downloadCompleted(),},AddonManager.UPDATE_WHEN_ADDON_INSTALLED);}},([error,message])=>{this.downloadFailed(error,message);});}
else{if(aRequest instanceof Ci.nsIHttpChannel)
this.downloadFailed(AddonManager.ERROR_NETWORK_FAILURE,aRequest.responseStatus+" "+
aRequest.responseStatusText);else
this.downloadFailed(AddonManager.ERROR_NETWORK_FAILURE,aStatus);}}
else{this.downloadFailed(AddonManager.ERROR_NETWORK_FAILURE,aStatus);}},downloadFailed:function(aReason,aError){logger.warn("Download of "+this.sourceURI.spec+" failed",aError);this.state=AddonManager.STATE_DOWNLOAD_FAILED;this.error=aReason;XPIProvider.removeActiveInstall(this);AddonManagerPrivate.callInstallListeners("onDownloadFailed",this.listeners,this.wrapper);
 if(this.state==AddonManager.STATE_DOWNLOAD_FAILED){logger.debug("downloadFailed: removing temp file for "+this.sourceURI.spec);this.removeTemporaryFile();}
else
logger.debug("downloadFailed: listener changed AddonInstall state for "+
this.sourceURI.spec+" to "+this.state);},downloadCompleted:function(){XPIDatabase.getVisibleAddonForID(this.addon.id,aAddon=>{if(aAddon)
this.existingAddon=aAddon;this.state=AddonManager.STATE_DOWNLOADED;this.addon.updateDate=Date.now();if(this.existingAddon){this.addon.existingAddonID=this.existingAddon.id;this.addon.installDate=this.existingAddon.installDate;applyBlocklistChanges(this.existingAddon,this.addon);}
else{this.addon.installDate=this.addon.updateDate;}
if(AddonManagerPrivate.callInstallListeners("onDownloadEnded",this.listeners,this.wrapper)){ if(this.state!=AddonManager.STATE_DOWNLOADED)
return;this.install();if(this.linkedInstalls){for(let install of this.linkedInstalls){if(install.state==AddonManager.STATE_DOWNLOADED)
install.install();}}}});},

startInstall:function(){this.state=AddonManager.STATE_INSTALLING;if(!AddonManagerPrivate.callInstallListeners("onInstallStarted",this.listeners,this.wrapper)){this.state=AddonManager.STATE_DOWNLOADED;XPIProvider.removeActiveInstall(this);AddonManagerPrivate.callInstallListeners("onInstallCancelled",this.listeners,this.wrapper)
return;}
 
for(let aInstall of XPIProvider.installs){if(aInstall.state==AddonManager.STATE_INSTALLED&&aInstall.installLocation==this.installLocation&&aInstall.addon.id==this.addon.id){logger.debug("Cancelling previous pending install of "+aInstall.addon.id);aInstall.cancel();}}
let isUpgrade=this.existingAddon&&this.existingAddon._installLocation==this.installLocation;let requiresRestart=XPIProvider.installRequiresRestart(this.addon);logger.debug("Starting install of "+this.addon.id+" from "+this.sourceURI.spec);AddonManagerPrivate.callAddonListeners("onInstalling",this.addon.wrapper,requiresRestart);let stagingDir=this.installLocation.getStagingDir();let stagedAddon=stagingDir.clone();Task.spawn((function*(){let installedUnpacked=0;yield this.installLocation.requestStagingDir(); stagedAddon.append(this.addon.id);yield removeAsync(stagedAddon);stagedAddon.leafName=this.addon.id+".xpi";yield removeAsync(stagedAddon); if(this.addon.unpack||Preferences.get(PREF_XPI_UNPACK,false)){logger.debug("Addon "+this.addon.id+" will be installed as "+"an unpacked directory");stagedAddon.leafName=this.addon.id;yield OS.File.makeDir(stagedAddon.path);yield ZipUtils.extractFilesAsync(this.file,stagedAddon);installedUnpacked=1;}
else{logger.debug("Addon "+this.addon.id+" will be installed as "+"a packed xpi");stagedAddon.leafName=this.addon.id+".xpi";yield OS.File.copy(this.file.path,stagedAddon.path);}
if(requiresRestart){ this.addon._sourceBundle=stagedAddon; let stagedJSON=stagedAddon.clone();stagedJSON.leafName=this.addon.id+".json";if(stagedJSON.exists())
stagedJSON.remove(true);let stream=Cc["@mozilla.org/network/file-output-stream;1"].createInstance(Ci.nsIFileOutputStream);let converter=Cc["@mozilla.org/intl/converter-output-stream;1"].createInstance(Ci.nsIConverterOutputStream);try{stream.init(stagedJSON,FileUtils.MODE_WRONLY|FileUtils.MODE_CREATE|FileUtils.MODE_TRUNCATE,FileUtils.PERMS_FILE,0);converter.init(stream,"UTF-8",0,0x0000);converter.writeString(JSON.stringify(this.addon));}
finally{converter.close();stream.close();}
logger.debug("Staged install of "+this.addon.id+" from "+this.sourceURI.spec+" ready; waiting for restart.");this.state=AddonManager.STATE_INSTALLED;if(isUpgrade){delete this.existingAddon.pendingUpgrade;this.existingAddon.pendingUpgrade=this.addon;}
AddonManagerPrivate.callInstallListeners("onInstallEnded",this.listeners,this.wrapper,this.addon.wrapper);}
else{ XPIProvider.removeActiveInstall(this);
 let reason=BOOTSTRAP_REASONS.ADDON_INSTALL;if(this.existingAddon){if(Services.vc.compare(this.existingAddon.version,this.addon.version)<0)
reason=BOOTSTRAP_REASONS.ADDON_UPGRADE;else
reason=BOOTSTRAP_REASONS.ADDON_DOWNGRADE;if(this.existingAddon.bootstrap){let file=this.existingAddon._sourceBundle;if(this.existingAddon.active){XPIProvider.callBootstrapMethod(this.existingAddon,file,"shutdown",reason,{newVersion:this.addon.version});}
XPIProvider.callBootstrapMethod(this.existingAddon,file,"uninstall",reason,{newVersion:this.addon.version});XPIProvider.unloadBootstrapScope(this.existingAddon.id);flushChromeCaches();}
if(!isUpgrade&&this.existingAddon.active){XPIDatabase.updateAddonActive(this.existingAddon,false);}} 
let existingAddonID=this.existingAddon?this.existingAddon.id:null;let file=this.installLocation.installAddon(this.addon.id,stagedAddon,existingAddonID); this.addon._sourceBundle=file;this.addon.visible=true;if(isUpgrade){this.addon=XPIDatabase.updateAddonMetadata(this.existingAddon,this.addon,file.persistentDescriptor);let state=XPIStates.getAddon(this.installLocation.name,this.addon.id);if(state){state.syncWithDB(this.addon,true);}else{logger.warn("Unexpected missing XPI state for add-on ${id}",this.addon);}}
else{this.addon.active=(this.addon.visible&&!this.addon.disabled);this.addon=XPIDatabase.addAddonMetadata(this.addon,file.persistentDescriptor);XPIStates.addAddon(this.addon);this.addon.installDate=this.addon.updateDate;XPIDatabase.saveChanges();}
XPIStates.save();let extraParams={};if(this.existingAddon){extraParams.oldVersion=this.existingAddon.version;}
if(this.addon.bootstrap){XPIProvider.callBootstrapMethod(this.addon,file,"install",reason,extraParams);}
AddonManagerPrivate.callAddonListeners("onInstalled",this.addon.wrapper);logger.debug("Install of "+this.sourceURI.spec+" completed.");this.state=AddonManager.STATE_INSTALLED;AddonManagerPrivate.callInstallListeners("onInstallEnded",this.listeners,this.wrapper,this.addon.wrapper);if(this.addon.bootstrap){if(this.addon.active){XPIProvider.callBootstrapMethod(this.addon,file,"startup",reason,extraParams);}
else{
 XPIProvider.unloadBootstrapScope(this.addon.id);}}
XPIProvider.setTelemetry(this.addon.id,"unpacked",installedUnpacked);recordAddonTelemetry(this.addon);}}).bind(this)).then(null,(e)=>{logger.warn("Failed to install "+this.file.path+" from "+this.sourceURI.spec,e);if(stagedAddon.exists())
recursiveRemove(stagedAddon);this.state=AddonManager.STATE_INSTALL_FAILED;this.error=AddonManager.ERROR_FILE_ACCESS;XPIProvider.removeActiveInstall(this);AddonManagerPrivate.callAddonListeners("onOperationCancelled",this.addon.wrapper);AddonManagerPrivate.callInstallListeners("onInstallFailed",this.listeners,this.wrapper);}).then(()=>{this.removeTemporaryFile();return this.installLocation.releaseStagingDir();});},getInterface:function(iid){if(iid.equals(Ci.nsIAuthPrompt2)){let win=this.window;if(!win&&this.browser)
win=this.browser.ownerDocument.defaultView;let factory=Cc["@mozilla.org/prompter;1"].getService(Ci.nsIPromptFactory);let prompt=factory.getPrompt(win,Ci.nsIAuthPrompt2);if(this.browser&&this.browser.isRemoteBrowser&&prompt instanceof Ci.nsILoginManagerPrompter)
prompt.setE10sData(this.browser,null);return prompt;}
else if(iid.equals(Ci.nsIChannelEventSink)){return this;}
return this.badCertHandler.getInterface(iid);}}
AddonInstall.createStagedInstall=function(aInstallLocation,aDir,aManifest){let url=Services.io.newFileURI(aDir);let install=new AddonInstall(aInstallLocation,aDir);install.initStagedInstall(aManifest);};AddonInstall.createInstall=function(aCallback,aFile){let location=XPIProvider.installLocationsByName[KEY_APP_PROFILE];let url=Services.io.newFileURI(aFile);try{let install=new AddonInstall(location,url);install.initLocalInstall(aCallback);}
catch(e){logger.error("Error creating install",e);makeSafe(aCallback)(null);}};AddonInstall.createDownload=function(aCallback,aUri,aHash,aName,aIcons,aVersion,aBrowser){let location=XPIProvider.installLocationsByName[KEY_APP_PROFILE];let url=NetUtil.newURI(aUri);let install=new AddonInstall(location,url,aHash,null,null,aBrowser);if(url instanceof Ci.nsIFileURL)
install.initLocalInstall(aCallback);else
install.initAvailableDownload(aName,null,aIcons,aVersion,aCallback);};AddonInstall.createUpdate=function(aCallback,aAddon,aUpdate){let url=NetUtil.newURI(aUpdate.updateURL);let releaseNotesURI=null;try{if(aUpdate.updateInfoURL)
releaseNotesURI=NetUtil.newURI(escapeAddonURI(aAddon,aUpdate.updateInfoURL));}
catch(e){}
let install=new AddonInstall(aAddon._installLocation,url,aUpdate.updateHash,releaseNotesURI,aAddon);if(url instanceof Ci.nsIFileURL){install.initLocalInstall(aCallback);}
else{install.initAvailableDownload(aAddon.selectedLocale.name,aAddon.type,aAddon.icons,aUpdate.version,aCallback);}};const wrapperMap=new WeakMap();let installFor=wrapper=>wrapperMap.get(wrapper);let addonFor=installFor;function AddonInstallWrapper(aInstall){wrapperMap.set(this,aInstall);}
AddonInstallWrapper.prototype={get __AddonInstallInternal__(){return AppConstants.DEBUG?installFor(this):undefined;},get type(){return getExternalType(installFor(this).type);},get iconURL(){return installFor(this).icons[32];},get existingAddon(){let install=installFor(this);return install.existingAddon?install.existingAddon.wrapper:null;},get addon(){let install=installFor(this);return install.addon?install.addon.wrapper:null;},get sourceURI(){return installFor(this).sourceURI;},get linkedInstalls(){let install=installFor(this);if(!install.linkedInstalls)
return null;return install.linkedInstalls.map(i=>i.wrapper);},install:function(){installFor(this).install();},cancel:function(){installFor(this).cancel();},addListener:function(listener){installFor(this).addListener(listener);},removeListener:function(listener){installFor(this).removeListener(listener);},};["name","version","icons","releaseNotesURI","file","state","error","progress","maxProgress","certificate","certName"].forEach(function(aProp){Object.defineProperty(AddonInstallWrapper.prototype,aProp,{get:function(){return installFor(this)[aProp];},enumerable:true,});});function UpdateChecker(aAddon,aListener,aReason,aAppVersion,aPlatformVersion){if(!aListener||!aReason)
throw Cr.NS_ERROR_INVALID_ARG;Components.utils.import("resource://gre/modules/addons/AddonUpdateChecker.jsm");this.addon=aAddon;aAddon._updateCheck=this;XPIProvider.doing(this);this.listener=aListener;this.appVersion=aAppVersion;this.platformVersion=aPlatformVersion;this.syncCompatibility=(aReason==AddonManager.UPDATE_WHEN_NEW_APP_INSTALLED);let updateURL=aAddon.updateURL;if(!updateURL){if(aReason==AddonManager.UPDATE_WHEN_PERIODIC_UPDATE&&Services.prefs.getPrefType(PREF_EM_UPDATE_BACKGROUND_URL)==Services.prefs.PREF_STRING){updateURL=Services.prefs.getCharPref(PREF_EM_UPDATE_BACKGROUND_URL);}else{updateURL=Services.prefs.getCharPref(PREF_EM_UPDATE_URL);}}
const UPDATE_TYPE_COMPATIBILITY=32;const UPDATE_TYPE_NEWVERSION=64;aReason|=UPDATE_TYPE_COMPATIBILITY;if("onUpdateAvailable"in this.listener)
aReason|=UPDATE_TYPE_NEWVERSION;let url=escapeAddonURI(aAddon,updateURL,aReason,aAppVersion);this._parser=AddonUpdateChecker.checkForUpdates(aAddon.id,aAddon.updateKey,url,this);}
UpdateChecker.prototype={addon:null,listener:null,appVersion:null,platformVersion:null,syncCompatibility:null,callListener:function(aMethod,...aArgs){if(!(aMethod in this.listener))
return;try{this.listener[aMethod].apply(this.listener,aArgs);}
catch(e){logger.warn("Exception calling UpdateListener method "+aMethod,e);}},onUpdateCheckComplete:function(aUpdates){XPIProvider.done(this.addon._updateCheck);this.addon._updateCheck=null;let AUC=AddonUpdateChecker;let ignoreMaxVersion=false;let ignoreStrictCompat=false;if(!AddonManager.checkCompatibility){ignoreMaxVersion=true;ignoreStrictCompat=true;}else if(this.addon.type in COMPATIBLE_BY_DEFAULT_TYPES&&!AddonManager.strictCompatibility&&!this.addon.strictCompatibility&&!this.addon.hasBinaryComponents){ignoreMaxVersion=true;} 
let compatUpdate=AUC.getCompatibilityUpdate(aUpdates,this.addon.version,this.syncCompatibility,null,null,ignoreMaxVersion,ignoreStrictCompat); if(compatUpdate)
this.addon.applyCompatibilityUpdate(compatUpdate,this.syncCompatibility);

if((this.appVersion&&Services.vc.compare(this.appVersion,Services.appinfo.version)!=0)||(this.platformVersion&&Services.vc.compare(this.platformVersion,Services.appinfo.platformVersion)!=0)){compatUpdate=AUC.getCompatibilityUpdate(aUpdates,this.addon.version,false,this.appVersion,this.platformVersion,ignoreMaxVersion,ignoreStrictCompat);}
if(compatUpdate)
this.callListener("onCompatibilityUpdateAvailable",this.addon.wrapper);else
this.callListener("onNoCompatibilityUpdateAvailable",this.addon.wrapper);function sendUpdateAvailableMessages(aSelf,aInstall){if(aInstall){aSelf.callListener("onUpdateAvailable",aSelf.addon.wrapper,aInstall.wrapper);}
else{aSelf.callListener("onNoUpdateAvailable",aSelf.addon.wrapper);}
aSelf.callListener("onUpdateFinished",aSelf.addon.wrapper,AddonManager.UPDATE_STATUS_NO_ERROR);}
let compatOverrides=AddonManager.strictCompatibility?null:this.addon.compatibilityOverrides;let update=AUC.getNewestCompatibleUpdate(aUpdates,this.appVersion,this.platformVersion,ignoreMaxVersion,ignoreStrictCompat,compatOverrides);if(update&&Services.vc.compare(this.addon.version,update.version)<0&&!this.addon._installLocation.locked){for(let currentInstall of XPIProvider.installs){ if(currentInstall.existingAddon!=this.addon||currentInstall.version!=update.version)
continue;

 if(currentInstall.state==AddonManager.STATE_AVAILABLE){logger.debug("Found an existing AddonInstall for "+this.addon.id);sendUpdateAvailableMessages(this,currentInstall);}
else
sendUpdateAvailableMessages(this,null);return;}
AddonInstall.createUpdate(aInstall=>{sendUpdateAvailableMessages(this,aInstall);},this.addon,update);}
else{sendUpdateAvailableMessages(this,null);}},onUpdateCheckError:function(aError){XPIProvider.done(this.addon._updateCheck);this.addon._updateCheck=null;this.callListener("onNoCompatibilityUpdateAvailable",this.addon.wrapper);this.callListener("onNoUpdateAvailable",this.addon.wrapper);this.callListener("onUpdateFinished",this.addon.wrapper,aError);},cancel:function(){let parser=this._parser;if(parser){this._parser=null; parser.cancel();}}};function AddonInternal(){this._hasResourceCache=new Map();XPCOMUtils.defineLazyGetter(this,"wrapper",()=>{return new AddonWrapper(this);});}
AddonInternal.prototype={_selectedLocale:null,_hasResourceCache:null,active:false,visible:false,userDisabled:false,appDisabled:false,softDisabled:false,sourceURI:null,releaseNotesURI:null,foreignInstall:false,seen:true,skinnable:false,get selectedLocale(){if(this._selectedLocale)
return this._selectedLocale;let locale=Locale.findClosestLocale(this.locales);this._selectedLocale=locale?locale:this.defaultLocale;return this._selectedLocale;},get providesUpdatesSecurely(){return!!(this.updateKey||!this.updateURL||this.updateURL.substring(0,6)=="https:");},get isCompatible(){return this.isCompatibleWith();},get disabled(){return(this.userDisabled||this.appDisabled||this.softDisabled);},get isPlatformCompatible(){if(this.targetPlatforms.length==0)
return true;let matchedOS=false;
 let needsABI=false;let abi=null;try{abi=Services.appinfo.XPCOMABI;}
catch(e){} 
try{for(let platform of this.targetPlatforms){if(platform.os==Services.appinfo.OS){if(platform.abi){needsABI=true;if(platform.abi===abi)
return true;}
else{matchedOS=true;}}}}catch(e){let message="Problem with addon "+this.id+" targetPlatforms "
+JSON.stringify(this.targetPlatforms);logger.error(message,e);AddonManagerPrivate.recordException("XPI",message,e); return false;}
return matchedOS&&!needsABI;},isCompatibleWith:function(aAppVersion,aPlatformVersion){let app=this.matchingTargetApplication;if(!app)
return false;if(!aAppVersion)
aAppVersion=Services.appinfo.version;if(!aPlatformVersion)
aPlatformVersion=Services.appinfo.platformVersion;let version;if(app.id==Services.appinfo.ID)
version=aAppVersion;else if(app.id==TOOLKIT_ID)
version=aPlatformVersion


if(this.type in COMPATIBLE_BY_DEFAULT_TYPES&&!AddonManager.strictCompatibility&&!this.strictCompatibility&&!this.hasBinaryComponents){if(this._repositoryAddon&&this._repositoryAddon.compatibilityOverrides){let overrides=this._repositoryAddon.compatibilityOverrides;let override=AddonRepository.findMatchingCompatOverride(this.version,overrides);if(override&&override.type=="incompatible")
return false;}
let minCompatVersion;if(app.id==Services.appinfo.ID)
minCompatVersion=XPIProvider.minCompatibleAppVersion;else if(app.id==TOOLKIT_ID)
minCompatVersion=XPIProvider.minCompatiblePlatformVersion;if(minCompatVersion&&Services.vc.compare(minCompatVersion,app.maxVersion)>0)
return false;return Services.vc.compare(version,app.minVersion)>=0;}
return(Services.vc.compare(version,app.minVersion)>=0)&&(Services.vc.compare(version,app.maxVersion)<=0)},get matchingTargetApplication(){let app=null;for(let targetApp of this.targetApplications){if(targetApp.id==Services.appinfo.ID)
return targetApp;if(targetApp.id==TOOLKIT_ID)
app=targetApp;}
return app;},get blocklistState(){let staticItem=findMatchingStaticBlocklistItem(this);if(staticItem)
return staticItem.level;return Blocklist.getAddonBlocklistState(this.wrapper);},get blocklistURL(){let staticItem=findMatchingStaticBlocklistItem(this);if(staticItem){let url=Services.urlFormatter.formatURLPref("extensions.blocklist.itemURL");return url.replace(/%blockID%/g,staticItem.blockID);}
return Blocklist.getAddonBlocklistURL(this.wrapper);},applyCompatibilityUpdate:function(aUpdate,aSyncCompatibility){for(let targetApp of this.targetApplications){for(let updateTarget of aUpdate.targetApplications){if(targetApp.id==updateTarget.id&&(aSyncCompatibility||Services.vc.compare(targetApp.maxVersion,updateTarget.maxVersion)<0)){targetApp.minVersion=updateTarget.minVersion;targetApp.maxVersion=updateTarget.maxVersion;}}}
if(aUpdate.multiprocessCompatible!==undefined)
this.multiprocessCompatible=aUpdate.multiprocessCompatible;this.appDisabled=!isUsableAddon(this);},getDataDirectory:function(callback){let parentPath=OS.Path.join(OS.Constants.Path.profileDir,"extension-data");let dirPath=OS.Path.join(parentPath,this.id);Task.spawn(function*(){yield OS.File.makeDir(parentPath,{ignoreExisting:true});yield OS.File.makeDir(dirPath,{ignoreExisting:true});}).then(()=>callback(dirPath,null),e=>callback(dirPath,e));},toJSON:function(aKey){let obj={};for(let prop in this){ if(prop=="wrapper")
continue; if(prop.substring(0,1)=="_")
continue; if(this.__lookupGetter__(prop))
continue; if(this.__lookupSetter__(prop))
continue; if(typeof this[prop]=="function")
continue;obj[prop]=this[prop];}
return obj;},importMetadata:function(aObj){for(let prop of PENDING_INSTALL_METADATA){if(!(prop in aObj))
continue;this[prop]=aObj[prop];} 
this.appDisabled=!isUsableAddon(this);},permissions:function(){let permissions=0; if(!(this.inDatabase))
return permissions;if(!this.appDisabled){if(this.userDisabled||this.softDisabled){permissions|=AddonManager.PERM_CAN_ENABLE;}
else if(this.type!="theme"){permissions|=AddonManager.PERM_CAN_DISABLE;}}
 
if(!this._installLocation.locked&&!this.pendingUninstall){if(this.type!="experiment"&&!this._installLocation.isLinkedAddon(this.id)){permissions|=AddonManager.PERM_CAN_UPGRADE;}
permissions|=AddonManager.PERM_CAN_UNINSTALL;}
return permissions;},};function AddonWrapper(aAddon){wrapperMap.set(this,aAddon);}
AddonWrapper.prototype={get __AddonInternal__(){return AppConstants.DEBUG?addonFor(this):undefined;},get seen(){return addonFor(this).seen;},markAsSeen:function(){addonFor(this).seen=true;XPIDatabase.saveChanges();},get type(){return getExternalType(addonFor(this).type);},get aboutURL(){return this.isActive?addonFor(this)["aboutURL"]:null;},get optionsURL(){let addon=addonFor(this);if(this.isActive&&addon.optionsURL)
return addon.optionsURL;if(this.isActive&&this.hasResource("options.xul"))
return this.getResourceURI("options.xul").spec;return null;},get optionsType(){if(!this.isActive)
return null;let addon=addonFor(this);let hasOptionsXUL=this.hasResource("options.xul");let hasOptionsURL=!!this.optionsURL;if(addon.optionsType){switch(parseInt(addon.optionsType,10)){case AddonManager.OPTIONS_TYPE_DIALOG:case AddonManager.OPTIONS_TYPE_TAB:return hasOptionsURL?addon.optionsType:null;case AddonManager.OPTIONS_TYPE_INLINE:case AddonManager.OPTIONS_TYPE_INLINE_INFO:case AddonManager.OPTIONS_TYPE_INLINE_BROWSER:return(hasOptionsXUL||hasOptionsURL)?addon.optionsType:null;}
return null;}
if(hasOptionsXUL)
return AddonManager.OPTIONS_TYPE_INLINE;if(hasOptionsURL)
return AddonManager.OPTIONS_TYPE_DIALOG;return null;},get iconURL(){return AddonManager.getPreferredIconURL(this,48);},get icon64URL(){return AddonManager.getPreferredIconURL(this,64);},get icons(){let addon=addonFor(this);let icons={};if(addon._repositoryAddon){for(let size in addon._repositoryAddon.icons){icons[size]=addon._repositoryAddon.icons[size];}}
if(addon.icons){for(let size in addon.icons){icons[size]=this.getResourceURI(addon.icons[size]).spec;}}else{ if(this.hasResource("icon.png")){icons[32]=icons[48]=this.getResourceURI("icon.png").spec;}
if(this.hasResource("icon64.png")){icons[64]=this.getResourceURI("icon64.png").spec;}}
if(this.isActive&&addon.iconURL){icons[32]=addon.iconURL;icons[48]=addon.iconURL;}
if(this.isActive&&addon.icon64URL){icons[64]=addon.icon64URL;}
Object.freeze(icons);return icons;},get screenshots(){let addon=addonFor(this);let repositoryAddon=addon._repositoryAddon;if(repositoryAddon&&("screenshots"in repositoryAddon)){let repositoryScreenshots=repositoryAddon.screenshots;if(repositoryScreenshots&&repositoryScreenshots.length>0)
return repositoryScreenshots;}
if(addon.type=="theme"&&this.hasResource("preview.png")){let url=this.getResourceURI("preview.png").spec;return[new AddonManagerPrivate.AddonScreenshot(url)];}
return null;},get applyBackgroundUpdates(){return addonFor(this).applyBackgroundUpdates;},set applyBackgroundUpdates(val){let addon=addonFor(this);if(this.type=="experiment"){logger.warn("Setting applyBackgroundUpdates on an experiment is not supported.");return addon.applyBackgroundUpdates;}
if(val!=AddonManager.AUTOUPDATE_DEFAULT&&val!=AddonManager.AUTOUPDATE_DISABLE&&val!=AddonManager.AUTOUPDATE_ENABLE){val=val?AddonManager.AUTOUPDATE_DEFAULT:AddonManager.AUTOUPDATE_DISABLE;}
if(val==addon.applyBackgroundUpdates)
return val;XPIDatabase.setAddonProperties(addon,{applyBackgroundUpdates:val});AddonManagerPrivate.callAddonListeners("onPropertyChanged",this,["applyBackgroundUpdates"]);return val;},set syncGUID(val){let addon=addonFor(this);if(addon.syncGUID==val)
return val;if(addon.inDatabase)
XPIDatabase.setAddonSyncGUID(addon,val);addon.syncGUID=val;return val;},get install(){let addon=addonFor(this);if(!("_install"in addon)||!addon._install)
return null;return addon._install.wrapper;},get pendingUpgrade(){let addon=addonFor(this);return addon.pendingUpgrade?addon.pendingUpgrade.wrapper:null;},get scope(){let addon=addonFor(this);if(addon._installLocation)
return addon._installLocation.scope;return AddonManager.SCOPE_PROFILE;},get pendingOperations(){let addon=addonFor(this);let pending=0;if(!(addon.inDatabase)){


if(!addon._install||addon._install.state==AddonManager.STATE_INSTALLING||addon._install.state==AddonManager.STATE_INSTALLED)
return AddonManager.PENDING_INSTALL;}
else if(addon.pendingUninstall){
 return AddonManager.PENDING_UNINSTALL;}
if(addon.active&&addon.disabled)
pending|=AddonManager.PENDING_DISABLE;else if(!addon.active&&!addon.disabled)
pending|=AddonManager.PENDING_ENABLE;if(addon.pendingUpgrade)
pending|=AddonManager.PENDING_UPGRADE;return pending;},get operationsRequiringRestart(){let addon=addonFor(this);let ops=0;if(XPIProvider.installRequiresRestart(addon))
ops|=AddonManager.OP_NEEDS_RESTART_INSTALL;if(XPIProvider.uninstallRequiresRestart(addon))
ops|=AddonManager.OP_NEEDS_RESTART_UNINSTALL;if(XPIProvider.enableRequiresRestart(addon))
ops|=AddonManager.OP_NEEDS_RESTART_ENABLE;if(XPIProvider.disableRequiresRestart(addon))
ops|=AddonManager.OP_NEEDS_RESTART_DISABLE;return ops;},get isDebuggable(){return this.isActive&&addonFor(this).bootstrap;},get permissions(){return addonFor(this).permissions();},get isActive(){let addon=addonFor(this);if(!addon.active)
return false;if(!Services.appinfo.inSafeMode)
return true;return addon.bootstrap&&canRunInSafeMode(addon);},get userDisabled(){let addon=addonFor(this);return addon.softDisabled||addon.userDisabled;},set userDisabled(val){let addon=addonFor(this);if(val==this.userDisabled){return val;}
if(addon.inDatabase){if(addon.type=="theme"&&val){if(addon.internalName==XPIProvider.defaultSkin)
throw new Error("Cannot disable the default theme");XPIProvider.enableDefaultTheme();}
else{XPIProvider.updateAddonDisabledState(addon,val);}}
else{addon.userDisabled=val; if(!val)
addon.softDisabled=false;}
return val;},set softDisabled(val){let addon=addonFor(this);if(val==addon.softDisabled)
return val;if(addon.inDatabase){ if(addon.type=="theme"&&val&&!addon.userDisabled){if(addon.internalName==XPIProvider.defaultSkin)
throw new Error("Cannot disable the default theme");XPIProvider.enableDefaultTheme();}
else{XPIProvider.updateAddonDisabledState(addon,undefined,val);}}
else{ if(!addon.userDisabled)
addon.softDisabled=val;}
return val;},get hidden(){let addon=addonFor(this);if(addon._installLocation.name==KEY_APP_TEMPORARY)
return false;return(addon._installLocation.name==KEY_APP_SYSTEM_DEFAULTS||addon._installLocation.name==KEY_APP_SYSTEM_ADDONS);},get isSystem(){let addon=addonFor(this);return(addon._installLocation.name==KEY_APP_SYSTEM_DEFAULTS||addon._installLocation.name==KEY_APP_SYSTEM_ADDONS);},isCompatibleWith:function(aAppVersion,aPlatformVersion){return addonFor(this).isCompatibleWith(aAppVersion,aPlatformVersion);},uninstall:function(alwaysAllowUndo){let addon=addonFor(this);XPIProvider.uninstallAddon(addon,alwaysAllowUndo);},cancelUninstall:function(){let addon=addonFor(this);XPIProvider.cancelUninstallAddon(addon);},findUpdates:function(aListener,aReason,aAppVersion,aPlatformVersion){
if(this.type=="experiment"){AddonManagerPrivate.callNoUpdateListeners(this,aListener,aReason,aAppVersion,aPlatformVersion);return;}
new UpdateChecker(addonFor(this),aListener,aReason,aAppVersion,aPlatformVersion);}, cancelUpdate:function(){let addon=addonFor(this);if(addon._updateCheck){addon._updateCheck.cancel();return true;}
return false;},hasResource:function(aPath){let addon=addonFor(this);if(addon._hasResourceCache.has(aPath))
return addon._hasResourceCache.get(aPath);let bundle=addon._sourceBundle.clone();try{var isDir=bundle.isDirectory();}catch(e){addon._hasResourceCache.set(aPath,false);return false;}
if(isDir){if(aPath)
aPath.split("/").forEach(part=>bundle.append(part));let result=bundle.exists();addon._hasResourceCache.set(aPath,result);return result;}
let zipReader=Cc["@mozilla.org/libjar/zip-reader;1"].createInstance(Ci.nsIZipReader);try{zipReader.open(bundle);let result=zipReader.hasEntry(aPath);addon._hasResourceCache.set(aPath,result);return result;}
catch(e){addon._hasResourceCache.set(aPath,false);return false;}
finally{zipReader.close();}},reload:function(){return new Promise(resolve=>{if(this.appDisabled){throw new Error("cannot reload add-on because it is disabled by the application");}
const addon=addonFor(this);const isReloadable=(!XPIProvider.enableRequiresRestart(addon)&&!XPIProvider.disableRequiresRestart(addon));if(!isReloadable){throw new Error("cannot reload add-on because it requires a browser restart");}
this.userDisabled=true;flushChromeCaches();this.userDisabled=false;resolve();});},getResourceURI:function(aPath){let addon=addonFor(this);if(!aPath)
return NetUtil.newURI(addon._sourceBundle);return getURIForResourceInFile(addon._sourceBundle,aPath);}};function PrivateWrapper(aAddon){AddonWrapper.call(this,aAddon);}
PrivateWrapper.prototype=Object.create(AddonWrapper.prototype);Object.assign(PrivateWrapper.prototype,{setDebugGlobal(global){let activeAddon=XPIProvider.activeAddons.get(this.id);if(activeAddon){activeAddon.debugGlobal=global;}}});function chooseValue(aAddon,aObj,aProp){let repositoryAddon=aAddon._repositoryAddon;let objValue=aObj[aProp];if(repositoryAddon&&(aProp in repositoryAddon)&&(objValue===undefined||objValue===null)){return[repositoryAddon[aProp],true];}
return[objValue,false];}
function defineAddonWrapperProperty(name,getter){Object.defineProperty(AddonWrapper.prototype,name,{get:getter,enumerable:true,});}
["id","syncGUID","version","isCompatible","isPlatformCompatible","providesUpdatesSecurely","blocklistState","blocklistURL","appDisabled","softDisabled","skinnable","size","foreignInstall","hasBinaryComponents","strictCompatibility","compatibilityOverrides","updateURL","getDataDirectory","multiprocessCompatible","signedState"].forEach(function(aProp){defineAddonWrapperProperty(aProp,function(){let addon=addonFor(this);return(aProp in addon)?addon[aProp]:undefined;});});["fullDescription","developerComments","eula","supportURL","contributionURL","contributionAmount","averageRating","reviewCount","reviewURL","totalDownloads","weeklyDownloads","dailyUsers","repositoryStatus"].forEach(function(aProp){defineAddonWrapperProperty(aProp,function(){let addon=addonFor(this);if(addon._repositoryAddon)
return addon._repositoryAddon[aProp];return null;});});["installDate","updateDate"].forEach(function(aProp){defineAddonWrapperProperty(aProp,function(){return new Date(addonFor(this)[aProp]);});});["sourceURI","releaseNotesURI"].forEach(function(aProp){defineAddonWrapperProperty(aProp,function(){let addon=addonFor(this);let[target,fromRepo]=chooseValue(addon,addon,aProp);if(!target)
return null;if(fromRepo)
return target;return NetUtil.newURI(target);});});PROP_LOCALE_SINGLE.forEach(function(aProp){defineAddonWrapperProperty(aProp,function(){let addon=addonFor(this); if(aProp=="creator"&&addon._repositoryAddon&&addon._repositoryAddon.creator){return addon._repositoryAddon.creator;}
let result=null;if(addon.active){try{let pref=PREF_EM_EXTENSION_FORMAT+addon.id+"."+aProp;let value=Preferences.get(pref,null,Ci.nsIPrefLocalizedString);if(value)
result=value;}
catch(e){}}
let rest;if(result==null)
[result,...rest]=chooseValue(addon,addon.selectedLocale,aProp);if(aProp=="creator")
return result?new AddonManagerPrivate.AddonAuthor(result):null;return result;});});PROP_LOCALE_MULTI.forEach(function(aProp){defineAddonWrapperProperty(aProp,function(){let addon=addonFor(this);let results=null;let usedRepository=false;if(addon.active){let pref=PREF_EM_EXTENSION_FORMAT+addon.id+"."+
aProp.substring(0,aProp.length-1);let list=Services.prefs.getChildList(pref,{});if(list.length>0){list.sort();results=[];for(let childPref of list){let value=Preferences.get(childPref,null,Ci.nsIPrefLocalizedString);if(value)
results.push(value);}}}
if(results==null)
[results,usedRepository]=chooseValue(addon,addon.selectedLocale,aProp);if(results&&!usedRepository){results=results.map(function(aResult){return new AddonManagerPrivate.AddonAuthor(aResult);});}
return results;});});function DirectoryInstallLocation(aName,aDirectory,aScope){this._name=aName;this.locked=true;this._directory=aDirectory;this._scope=aScope
this._IDToFileMap={};this._linkedAddons=[];if(!aDirectory||!aDirectory.exists())
return;if(!aDirectory.isDirectory())
throw new Error("Location must be a directory.");this._readAddons();}
DirectoryInstallLocation.prototype={_name:"",_directory:null,_IDToFileMap:null, _readDirectoryFromFile:function(aFile){let linkedDirectory;if(aFile.isSymlink()){linkedDirectory=aFile.clone();try{linkedDirectory.normalize();}catch(e){logger.warn("Symbolic link "+aFile.path+" points to a path"+" which does not exist");return null;}}
else{let fis=Cc["@mozilla.org/network/file-input-stream;1"].createInstance(Ci.nsIFileInputStream);fis.init(aFile,-1,-1,false);let line={value:""};if(fis instanceof Ci.nsILineInputStream)
fis.readLine(line);fis.close();if(line.value){linkedDirectory=Cc["@mozilla.org/file/local;1"].createInstance(Ci.nsIFile);try{linkedDirectory.initWithPath(line.value);}
catch(e){linkedDirectory.setRelativeDescriptor(aFile.parent,line.value);}}}
if(linkedDirectory){if(!linkedDirectory.exists()){logger.warn("File pointer "+aFile.path+" points to "+linkedDirectory.path+" which does not exist");return null;}
if(!linkedDirectory.isDirectory()){logger.warn("File pointer "+aFile.path+" points to "+linkedDirectory.path+" which is not a directory");return null;}
return linkedDirectory;}
logger.warn("File pointer "+aFile.path+" does not contain a path");return null;},_readAddons:function(){

let entries=getDirectoryEntries(this._directory);for(let entry of entries){let id=entry.leafName;if(id==DIR_STAGE||id==DIR_TRASH)
continue;let directLoad=false;if(entry.isFile()&&id.substring(id.length-4).toLowerCase()==".xpi"){directLoad=true;id=id.substring(0,id.length-4);}
if(!gIDTest.test(id)){logger.debug("Ignoring file entry whose name is not a valid add-on ID: "+
entry.path);continue;}
if(!directLoad&&(entry.isFile()||entry.isSymlink())){let newEntry=this._readDirectoryFromFile(entry);if(!newEntry){logger.debug("Deleting stale pointer file "+entry.path);try{entry.remove(true);}
catch(e){logger.warn("Failed to remove stale pointer file "+entry.path,e);}
continue;}
entry=newEntry;this._linkedAddons.push(id);}
this._IDToFileMap[id]=entry;XPIProvider._addURIMapping(id,entry);}},get name(){return this._name;},get scope(){return this._scope;},getAddonLocations:function(){let locations=new Map();for(let id in this._IDToFileMap){locations.set(id,this._IDToFileMap[id].clone());}
return locations;},getLocationForID:function(aId){if(aId in this._IDToFileMap)
return this._IDToFileMap[aId].clone();throw new Error("Unknown add-on ID "+aId);},isLinkedAddon:function(aId){return this._linkedAddons.indexOf(aId)!=-1;}};function MutableDirectoryInstallLocation(aName,aDirectory,aScope){DirectoryInstallLocation.call(this,aName,aDirectory,aScope);this.locked=false;this._stagingDirLock=0;}
MutableDirectoryInstallLocation.prototype=Object.create(DirectoryInstallLocation.prototype);Object.assign(MutableDirectoryInstallLocation.prototype,{getStagingDir:function(){let dir=this._directory.clone();dir.append(DIR_STAGE);return dir;},requestStagingDir:function(){this._stagingDirLock++;if(this._stagingDirPromise)
return this._stagingDirPromise;OS.File.makeDir(this._directory.path);let stagepath=OS.Path.join(this._directory.path,DIR_STAGE);return this._stagingDirPromise=OS.File.makeDir(stagepath).then(null,(e)=>{if(e instanceof OS.File.Error&&e.becauseExists)
return;logger.error("Failed to create staging directory",e);throw e;});},releaseStagingDir:function(){this._stagingDirLock--;if(this._stagingDirLock==0){this._stagingDirPromise=null;this.cleanStagingDir();}
return Promise.resolve();},cleanStagingDir:function(aLeafNames=[]){let dir=this.getStagingDir();for(let name of aLeafNames){let file=dir.clone();file.append(name);recursiveRemove(file);}
if(this._stagingDirLock>0)
return;let dirEntries=dir.directoryEntries.QueryInterface(Ci.nsIDirectoryEnumerator);try{if(dirEntries.nextFile)
return;}
finally{dirEntries.close();}
try{setFilePermissions(dir,FileUtils.PERMS_DIRECTORY);dir.remove(false);}
catch(e){logger.warn("Failed to remove staging dir",e);}},getTrashDir:function(){let trashDir=this._directory.clone();trashDir.append(DIR_TRASH);let trashDirExists=trashDir.exists();try{if(trashDirExists)
recursiveRemove(trashDir);trashDirExists=false;}catch(e){logger.warn("Failed to remove trash directory",e);}
if(!trashDirExists)
trashDir.create(Ci.nsIFile.DIRECTORY_TYPE,FileUtils.PERMS_DIRECTORY);return trashDir;},installAddon:function(aId,aSource,aExistingAddonID,aCopy){let trashDir=this.getTrashDir();let transaction=new SafeInstallOperation();let moveOldAddon=aId=>{let file=this._directory.clone();file.append(aId);if(file.exists())
transaction.moveUnder(file,trashDir);file=this._directory.clone();file.append(aId+".xpi");if(file.exists()){flushJarCache(file);transaction.moveUnder(file,trashDir);}}
 
try{moveOldAddon(aId);if(aExistingAddonID&&aExistingAddonID!=aId){moveOldAddon(aExistingAddonID);{let oldDataDir=FileUtils.getDir(KEY_PROFILEDIR,["extension-data",aExistingAddonID],false,true);if(oldDataDir.exists()){let newDataDir=FileUtils.getDir(KEY_PROFILEDIR,["extension-data",aId],false,true);if(newDataDir.exists()){let trashData=trashDir.clone();trashData.append("data-directory");transaction.moveUnder(newDataDir,trashData);}
transaction.moveTo(oldDataDir,newDataDir);}}}
if(aCopy){transaction.copy(aSource,this._directory);}
else{if(aSource.isFile())
flushJarCache(aSource);transaction.moveUnder(aSource,this._directory);}}
finally{
try{recursiveRemove(trashDir);}
catch(e){logger.warn("Failed to remove trash directory when installing "+aId,e);}}
let newFile=this._directory.clone();newFile.append(aSource.leafName);try{newFile.lastModifiedTime=Date.now();}catch(e){logger.warn("failed to set lastModifiedTime on "+newFile.path,e);}
this._IDToFileMap[aId]=newFile;XPIProvider._addURIMapping(aId,newFile);if(aExistingAddonID&&aExistingAddonID!=aId&&aExistingAddonID in this._IDToFileMap){delete this._IDToFileMap[aExistingAddonID];}
return newFile;},uninstallAddon:function(aId){let file=this._IDToFileMap[aId];if(!file){logger.warn("Attempted to remove "+aId+" from "+
this._name+" but it was already gone");return;}
file=this._directory.clone();file.append(aId);if(!file.exists())
file.leafName+=".xpi";if(!file.exists()){logger.warn("Attempted to remove "+aId+" from "+
this._name+" but it was already gone");delete this._IDToFileMap[aId];return;}
let trashDir=this.getTrashDir();if(file.leafName!=aId){logger.debug("uninstallAddon: flushing jar cache "+file.path+" for addon "+aId);flushJarCache(file);}
let transaction=new SafeInstallOperation();try{transaction.moveUnder(file,trashDir);}
finally{
 try{recursiveRemove(trashDir);}
catch(e){logger.warn("Failed to remove trash directory when uninstalling "+aId,e);}}
delete this._IDToFileMap[aId];},});function SystemAddonInstallLocation(aName,aDirectory,aScope,aResetSet){this._baseDir=aDirectory;this._nextDir=null;if(aResetSet)
this.resetAddonSet();this._addonSet=this._loadAddonSet();this._directory=null;if(this._addonSet.directory){this._directory=aDirectory.clone();this._directory.append(this._addonSet.directory);logger.info("SystemAddonInstallLocation scanning directory "+this._directory.path);}
else{logger.info("SystemAddonInstallLocation directory is missing");}
DirectoryInstallLocation.call(this,aName,this._directory,aScope);this.locked=true;}
SystemAddonInstallLocation.prototype=Object.create(DirectoryInstallLocation.prototype);Object.assign(SystemAddonInstallLocation.prototype,{_loadAddonSet:function(){try{let setStr=Preferences.get(PREF_SYSTEM_ADDON_SET,null);if(setStr){let addonSet=JSON.parse(setStr);if((typeof addonSet=="object")&&addonSet.schema==1)
return addonSet;}}
catch(e){logger.error("Malformed system add-on set, resetting.");}
return{schema:1,addons:{}};},_saveAddonSet:function(aAddonSet){Preferences.set(PREF_SYSTEM_ADDON_SET,JSON.stringify(aAddonSet));},getAddonLocations:function(){ if(Services.appinfo.inSafeMode)
return new Map();let addons=DirectoryInstallLocation.prototype.getAddonLocations.call(this); for(let id of addons.keys()){if(!(id in this._addonSet.addons))
addons.delete(id);}
return addons;},isActive:function(){return this._directory!=null;},isValidAddon:function(aAddon){if(aAddon.appDisabled){logger.warn(`System add-on ${aAddon.id} isn't compatible with the application.`);return false;}
if(aAddon.unpack){logger.warn(`System add-on ${aAddon.id} isn't a packed add-on.`);return false;}
if(!aAddon.bootstrap){logger.warn(`System add-on ${aAddon.id} isn't restartless.`);return false;}
return true;},isValid:function(aAddons){for(let id of Object.keys(this._addonSet.addons)){if(!aAddons.has(id)){logger.warn(`Expected add-on ${id} is missing from the system add-on location.`);return false;}
let addon=aAddons.get(id);if(addon.version!=this._addonSet.addons[id].version){logger.warn(`Expected system add-on ${id} to be version ${this._addonSet.addons[id].version} but was ${addon.version}.`);return false;}
if(!this.isValidAddon(addon))
return false;}
return true;},resetAddonSet:function(){this._saveAddonSet({schema:1,addons:{}});},cleanDirectories:Task.async(function*(){ if(!(yield OS.File.exists(this._baseDir.path))){return;}
let iterator;try{iterator=new OS.File.DirectoryIterator(this._baseDir.path);}
catch(e){logger.error("Failed to clean updated system add-ons directories.",e);return;}
try{let entries=[];yield iterator.forEach(entry=>{ if(this._directory&&this._directory.path==entry.path)
return; if(this._nextDir&&this._nextDir.path==entry.path)
return;entries.push(entry);});for(let entry of entries){if(entry.isDir){yield OS.File.removeDir(entry.path,{ignoreAbsent:true,ignorePermissions:true,});}
else{yield OS.File.remove(entry.path,{ignoreAbsent:true,});}}}
catch(e){logger.error("Failed to clean updated system add-ons directories.",e);}
finally{iterator.close();}}),installAddonSet:Task.async(function*(aAddons){ yield OS.File.makeDir(this._baseDir.path,{ignoreExisting:true});let newDir=this._baseDir.clone();let uuidGen=Cc["@mozilla.org/uuid-generator;1"].getService(Ci.nsIUUIDGenerator);newDir.append("blank");while(true){newDir.leafName=uuidGen.generateUUID().toString();try{yield OS.File.makeDir(newDir.path,{ignoreExisting:false});break;}
catch(e){}}
let copyAddon=Task.async(function*(addon){let target=OS.Path.join(newDir.path,addon.id+".xpi");logger.info(`Copying ${addon.id} from ${addon._sourceBundle.path} to ${target}.`);try{yield OS.File.copy(addon._sourceBundle.path,target);}
catch(e){logger.error(`Failed to copy ${addon.id} from ${addon._sourceBundle.path} to ${target}.`,e);throw e;}
addon._sourceBundle=new nsIFile(target);});try{yield waitForAllPromises(aAddons.map(copyAddon));}
catch(e){try{yield OS.File.removeDir(newDir.path,{ignorePermissions:true});}
catch(e){logger.warn(`Failed to remove new system add-on directory ${newDir.path}.`,e);}
throw e;} 
let state={schema:1,directory:newDir.leafName,addons:{}};for(let addon of aAddons){state.addons[addon.id]={version:addon.version}}
this._saveAddonSet(state);this._nextDir=newDir;}),});const TemporaryInstallLocation={locked:false,name:KEY_APP_TEMPORARY,scope:AddonManager.SCOPE_TEMPORARY,getAddonLocations:()=>[],isLinkedAddon:()=>false,installAddon:()=>{},uninstallAddon:(aAddon)=>{},getStagingDir:()=>{},}
function WinRegInstallLocation(aName,aRootKey,aScope){this.locked=true;this._name=aName;this._rootKey=aRootKey;this._scope=aScope;this._IDToFileMap={};let path=this._appKeyPath+"\\Extensions";let key=Cc["@mozilla.org/windows-registry-key;1"].createInstance(Ci.nsIWindowsRegKey);
try{key.open(this._rootKey,path,Ci.nsIWindowsRegKey.ACCESS_READ);}
catch(e){return;}
this._readAddons(key);key.close();}
WinRegInstallLocation.prototype={_name:"",_rootKey:null,_scope:null,_IDToFileMap:null, get _appKeyPath(){let appVendor=Services.appinfo.vendor;let appName=Services.appinfo.name; if(AppConstants.MOZ_APP_NAME=="thunderbird"&&appVendor=="")
appVendor="Mozilla"; if(appVendor!="")
appVendor+="\\";return"SOFTWARE\\"+appVendor+appName;},_readAddons:function(aKey){let count=aKey.valueCount;for(let i=0;i<count;++i){let id=aKey.getValueName(i);let file=Cc["@mozilla.org/file/local;1"].createInstance(Ci.nsIFile);file.initWithPath(aKey.readStringValue(id));if(!file.exists()){logger.warn("Ignoring missing add-on in "+file.path);continue;}
this._IDToFileMap[id]=file;XPIProvider._addURIMapping(id,file);}},get name(){return this._name;},get scope(){return this._scope;},getAddonLocations:function(){let locations=new Map();for(let id in this._IDToFileMap){locations.set(id,this._IDToFileMap[id].clone());}
return locations;},isLinkedAddon:function(aId){return true;}};var addonTypes=[new AddonManagerPrivate.AddonType("extension",URI_EXTENSION_STRINGS,STRING_TYPE_NAME,AddonManager.VIEW_TYPE_LIST,4000,AddonManager.TYPE_SUPPORTS_UNDO_RESTARTLESS_UNINSTALL),new AddonManagerPrivate.AddonType("theme",URI_EXTENSION_STRINGS,STRING_TYPE_NAME,AddonManager.VIEW_TYPE_LIST,5000),new AddonManagerPrivate.AddonType("dictionary",URI_EXTENSION_STRINGS,STRING_TYPE_NAME,AddonManager.VIEW_TYPE_LIST,7000,AddonManager.TYPE_UI_HIDE_EMPTY|AddonManager.TYPE_SUPPORTS_UNDO_RESTARTLESS_UNINSTALL),new AddonManagerPrivate.AddonType("locale",URI_EXTENSION_STRINGS,STRING_TYPE_NAME,AddonManager.VIEW_TYPE_LIST,8000,AddonManager.TYPE_UI_HIDE_EMPTY|AddonManager.TYPE_SUPPORTS_UNDO_RESTARTLESS_UNINSTALL),];

if(Preferences.get("experiments.supported",false)){addonTypes.push(new AddonManagerPrivate.AddonType("experiment",URI_EXTENSION_STRINGS,STRING_TYPE_NAME,AddonManager.VIEW_TYPE_LIST,11000,AddonManager.TYPE_UI_HIDE_EMPTY|AddonManager.TYPE_SUPPORTS_UNDO_RESTARTLESS_UNINSTALL));}
AddonManagerPrivate.registerProvider(XPIProvider,addonTypes);