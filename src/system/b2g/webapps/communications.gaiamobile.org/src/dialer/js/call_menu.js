'use strict';
var CallMenu = {
  inCallMenuLayer: -1,
  dial: function cm_dial(cardIndex, phoneNumber, onlyVoice) {
    onlyVoice = true;//PR4616-xuemingli@t2mobile.com-Phone shows video call option in call
    var cardSelected = -1;
    var isVideo = false;
    var lastNavId = NavigationMap.latestNavId;

    if (this.inCallMenuLayer !== -1 ||
      (this.optionMenu && this.optionMenu.form.classList.contains('visible'))) {
      return;
    }
    if (window.navigator.mozMobileConnections.length < 2) {
      cardSelected = 0;
    } else if (!window.navigator.mozMobileConnections[1].iccId) {
      cardSelected = 0;
    } else if (!window.navigator.mozMobileConnections[0].iccId) {
      cardSelected = 1;
    } else if (cardIndex !== -1) {
      cardSelected = cardIndex;
    }
    if (cardSelected !== -1) {
      this.inCallMenuLayer = 1;
      if (onlyVoice) {
        var dialPhoneNumber = phoneNumber || OptionHelper.option.phoneNumber ||
          document.querySelector('#call-log-container li').dataset.phoneNumber;
        CallHandler.inCall = true;
        CallHandler.call(dialPhoneNumber, cardSelected, isVideo);
        setTimeout(() => {
          this.inCallMenuLayer = -1;
        }, 3000);
        return;
      }
    } else {
      this.inCallMenuLayer = 0;
    }
    var items = [];
    var _ = navigator.mozL10n.get;

    var timeoutHandle = null;
    var params = {
      classes: ['group-menu', 'softkey'],
      header: _('select') || '',
      items: null,
      complete: () => {
        OptionHelper.show(NavigationMap.latestClassName);
        NavigationMap.reset(NavigationMap.latestClassName, lastNavId);
        var dialPhoneNumber = phoneNumber || OptionHelper.option.phoneNumber;
        CallHandler.inCall = true;
        if (onlyVoice) {
          CallHandler.call(dialPhoneNumber, cardSelected, false);
        } else {
          CallHandler.call(dialPhoneNumber, cardSelected, isVideo);
        }
        setTimeout(() => {
          this.inCallMenuLayer = -1;
        }, 3000);
      },
      menuClassName: 'menu-button'
    };
    if (this.inCallMenuLayer) {
      items.push({
        name: _('call'),
        method: () => {
          isVideo = false;
        }
      });
      items.push({
        name: _('video-call'),
        method: () => {
          isVideo = true;
        }
      });
    } else {
      var conn = navigator.mozMobileConnections[0];
      var operator = MobileOperator.userFacingInfo(conn).operator;
      items.push({
        name: operator ? _('sim-with-index-and-carrier',
          { 'index': 1, 'carrier': operator }) :
          _('sim-without-carrier', { 'index': 1 }),
        method: () => {
          isVideo = false;
        }
      });
      conn = navigator.mozMobileConnections[1];
      operator = MobileOperator.userFacingInfo(conn).operator;
      items.push({
        name: operator ? _('sim-with-index-and-carrier',
          { 'index': 2, 'carrier': operator }) :
          _('sim-without-carrier', { 'index': 2 }),
        method: () => {
          isVideo = true;
        }
      });
    }

    params.items = items;
    this.optionMenu = new OptionMenu(params);
    this.optionMenu.show().then(() => {
      var menu = document.getElementById('mainmenu');
      var header = this.optionMenu.form.querySelector('h1');
      header.setAttribute('style', 'bottom:' + menu.offsetHeight + 'px');
    });
    secondCall = false;
    NavigationMap.reset_options();
    OptionHelper.show('chooser');
    var menu = document.getElementById('mainmenu');
    menu.addEventListener('keydown', (evt) => {
      switch (evt.key) {
        case 'BrowserBack':
        case 'Backspace':
          evt.stopPropagation();
          evt.preventDefault();
          CallMenu.optionMenu.hide();
          CallHandler.inCall = false;
          OptionHelper.show(NavigationMap.latestClassName);
          NavigationMap.reset(NavigationMap.latestClassName, lastNavId);
          this.inCallMenuLayer = -1;
          break;
      }
    });
    menu.addEventListener('click', (evt) => {
      if (this.inCallMenuLayer === 0) {
        var items = menu.querySelectorAll('.menu-button.p-pri');
        if (evt.target === items[0]) {
          cardSelected = 0;
        } else {
          cardSelected = 1;
        }
        if (!onlyVoice) {
          items[0].textContent = _('call');
          items[1].textContent = _('video-call');
          NavigationMap.reset_options();
        } else {
          this.inCallMenuLayer = 2;
        }
      }
      if (this.inCallMenuLayer !== 2) {
        evt.stopPropagation();
        evt.preventDefault();
      }
      this.inCallMenuLayer++;
    }, true);
  }
};
