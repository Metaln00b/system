
define(['require','tmpl!./tng/account_settings_server.html','evt','l10n!','cards','./base','template!./settings_account.html','./editor_mixins','./account_prefs_mixins'],function(require) {

var tngAccountSettingsServerNode =
                             require('tmpl!./tng/account_settings_server.html'),
    evt = require('evt'),
    mozL10n = require('l10n!'),
    cards = require('cards'),
    trailingRegExp = /\s+$/;

var self;
var queryChild;
var dataChanged;

return [
  require('./base')(require('template!./settings_account.html')),
  require('./editor_mixins'),
  require('./account_prefs_mixins'),
  {
    onArgs: function(args) {
      this.account = args.account;
      this.identity = this.account.identities[0];

      this.headerLabel.textContent = args.account.name;
      this.signatureNode.innerHTML = '<br>';

      this._bindPrefs('tng-account-check-interval',
                      'tng-notify-mail',
                      'tng-sound-onsend',
                      'tng-signature-enable',
                      'signature-box',
                      'account-label');

      this.accountNameNode.textContent =
                     (this.identity && this.identity.name) || this.account.name;

      // ActiveSync, IMAP and SMTP are protocol names, no need to be localized
      this.accountTypeNode.textContent =
        (this.account.type === 'activesync') ? 'ActiveSync' :
        (this.account.type === 'imap+smtp') ? 'IMAP+SMTP' : 'POP3+SMTP';

      // Handle default account checkbox. If already a default, then the
      // checkbox cannot be unchecked. The default is changed by going to an
      // account that is not the default and checking that checkbox.
      if (this.account.isDefault) {
        this.defaultInputNode.disabled = true;
        this.defaultInputNode.checked = true;
      } else {
        this.defaultLabelNode.addEventListener('click',
                                      this.onChangeDefaultAccount.bind(this),
                                      false);
      }

      if (this.account.type === 'activesync') {
        this.synchronizeNode.value = this.account.syncRange;
      } else {
        // Remove it from the DOM so that css selectors for last-child can work
        // efficiently. Also, it just makes the overall DOM smaller.
        this.syncSettingNode.parentNode.removeChild(this.syncSettingNode);
      }

      this.account.servers.forEach(function(server, index) {
        var serverNode = tngAccountSettingsServerNode.cloneNode(true);
        var serverLabel = serverNode.querySelector('.tng-account-server-label');

        mozL10n.setAttributes(serverLabel,
                              'settings-' + server.type + '-label');
        serverLabel.addEventListener('click',
          this.onClickServers.bind(this, index), false);

        this.serversContainer.appendChild(serverNode);
      }.bind(this));

      var credL10nId = 'settings-account-userpass';
      if (this.account.authMechanism === 'oauth2') {
        credL10nId = 'settings-account-useroauth2';
      }
      mozL10n.setAttributes(this.accountCredNode, credL10nId);

      this._bindEditor(this.signatureNode);
      self = this;
      dataChanged = false;
    },

    onBack: function() {
      cards.removeCardAndSuccessors(this, 'animate', 1);
    },

    updateFocusList: function(){
      NavigationMap.navSetup('cards-settings-account',
          'cards-settings-account ul li:not(.hidden)');
    },

    handleKeyDown: function(evt) {
      if (NavigationMap.getCurrentControl().queryChild === queryChild &&
          (!document.getElementById('Symbols'))) {
        switch (evt.key) {
          case 'Accept':
          case 'Enter':
            var selectEl = document
                .querySelector('.scrollregion-below-header .focus select');
            if (selectEl) {
              selectEl.focus();
            }
            break;
          case 'Backspace':
            evt.preventDefault();
            if (document.querySelector('#confirm-dialog-container' +
                ' gaia-confirm')) {
              cards._endKeyClicked = false;
            } else {
              self.onBack();
            }
            break;
        }
      }
      if (evt.key === 'ArrowUp' || evt.key === 'ArrowDown') {
        self.updateCsk();
      }
    },

    updateCsk: function() {
      if (document.activeElement.classList.contains('no-select')) {
        if (document.activeElement.classList.contains('signature-box')) {
          window.option.buttonCsk.textContent = navigator.mozL10n.get('enter');
        } else {
          window.option.buttonCsk.textContent = '';
        }
      } else {
        window.option.buttonCsk.textContent = navigator.mozL10n.get('select');
      }
    },

    endkeyHandler: function(e) {
      var cb = e.detail.callback;
      if (document.getElementById('Symbols')) {
        var evt = new CustomEvent('back-accepted');
        window.dispatchEvent(evt);
      }

      if (dataChanged) {
        var dialogConfig = {
          title: {
            id: 'confirmation-title',
            args: {}
          },
          body: {
            id: 'data-loss-warning-message',
            args: {}
          },
          desc: {
            id: 'back-to-edit-message',
            args: {}
          },
          cancel: {
            l10nId: 'exit',
            priority: 1,
            callback: function() {
              cb();
            }
          },
          confirm: {
            l10nId: 'return',
            priority: 3,
            callback: function() {
              cards._endKeyClicked = false;
            }
          }
        };

        var dialog = new ConfirmDialogHelper(dialogConfig);
        dialog.show(document.getElementById('confirm-dialog-container'));
      } else {
        cb();
      }
    },

    onBodyNodeKeydown: function(evt) {
      var range = window.getSelection().getRangeAt(0);
      var currentElement = range.startContainer;

      switch (evt.key) {
        case 'ArrowUp':
          if ((currentElement === document.activeElement ||
              currentElement === document.activeElement.firstChild) &&
              (range.startOffset === 0 ||
              currentElement.textContent.length === 0)) {
            break;
          }
          evt.stopPropagation();
          break;
        case 'ArrowDown':
          if ((currentElement === document.activeElement && isLastLine(range))
              || isLastNode(range)) {
            break;
          }
          evt.stopPropagation();
          break;
        case 'ArrowLeft':
        case 'ArrowRight':
          break;
      }

      function isLastLine(range) {
        if (document.activeElement.lastChild.tagName === 'BR' &&
            range.startOffset === (document.activeElement.childNodes.length - 1)
        ) {
          return true;
        } else if (range.startOffset ===
            document.activeElement.childNodes.length) {
          return true;
        }
        return false;
      }

      function isLastNode(range) {
        if (range.startOffset === range.startContainer.length) {
          return true;
        }
        return false;
      }
    },

    getTextFromEditor: function() {
      var text = this.fromEditor().replace(trailingRegExp, '');
      return text;
    },

    onInfoInput: function(event) {
      dataChanged = true;
    },

    onCardVisible: function(navDirection) {
      const CARD_NAME = this.localName;
      const QUERY_CHILD = CARD_NAME + ' ' + 'ul li:not(.hidden)';
      const CONTROL_ID = CARD_NAME + ' ' + QUERY_CHILD;
      queryChild = QUERY_CHILD;

      var menuOptions = [
      {
        name: 'Delete Account',
        l10nId: 'settings-account-delete',
        priority: 1,
        method: function() {
          self.onDelete();
        }
      },
      {
        name: 'Select',
        l10nId: 'select',
        priority: 2
      },
      {
        name: 'Save',
        l10nId: 'opt-save',
        priority: 3,
        method: function() {
          var signature = self.getTextFromEditor();
          var accountLabel = self.accountLabelNode.value.trim();

          // Only push the signature if it was changed
          if (signature !== self.identity.signature) {
            self.identity.modifyIdentity({ signature: signature });
          }

          if (accountLabel !== self.account.label) {
            self.account.modifyAccount({ label: accountLabel });
          }

          cards.removeCardAndSuccessors(self, 'animate', 1);
        }
      }];

      // forward: new card pushed
      if (navDirection === 'forward') {
        NavigationMap.navSetup(CARD_NAME, QUERY_CHILD);
        NavigationMap.setCurrentControl(CONTROL_ID);
        NavigationMap.setFocus('first');
        NavigationMap.setSoftKeyBar(menuOptions);
      }
      // back: hidden card is restored
      else if (navDirection === 'back') {
        NavigationMap.setCurrentControl(CONTROL_ID);
        NavigationMap.setFocus('restore');
        NavigationMap.setSoftKeyBar(menuOptions);
      }
      this.updateSignatureBox();

      window.addEventListener('keydown', self.handleKeyDown);
      document.addEventListener('update-list', self.updateFocusList);
      window.addEventListener('email-endkey', self.endkeyHandler);

      var signatureNode = document.querySelector('.signature-box');
      var signatureNodeParentLi =
          document.querySelector('.settings-account-signature');
      signatureNodeParentLi.addEventListener('focus', function(evt) {
        signatureNode.focus();
        if (!evt.relatedTarget.classList.contains('signature-box')) {
          var selection = window.getSelection();
          var range = document.createRange();
          var lastChild = signatureNode.lastElementChild;
          if (lastChild.nodeName === 'BR') {
            range.setStartBefore(lastChild);
          } else {
            range.setStartAfter(lastChild);
          }
          selection.removeAllRanges();
          selection.addRange(range);
        }
      });

      this.accountLabelNode.parentNode.addEventListener('focus', function() {
        self.accountLabelNode.focus();
        self.accountLabelNode.setSelectionRange(9999, 9999);
      });

      if (document.activeElement.classList.contains('no-select')) {
        window.option.buttonCsk.textContent = '';
      }
    },

    onClickCredentials: function() {
      window.removeEventListener('keydown', self.handleKeyDown);
      cards.pushCard(
        'settings_account_credentials', 'animate',
        {
          account: this.account
        },
        'right');
    },

    onClickServers: function(index) {
      window.removeEventListener('keydown', self.handleKeyDown);
      var focused = document.querySelector('.focus');
      if (focused) {
        focused.classList.remove('focus');
      }
      cards.pushCard(
        'settings_account_servers', 'animate',
        {
          account: this.account,
          index: index
        },
        'right');
    },

    onChangeDefaultAccount: function(event) {
      event.stopPropagation();
      if (event.preventBubble) {
        event.preventBubble();
      }

      if (!this.defaultInputNode.disabled) {
        this.defaultInputNode.disabled = true;
        this.defaultInputNode.checked = true;
        this.account.modifyAccount({ setAsDefault: true });
      }
    },

    onChangeSynchronize: function(event) {
      this.account.modifyAccount({ syncRange: event.target.value });
    },

    onDelete: function() {
      var account = this.account;

      var dialogConfig = {
        title: {
          id: 'confirm-dialog-title',
          args: {}
        },
        body: {
          id: 'settings-account-delete-prompt',
          args: {
            account: account.name
          }
        },
        desc: {
          id: 'settings-account-delete-desc',
          args: {}
        },
        cancel: {
          l10nId: 'settings-account-delete-cancel',
          priority: 1,
          callback: function() {
          }
        },
        confirm: {
          l10nId: 'settings-account-delete-confirm',
          priority: 3,
          callback: function() {
            account.deleteAccount();
            setTimeout( () => {
              self.onBack();
              evt.emit('accountDeleted', account);
            }, 100);
          }
        }
      };

      var dialog = new ConfirmDialogHelper(dialogConfig);
      dialog.show(document.getElementById('confirm-dialog-container'));

      setTimeout( () => {
        document.querySelector('#confirm-dialog-container' +
                               ' .noborder').style.color = '#657073';
      }, 80);
    },

    die: function() {
      window.removeEventListener('keydown', self.handleKeyDown);
      document.removeEventListener('update-list', self.updateFocusList);
      window.removeEventListener('email-endkey', self.endkeyHandler);
    }
  }
];
});
