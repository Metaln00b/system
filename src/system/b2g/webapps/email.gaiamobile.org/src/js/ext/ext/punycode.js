(function(e) {
    function t(e) {
        throw RangeError(x[e]);
    }
    function n(e, t) {
        for (var n = e.length; n--; ) e[n] = t(e[n]);
        return e;
    }
    function o(e, t) {
        return n(e.split(M), t).join(".");
    }
    function r(e) {
        for (var t, n, o = [], r = 0, s = e.length; s > r; ) t = e.charCodeAt(r++), t >= 55296 && 56319 >= t && s > r ? (n = e.charCodeAt(r++), 
        56320 == (64512 & n) ? o.push(((1023 & t) << 10) + (1023 & n) + 65536) : (o.push(t), 
        r--)) : o.push(t);
        return o;
    }
    function s(e) {
        return n(e, function(e) {
            var t = "";
            return e > 65535 && (e -= 65536, t += D(55296 | 1023 & e >>> 10), e = 56320 | 1023 & e), 
            t += D(e);
        }).join("");
    }
    function i(e) {
        return 10 > e - 48 ? e - 22 : 26 > e - 65 ? e - 65 : 26 > e - 97 ? e - 97 : v;
    }
    function a(e, t) {
        return e + 22 + 75 * (26 > e) - ((0 != t) << 5);
    }
    function c(e, t, n) {
        var o = 0;
        for (e = n ? O(e / w) : e >> 1, e += O(e / t); e > N * S >> 1; o += v) e = O(e / N);
        return O(o + (N + 1) * e / (e + T));
    }
    function d(e) {
        var n, o, r, a, d, u, l, h, p, f, g = [], m = e.length, y = 0, T = E, w = I;
        for (o = e.lastIndexOf(C), 0 > o && (o = 0), r = 0; o > r; ++r) e.charCodeAt(r) >= 128 && t("not-basic"), 
        g.push(e.charCodeAt(r));
        for (a = o > 0 ? o + 1 : 0; m > a; ) {
            for (d = y, u = 1, l = v; a >= m && t("invalid-input"), h = i(e.charCodeAt(a++)), 
            (h >= v || h > O((_ - y) / u)) && t("overflow"), y += h * u, p = w >= l ? b : l >= w + S ? S : l - w, 
            !(p > h); l += v) f = v - p, u > O(_ / f) && t("overflow"), u *= f;
            n = g.length + 1, w = c(y - d, n, 0 == d), O(y / n) > _ - T && t("overflow"), T += O(y / n), 
            y %= n, g.splice(y++, 0, T);
        }
        return s(g);
    }
    function u(e) {
        var n, o, s, i, d, u, l, h, p, f, g, m, y, T, w, A = [];
        for (e = r(e), m = e.length, n = E, o = 0, d = I, u = 0; m > u; ++u) g = e[u], 128 > g && A.push(D(g));
        for (s = i = A.length, i && A.push(C); m > s; ) {
            for (l = _, u = 0; m > u; ++u) g = e[u], g >= n && l > g && (l = g);
            for (y = s + 1, l - n > O((_ - o) / y) && t("overflow"), o += (l - n) * y, n = l, 
            u = 0; m > u; ++u) if (g = e[u], n > g && ++o > _ && t("overflow"), g == n) {
                for (h = o, p = v; f = d >= p ? b : p >= d + S ? S : p - d, !(f > h); p += v) w = h - f, 
                T = v - f, A.push(D(a(f + w % T, 0))), h = O(w / T);
                A.push(D(a(h, 0))), d = c(o, y, s == i), o = 0, ++s;
            }
            ++o, ++n;
        }
        return A.join("");
    }
    function l(e) {
        return o(e, function(e) {
            return A.test(e) ? d(e.slice(4).toLowerCase()) : e;
        });
    }
    function h(e) {
        return o(e, function(e) {
            return k.test(e) ? "xn--" + u(e) : e;
        });
    }
    var p = "object" == typeof exports && exports, f = "object" == typeof module && module && module.exports == p && module, g = "object" == typeof global && global;
    (g.global === g || g.window === g) && (e = g);
    var m, y, _ = 2147483647, v = 36, b = 1, S = 26, T = 38, w = 700, I = 72, E = 128, C = "-", A = /^xn--/, k = /[^ -~]/, M = /\x2E|\u3002|\uFF0E|\uFF61/g, x = {
        overflow: "Overflow: input needs wider integers to process",
        "not-basic": "Illegal input >= 0x80 (not a basic code point)",
        "invalid-input": "Invalid input"
    }, N = v - b, O = Math.floor, D = String.fromCharCode;
    if (m = {
        version: "1.2.4",
        ucs2: {
            decode: r,
            encode: s
        },
        decode: d,
        encode: u,
        toASCII: h,
        toUnicode: l
    }, "function" == typeof define && "object" == typeof define.amd && define.amd) define("punycode", [], function() {
        return m;
    }); else if (p && !p.nodeType) if (f) f.exports = m; else for (y in m) m.hasOwnProperty(y) && (p[y] = m[y]); else e.punycode = m;
})(this);