define([ "../imapchew", "../../allback", "exports" ], function(e, n, t) {
    function o(e) {
        this.storage = e.storage, this.connection = e.connection, this.knownHeaders = e.knownHeaders || [], 
        this.knownUIDs = e.knownUIDs || [], this.newUIDs = e.newUIDs || [], this._progress = e.initialProgress || .25, 
        this._progressCost = (this.knownUIDs.length ? a : 0) + i * this.knownUIDs.length + (this.newUIDs.length ? c : 0) + l * this.newUIDs.length, 
        this.onprogress = null, this.oncomplete = null, this._beginSync();
    }
    var r = [ "BODYSTRUCTURE", "INTERNALDATE", "FLAGS", "BODY.PEEK[HEADER.FIELDS (FROM TO CC BCC SUBJECT REPLY-TO MESSAGE-ID REFERENCES)]" ], s = [ "FLAGS" ], a = 20, i = 1, c = 20, l = 5;
    o.prototype = {
        _updateProgress: function(e) {
            this._progress += e, this.onprogress && this.onprogress(.25 + .75 * (this._progress / this._progressCost));
        },
        _beginSync: function() {
            var e = n.latch();
            this.newUIDs.length && this._handleNewUids(e.defer("new")), this.knownUIDs.length && this._handleKnownUids(e.defer("known")), 
            e.then(function() {
                this.oncomplete && this.oncomplete(this.newUIDs.length, this.knownUIDs.length);
            }.bind(this));
        },
        _handleNewUids: function(t) {
            var o = [], s = this;
            this.connection.listMessages(this.newUIDs, r, {
                byUid: !0
            }, function(r, a) {
                if (r) return console.warn("New UIDs fetch error, ideally harmless:", r), t(), void 0;
                var i = n.latch();
                a.forEach(function(n) {
                    var t = n.flags.indexOf("\\Recent");
                    -1 !== t && n.flags.splice(t, 1);
                    try {
                        var r = e.chewHeaderAndBodyStructure(n, s.storage.folderId, s.storage._issueNewHeaderId());
                        r.header.bytesToDownloadForBodyDisplay = e.calculateBytesToDownloadForImapBodyDisplay(r.bodyInfo), 
                        o.push(r), s.storage.addMessageHeader(r.header, r.bodyInfo, i.defer()), s.storage.addMessageBody(r.header, r.bodyInfo, i.defer());
                    } catch (a) {
                        console.warn("message problem, skipping message", a, "\n", a.stack);
                    }
                }.bind(this)), i.then(t);
            }.bind(this));
        },
        _handleKnownUids: function(e) {
            var t = this;
            this.connection.listMessages(t.knownUIDs, s, {
                byUid: !0
            }, function(o, r) {
                if (o) return console.warn("Known UIDs fetch error, ideally harmless:", o), e(), 
                void 0;
                var s = n.latch();
                r.forEach(function(e, n) {
                    console.log("FETCHED", n, "known id", t.knownHeaders[n].id, "known srvid", t.knownHeaders[n].srvid, "actual id", e.uid);
                    var o = e.flags.indexOf("\\Recent");
                    if (-1 !== o && e.flags.splice(o, 1), t.knownHeaders[n].srvid !== e.uid && (n = t.knownUIDs.indexOf(e.uid), 
                    -1 === n)) return console.warn("Server fetch reports unexpected message:", e.uid), 
                    void 0;
                    var r = t.knownHeaders[n], a = r.flags.slice();
                    a.sort(), e.flags.sort(), -1 === r.flags.indexOf("\\Seen") && -1 !== e.flags.indexOf("\\Seen") ? t.storage.folderMeta.unreadCount-- : -1 !== r.flags.indexOf("\\Seen") && -1 === e.flags.indexOf("\\Seen") && t.storage.folderMeta.unreadCount++, 
                    r.flags.toString() !== e.flags.toString() ? (console.warn('  FLAGS: "' + r.flags.toString() + '" VS "' + e.flags.toString() + '"'), 
                    r.flags = e.flags, t.storage.updateMessageHeader(r.date, r.id, !0, r, null, s.defer())) : t.storage.unchangedMessageHeader(r);
                }), t._updateProgress(a + i * t.knownUIDs.length), s.then(e);
            }.bind(this));
        }
    }, t.Sync = o;
});