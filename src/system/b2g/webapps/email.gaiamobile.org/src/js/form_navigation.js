define([ "require" ], function() {
    function e(e) {
        function t(e, t) {
            for (var n in t) e[n] = t[n];
            return e;
        }
        if (!e.formElem) throw new Error("The form element should be defined.");
        var n = this;
        this.options = t({
            formElem: null,
            checkFormValidity: function() {
                return n.options.formElem.checkValidity();
            },
            onLast: function() {}
        }, e), this.options.formElem.addEventListener("keypress", this.onKeyPress.bind(this)), 
        this.options.formElem.addEventListener("click", this.onClick.bind(this));
    }
    return e.prototype = {
        onKeyPress: function(e) {
            if (13 === e.keyCode) {
                var t = this.focusNextInput(e);
                !t && this.options.checkFormValidity() && this.options.onLast(e);
            }
        },
        onClick: function(e) {
            if ("reset" === e.target.type) for (var t = this.options.checkFormValidity(), n = this.options.formElem.getElementsByTagName("button"), o = 0; o < n.length; o++) {
                var r = n[o];
                "reset" !== r.type && (r.disabled = !t);
            }
        },
        focusNextInput: function(e) {
            for (var t = e.target, n = this.options.formElem.getElementsByTagName("input"), o = !1, r = 0; r < n.length; r++) {
                var i = n[r];
                if (t !== i) {
                    if (o && "hidden" !== i.type && "button" !== i.type && (i.focus(), document.activeElement === i)) return i;
                } else o = !0;
            }
            return t.blur(), null;
        }
    }, e;
});