(function(e) {
    e.ComponentUtils = {
        style: function(e) {
            var t = document.createElement("style"), n = e + "style.css", o = this;
            t.setAttribute("scoped", ""), t.innerHTML = "@import url(" + n + ");", this.appendChild(t), 
            this.style.visibility = "hidden", t.addEventListener("load", function() {
                o.shadowRoot && o.shadowRoot.appendChild(t.cloneNode(!0)), o.style.visibility = "";
            });
        }
    };
})(window);