/* global SettingsSoftkey */
define(['require','modules/settings_service','modules/settings_panel','shared/toaster','shared/settings_listener'],function(require) {
  
  var SettingsService = require('modules/settings_service');
  var SettingsPanel = require('modules/settings_panel');
  var Toaster = require('shared/toaster');
  var SettingsListener = require('shared/settings_listener');

  return function ctor_call_cf_details_panel() {
    var _callForwardingKey,
      _callForwardingNumber;
    var _selectEnable = false;
    var _saveEnable = false;
    var _callForwardingEnabled = false;

    var _inputItem,
      _selectItem;

    var _savedValue = false;

    var settingsKey = {'call-cf-unconditionalSettings' : 'ril.cf.unconditional.enabled',
                       'call-cf-mobileBusySettings' : 'ril.cf.mobilebusy.enabled',
                       'call-cf-noReplySettings' : 'ril.cf.noreply.enabled',
                       'call-cf-notReachableSettings' : 'ril.cf.notreachable.enabled'};

    var numbersKey = {'call-cf-unconditionalSettings' : 'ril.cf.unconditional.number',
                      'call-cf-mobileBusySettings' : 'ril.cf.mobilebusy.number',
                      'call-cf-noReplySettings' : 'ril.cf.noreply.number',
                      'call-cf-notReachableSettings' : 'ril.cf.notreachable.number'};

    function _initSoftkey() {
      var params = {
        menuClassName: 'menu-button',
        header: {
          l10nId: 'message'
        },
        items: [{
          name: 'Cancel',
          l10nId: 'cancel',
          priority: 1,
          method: function() {
            SettingsService.navigate('call-cfSettings');
          }
        }]
      };

      if (_selectEnable) {
        params.items.push({
          name: 'Select',
          l10nId: 'select',
          priority: 2,
          method: function() {}
        });
      }

      if (_saveEnable) {
        params.items.push({
          name: 'Save',
          l10nId: 'save',
          priority: 3,
          method: function() {
            _setCallForwardingOption();
            SettingsService.navigate('call-cfSettings');
          }
        });
      }
      SettingsSoftkey.init(params);
      SettingsSoftkey.show();
    }

    function _updateUI() {
      var request = SettingsListener.getSettingsLock().get(_callForwardingKey);

      request.onsuccess = function() {
        _savedValue = request.result[_callForwardingKey];
        _callForwardingEnabled = request.result[_callForwardingKey];
        _selectItem.value = _callForwardingEnabled ? 'true' : 'false';
        _updateInputStatus();
      };

      request.onerror = function() {
        var toast = {
          messageL10nId: 'callForwardingSetError',
          latency: 2000,
          useTransition: true
        };
        Toaster.showToast(toast);
        SettingsService.navigate('call-cfSettings');
      };
    }

    function _updateInputStatus() {
      var enabled = (_selectItem.value === 'true' || false);
      _inputItem.parentNode.hidden = !enabled;
      if (enabled) {
        SettingsListener.getSettingsLock().get(_callForwardingNumber)
          .then((result) => {
            var number = result[_callForwardingNumber];
            _inputItem.setAttribute('value', number);
            _selectEnable = true;
            _saveEnable = false;
            _initSoftkey();
            NavigationMap.reset();
          })
          .catch((error) => console.log('Promise rejects due to ' + error));
      } else {
        _selectEnable = true;
        _saveEnable = _callForwardingEnabled;
        _initSoftkey();
        NavigationMap.reset();
      }
    }

    function _setCallForwardingOption() {
      var enabled = (_selectItem.value === 'true' || false);
      var option = {};
      option[_callForwardingKey] = enabled;
      SettingsListener.getSettingsLock().set(option);
    }

    function _addFocus() {
      _inputItem.focus();
      _selectEnable = false;
      _initSoftkey();
    }

    function _updateSelectSoftkey() {
      _selectEnable = true;
      if (_selectItem.value === 'true') {
        _saveEnable = false;
      } else {
        _saveEnable = _callForwardingEnabled;
      }
      _initSoftkey();
    }

    function _updateSaveSoftkey() {
      _selectEnable = false;
      if (_selectItem.value === 'true' && _inputItem.value.length) {
        _saveEnable = true;
      } else {
        _saveEnable = false;
      }
      _initSoftkey();
    }

    return SettingsPanel({
      onInit: function(panel) {
        _callForwardingKey = settingsKey[panel.id];
        _callForwardingNumber = numbersKey[panel.id];
        _selectItem = panel.querySelector('div select');
        _inputItem = panel.querySelector('li input');
      },

      onBeforeShow: function() {
        _initSoftkey();
        if (!_callForwardingKey) {
          return;
        }
        _updateUI();
        _selectItem.parentNode.parentNode.addEventListener('focus', _updateSelectSoftkey);
        _selectItem.addEventListener('change', _updateInputStatus);
        _inputItem.parentNode.addEventListener('focus', _addFocus);
        _inputItem.addEventListener('input', _updateSaveSoftkey);
      },

      onBeforeHide: function() {
        SettingsSoftkey.hide();
        _selectItem.parentNode.parentNode.removeEventListener('focus', _updateSelectSoftkey);
        _selectItem.removeEventListener('change', _updateInputStatus);
        _inputItem.parentNode.removeEventListener('focus', _addFocus);
        _inputItem.removeEventListener('input', _updateSaveSoftkey);
      }
    });
  };
});
