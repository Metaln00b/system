/*global ActivityHandler, MozActivity, TelephonyHelper */

(function(exports) {
'use strict';

function handleActivity(activity, onsuccess, onerror) {
  // Note: the MozActivity is intentionally constructed in the caller and
  // passed down to this function in order to make it possible to pass an
  // inline option to its constructor in the caller for the sake of static
  // analysis tools.  Please do not change that!

  if (typeof onsuccess === 'function') {
    activity.onsuccess = onsuccess;
  }

  activity.onerror = typeof onerror === 'function' ? onerror : function(error) {
    console.warn('Unhandled error spawning activity; ' + error.message);
  };
}

var ActivityPicker = {
  dial: function ap_call(number, isDirect, onsuccess, onerror) {
    function optionMenu() {
      document.removeEventListener('transitionend', optionMenu);
      ThreadUI.simSelectOptions(telephonyCall);
    }

    function telephonyCall(serviceID) {
      TelephonyHelper.call(number, serviceID, false, onsuccess, onerror);
    }

    if (Settings.telephonyServiceId === -1) {
      // Bug311-gang-chen@t2mobile.com-[DSDS] no phone number choose begin
      // Bug655-gang-chen@t2mobile.com-option menu misplaced begin
      /*if (ThreadUI.isOptionsCall) {
        ThreadUI.isOptionsCall = false;
        document.addEventListener('transitionend', optionMenu);
      } else {
        optionMenu();
      }*/
      // Bug655-gang-chen@t2mobile.com-option menu misplaced end
      // Bug311-gang-chen@t2mobile.com-[DSDS] no phone number choose end
     // if (isDirect) {
       // ThreadUI.simSelectOptions(telephonyCall);
      if (Settings.hasSeveralSim()) {
        if (isDirect) {
          // If there is option menu, not create it again.
          if (!document.getElementById('option-menu')) {
            ThreadUI.simSelectOptions(telephonyCall);
          }
        } else {
          document.addEventListener('transitionend', optionMenu);
        }
      } else {
        var telServiceId = ThreadUI.selectSIMCard();
        TelephonyHelper.call(number, telServiceId);
      }
    } else {
      // XXX, call a number directly, we should implement an activity to
      // do this later
      // If there is only one sim card slot, we use sim1 to call number.
      var telServiceId = Settings.telephonyServiceId;
      if (telServiceId === null) {
        telServiceId = 0;
      }
      TelephonyHelper.call(number, telServiceId);
    }
  },
  email: function ap_email(email, onsuccess, onerror) {
    handleActivity(new MozActivity({
      name: 'new',
      data: {
        type: 'mail',
        URI: 'mailto:' + email
      }
    }), onsuccess, onerror);
  },
  url: function ap_browse(url) {
    window.open(url);
  },
  createNewContact:
   function ap_createNewContact(contactProps, onsuccess, onerror) {
    handleActivity(new MozActivity({
      name: 'new',
      data: {
        type: 'webcontacts/contact',
        params: contactProps
      }
    }), onsuccess, onerror);
  },
  addToExistingContact:
   function ap_addToExistingContact(contactProps, onsuccess, onerror) {
    handleActivity(new MozActivity({
      name: 'update',
      data: {
        type: 'webcontacts/contact',
        params: contactProps
      }
    }), onsuccess, onerror);
  },
  viewContact:
   function ap_viewContact(contactProps, onsuccess, onerror) {
    handleActivity(new MozActivity({
      name: 'open',
      data: {
        type: 'webcontacts/contact',
        params: contactProps
      }
    }), onsuccess, onerror);
  },
  sendMessage: function ap_sendMessage(number, contact) {
    // Using ActivityHandler here to navigate to Composer view in the same way
    // as it's done for real activity.
    ActivityHandler.toView({
      number: number,
      contact: contact
    });
  }
};

exports.ActivityPicker = ActivityPicker;

}(this));
