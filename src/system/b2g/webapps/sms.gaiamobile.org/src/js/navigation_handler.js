/* global LazyLoader, NavigationMap */

(function() {
  'use strict';

  var scrollVar = {
    block: 'start',
    behavior: 'smooth'
  };

  var loader = LazyLoader;
  loader.load('js/navigation_map.js', function() {
    NavigationMap.init();
    if (NavigationMap.scrollVar) {
      scrollVar = NavigationMap.scrollVar;
    }
    window.dispatchEvent(new CustomEvent('mapLoaded'));
  });

  window.addEventListener('keydown', function(e) {
    handleKeydown(e);
  });

  window.addEventListener('menuEvent', function(e) {
    NavigationMap && (NavigationMap.optionMenuVisible = e.detail.menuVisible);
  });

  function handleKeydown(e) {
    var el = e.target,
        next;

    if (NavigationMap && NavigationMap.lockNav(e)) {
      return;
    }

    if (e.key === 'Enter' || e.key === 'Accept') {
      handleClick(e);
    } else {
      if (!e.target.classList) {
        return;
      }
      if (!e.target.classList.contains('focus')) {
        console.warn('e.target does not have focus');
        el = document.querySelector('.focus');
      }

      next = findElementFromNavProp(el, e);
      if (next && next.classList.contains('hidden')
        && (typeof ThreadUI !== 'undefined')
        && ThreadUI._renderingMessage
        && e.key === 'ArrowDown') {
        // When rendering message, we will skip handle ArrowDown
        return;
      }
      if (next) {
        var prevFocused = document.querySelectorAll('.focus');
        if (next == prevFocused[0]) {
          return;
        }

        if (isRecipientFromNavProp() &&
            (e.key === 'ArrowLeft' || e.key === 'ArrowRight')) {
          return;
        }

        if (prevFocused.length > 0) {
          prevFocused[0].classList.remove('focus');
        }
        if (!NavigationMap.scrollToElement) {
          next.scrollIntoView(scrollVar);
        } else {
          NavigationMap.scrollToElement(next, e);
        }
        next.classList.add('focus');
        if (NavigationMap.ignoreFocus === null || !NavigationMap.ignoreFocus) {
          next.focus();
          // App make decision to focus on next element.
          // Then, we have to prevent default here.
          e.preventDefault();
        }

        document.dispatchEvent(new CustomEvent('focusChanged', {
          detail: {
            focusedElement: next
          }
        }));
      }
    }
  }

  function isRecipientFromNavProp() {
    var activeElement = document.activeElement;
    var messagesRecipients =
      document.getElementById('messages-recipients-list');
    if ((activeElement.parentNode.id === 'messages-recipients-list') &&
        (messagesRecipients.querySelectorAll('.recipient').length > 1) &&
        (activeElement.getAttribute('contenteditable') === 'true') &&
        (activeElement.textContent)) {
      return true;
    }
    return false;
  }

  function findElementFromNavProp(current, e) {
    if (!current || NavigationMap && NavigationMap.disableNav) {
      return null;
    }

    var style = current.style;
    var id = null;
    switch (e.key) {
      case 'ArrowLeft':
        id = style.getPropertyValue('--nav-left');
        break;
      case 'ArrowRight':
        id = style.getPropertyValue('--nav-right');
        break;
      case 'ArrowUp':
        id = style.getPropertyValue('--nav-up');
        break;
      case 'ArrowDown':
        id = style.getPropertyValue('--nav-down');
        break;
      case 'Home':
      case 'MozHomeScree':
        id = style.getPropertyValue('--nav-home');
        break;
      default:
        break;
    }

    if (!id) {
      return null;
    }

    return document.querySelector('[data-nav-id="' + id + '"]');
  }

  function handleClick(e) {
    var el = document.querySelector('.focus');
    el && el.focus();

    if (NavigationMap && NavigationMap.optionMenuVisible && !e.target.classList.contains('menu-button')) {
      // workaround for case of quick click just right after option menu opening start
      var selectedMenuElement = document.querySelector('menu button.menu-button');
      selectedMenuElement && selectedMenuElement.click && selectedMenuElement.click();
    } else if (NavigationMap && NavigationMap.handleClick) {
      //costimization of click action.
      NavigationMap.handleClick(e);
    } else {
      e.target.click();
      for (var i = 0; i < e.target.children.length; i++) {
        e.target.children[i].click();
      }
    }
  }
})();
