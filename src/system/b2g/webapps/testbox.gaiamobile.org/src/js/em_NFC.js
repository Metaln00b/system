/**
 * Created with JetBrains WebStorm.
 * User: xa
 * Date: 3/18/14
 * Time: 11:12 AM
 * To change this template use File | Settings | File Templates.
 */

function debug(msg) {
  dump('lx:-*-* nfc.js-*-* ' + msg + '\n');
}
var NFCTest = (function() {
  var totalTime;
  var successTime;
  function init() {
    var timeId;
    var clearButton = document.getElementById('nfcClear');
    var StartButton = document.getElementById('nfcStart');
     totalTime = 0;
     successTime = 0;


    clearButton.onclick = function() {
      dump('lx: clearButtom click \n');
      this.disabled = true;
      totalTime = 0;
      successTime = 0;
      //document.getElementById('totalTimes').textContent ="Total Times:   " +
      // totalTime;
      $('nfcStart').disabled = false;
      document.getElementById('successTimes').textContent =
        'Success Times:   ' + successTime;
      var path = '/data/nfc_pcd.txt';
      if (navigator.engmodeExtension) {
        debug('lx:nfcEUTStop \n');
        var request = navigator.engmodeExtension.fileWriteLE('stop', path, 'f');
        request.onsuccess = function(msg) {
          dump('lx:stop onsuccess ' + msg);
        };
        request.onerror = function(msg) {
          dump('lx:stop error ' + msg);
        };
      }
    };
    StartButton.onclick = function() {

     var totalTimes = $('inputTimes').value;
     var intervalTime = $('intervalTime').value;
      dump('lx:total time ' + totalTimes + '\n');
      if (totalTimes && intervalTime) {
        this.disabled = true;

        if (navigator.engmodeExtension) {
        var engmodeEx = navigator.engmodeExtension;
        var parmArray = new Array();
        parmArray.push('test_pn547');
        parmArray.push('1 ' + intervalTime + ' ' + totalTimes);
        parmArray.push('/data/testbox_log/nfc.txt');
        var request = engmodeEx.execCmdLE(parmArray, 3);
          request.onsuccess = function() {
            dump('lx:NFC PCD Test Success!!!');
            var sysVer = navigator.engmodeExtension.fileReadLE('test_pn547');
            dump('lx: nfcEUTRun ' + sysVer + '\n');
            $('successTimes').textContent = sysVer;
            //$('nfcClear').disabled = false;
            clearInterval(timeId);
          };

          request.onerror = function(e) {
            alert('error!');
            $('nfcClear').disabled = false;
          };
      }
      timeId = setInterval(function() {
        var value = navigator.engmodeExtension.fileReadLE('test_pn547');
        dump('lx: nfcEUTRun ' + value + '\n');
        $('successTimes').textContent = value;
        $('nfcClear').disabled = false;
      }, 2000);
    } else {
      alert('input has no value!!');
      }

    };

  }

  return {
    init: init
  };
}());

NFCTest.init();

