#!/system/bin/busybox sh

# Usage: qpwn.sh [sim number] [new IMEI - 15 decimals]
# Use SIM number 9 to switch to band unlocking mode instead

OPDIR=$(mktemp -d)
BLKPREF=/dev/block/bootdevice/by-name
SIMINDEX=$1
TARGETNAME=nvm/num/550
if [ "$SIMINDEX" = 9 ]; then
  TARGETNAME=nvm/num/6828
  TARGETNAME2=nvm/num/6829
  PREP="\xff\x3f\xff\xff\xff\x0f\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
else
  if [ "$SIMINDEX" = 2 ]; then
    TARGETNAME=nvm/context1/550
  fi
  IMEI="80a$2"
  PREP="$(echo -n $IMEI | busybox sed -re 's/([a0-9])([a0-9])/\\x\2\1/g')" 
  TARGETNAME2=$TARGETNAME
fi
cd $OPDIR
echo "Reading tunning partition..."
busybox tar xf $BLKPREF/tunning
echo -ne "$PREP" > $TARGETNAME 
echo -ne "$PREP" > $TARGETNAME2
echo "Writing tunning partition..."
busybox tar cf - . > $BLKPREF/tunning
echo "Formatting modemst1 and modemst2..."
dd if=/dev/zero of=$BLKPREF/modemst1
dd if=/dev/zero of=$BLKPREF/modemst2
echo "IMEI changed, reboot to apply"
